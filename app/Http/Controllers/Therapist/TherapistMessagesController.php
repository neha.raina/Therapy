<?php

namespace App\Http\Controllers\Therapist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TherapistMessagesController extends Controller
{
    //therapist Messages listing
    public function index ()
    {
        
        return view ('front.therapist.therapist-messages');
    }
}

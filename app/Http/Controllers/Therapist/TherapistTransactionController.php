<?php

namespace App\Http\Controllers\Therapist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Transaction;
use App\Models\SearchTransaction;

class TherapistTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Payment tranction
         $transactions= Transaction::with('customer.User')->get() ;
            // return view ('front.user.transaction.transaction',compact('transactions'));
        return view ('front.therapist.transaction.therapist-transaction',compact('transactions'));
    }
    
     /*
     * Search transaction
     */
    public function search ()
    { 
        
        $searchQuery=new SearchTransaction;
        $transactions = $searchQuery->search();
        return view ('front.therapist.transaction.therapist-transaction',compact('transactions'));
       // return view ('front.user.transaction.transaction',compact('transactions'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delTrans =Transaction::find($id); 
        $delTrans->delete();
        return redirect()->route('therapist-transaction');
    }
}

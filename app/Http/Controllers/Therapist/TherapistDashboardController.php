<?php

namespace App\Http\Controllers\Therapist;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TherapistDashboardController extends Controller
{
    // therapist dashboard listing
    public function index () 
    {
           
        return view('front.therapist.therapist-dashboard');
    }
}

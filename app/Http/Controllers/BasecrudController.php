<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Users;
use App\Models\UsersSearch;
use Illuminate\Support\Facades\Validator;

use Hash;
use Session;
use Illuminate\Support\Facades\Input;

class BasecrudController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $searchModelName = $this->modelName. 'Search';
        $searchModel = new $searchModelName();
        $models = $searchModel->search($this->modelName);
        return view($this->baseRouteName. '.index', ['models' => $models, 'searchModel' => $searchModel]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $model = new $this->modelName;
        
        // load model fields from last request
        if (Input::old($this->formKey))
            $model->load(Input::old($this->formKey));
        
        return view($this->baseRouteName. '.create', ['model' => $model]);
    
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id or primaryKey of the model
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modelName=$this->modelName;
        $model = $modelName::where([$this->modelPrimaryKey => $id])->first();
        if($model)
            return view($this->baseRouteName. '.show',[$this->baseRouteName => $model]);
        else {
            Session::flash('error', ___('Invalid Access'));
            return redirect()->route($this->baseRouteName. '.index');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $modelName=$this->modelName;
        $model =$modelName::find($id);
        
        // load model fields from last request
        if (Input::old($this->formKey))
            $model->load(Input::old($this->formKey));
        
        return view($this->baseRouteName. '.update', ['model' => $model]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        return $this->store($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $modelName=$this->modelName;
        $model = $modelName::find($id);
        if ($model){
            $model->delete();
            Session::flash('success', ___('Item removed'));
        } else
            Session::flash('error', ___('Invalid Request'));
        return redirect()->route($this->baseRouteName. '.index');
    }
}

<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BasecrudController;
use Illuminate\Http\Request;

use App\Models\Courses;
use App\Models\CoursesSearch;
use Illuminate\Support\Facades\Validator;

use Hash;
use Session;
use Illuminate\Support\Facades\Input;

class CourseController extends BasecrudController
{

    public $modelName = 'App\Models\Courses';
    public $baseRouteName = 'courses';
    public $modelPrimaryKey = 'pkCourseID';
    public $formKey = 'Courses';

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {

    //     $usersSearch = new CoursesSearch();
    //     $courses = $coursesSearch->search('App\Models\Courses');
    //     return view('course.index', ['models' => $courses, 'searchModel' => $coursesSearch]);

    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
        
    //     $model = new Courses;
        
    //     // load model fields from last request
    //     if (Input::old('Courses'))
    //         $model->load(Input::old('Courses'));
        
    //     return view('course.create', ['model' => $model]);
    
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $id = null)
    {
        // echo '<pre>';
        // var_dump($request['Courses']);
        // die();
        
        if (empty($id)){

            $model = new Courses;
            $model->load($request['Courses']);

        } else {
            $model = Courses::findOrNew($id)->getModel()->setScenario('update')->load($request['Courses']);        
        }

        if ($model->validate() && $model->save()) {
            return redirect()->route('course.show', ['id' => $model->pkCourseID]);
        } else {
            Session::flash('error', $model->getErrors());
            return redirect()->back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id or primaryKey of the model
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     $course = Courses::where(['pkCourseID' => $id])->first();
    //     if($course)
    //         return view('course.show',['course' => $course]);
    //     else {
    //         Session::flash('error', ___('Invalid Access'));
    //         return redirect()->route('course.index');
    //     }
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $model = Courses::find($id);
        
    //     // load model fields from last request
    //     if (Input::old('Courses'))
    //         $model->load(Input::old('Courses'));
        
    //     return view('course.update', ['model' => $model]);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, int $id)
    // {
    //     return $this->store($request, $id);
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $model = Courses::find($id);
    //     if ($model){
    //         $model->delete();
    //         Session::flash('success', ___('Item removed'));
    //     } else
    //         Session::flash('error', ___('Invalid Request'));
    //     return redirect()->route('course.index');
    // }


}

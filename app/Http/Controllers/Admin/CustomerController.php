<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BasecrudController;
use Illuminate\Http\Request;

use App\Models\Customer;
use App\Models\CustomersSearch;
use Illuminate\Support\Facades\Validator;

use Hash;
use Session;
use Illuminate\Support\Facades\Input;

class CustomerController extends BasecrudController
{

    public $modelName = 'App\Models\Customers';
    public $baseRouteName = 'customers';
    public $modelPrimaryKey = 'pkCustomerID';
    public $formKey = 'Customers';

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function index()
    {

       $customersSearch = new CustomersSearch();
       $customers = $customersSearch->search(Customer::with('user'));
 //      $customer=Customer::with('user')->get();
//       echo '<pre>';
//       print_r($customer[0]->user->email);
//       die;
       return view('customers.index', ['models' => $customers, 'searchModel' => $customersSearch]);

     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
        
         $model = new Customer;
        
        // load model fields from last request
         if (Input::old('Customer'))
             $model->load(Input::old('Customer'));
        
         return view('customers.create', ['model' => $model]);
    
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $id = null)
    {
        // echo '<pre>';
        // var_dump($request['Customers']);
        // die();
        
        if (empty($id)){

            $model = new Customers;
            $model->load($request['Customers']);

        } else {
            $model = Customers::findOrNew($id)->getModel()->setScenario('update')->load($request['Customers']);        
        }

        if ($model->validate() && $model->save()) {
            return redirect()->route('customer.show', ['id' => $model->pkCustomerID]);
        } else {
            Session::flash('error', $model->getErrors());
            return redirect()->back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id or primaryKey of the model
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     $customer = Customers::where(['pkCustomerID' => $id])->first();
    //     if($customer)
    //         return view('customer.show',['customer' => $customer]);
    //     else {
    //         Session::flash('error', ___('Invalid Access'));
    //         return redirect()->route('customer.index');
    //     }
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $model = Customers::find($id);
        
    //     // load model fields from last request
    //     if (Input::old('Customers'))
    //         $model->load(Input::old('Customers'));
        
    //     return view('customer.update', ['model' => $model]);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, int $id)
    // {
    //     return $this->store($request, $id);
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $model = Customers::find($id);
    //     if ($model){
    //         $model->delete();
    //         Session::flash('success', ___('Item removed'));
    //     } else
    //         Session::flash('error', ___('Invalid Request'));
    //     return redirect()->route('customer.index');
    // }


}

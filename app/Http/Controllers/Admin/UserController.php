<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BasecrudController;
use Illuminate\Http\Request;

use App\Models\Users;
use App\Models\UsersSearch;
use Illuminate\Support\Facades\Validator;

use Hash;
use Session;
use Illuminate\Support\Facades\Input;

class UserController extends BasecrudController
{

    public $modelName = 'App\Models\Users';
    public $baseRouteName = 'user';
    public $modelPrimaryKey = 'pkUserID';
    public $formKey = 'Users';

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {

    //     $usersSearch = new UsersSearch();
    //     $users = $usersSearch->search('App\Models\Users');
    //     return view('user.index', ['models' => $users, 'searchModel' => $usersSearch]);

    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
        
    //     $model = new Users;
        
    //     // load model fields from last request
    //     if (Input::old('Users'))
    //         $model->load(Input::old('Users'));
        
    //     return view('user.create', ['model' => $model]);
    
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $id = null)
    {
        // echo '<pre>';
        // var_dump($request['Users']);
        // die();
        
        if (empty($id)){

            $model = new Users;
            $model->load($request['Users']);

        } else {
            $model = Users::findOrNew($id)->getModel()->setScenario('update')->load($request['Users']);        
        }

        if (!$model->exists || $model->userPassword != $request['Users']['userPassword']) {
            $model->userPassword = Hash::make($request['Users']['userPassword']);
        }

        if ($model->validate() && $model->save()) {
            return redirect()->route('user.show', ['id' => $model->pkUserID]);
        } else {
            Session::flash('error', $model->getErrors());
            return redirect()->back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id or primaryKey of the model
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     $user = Users::where(['pkUserID' => $id])->first();
    //     if($user)
    //         return view('user.show',['user' => $user]);
    //     else {
    //         Session::flash('error', ___('Invalid Access'));
    //         return redirect()->route('user.index');
    //     }
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $model = Users::find($id);
        
    //     // load model fields from last request
    //     if (Input::old('Users'))
    //         $model->load(Input::old('Users'));
        
    //     return view('user.update', ['model' => $model]);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, int $id)
    // {
    //     return $this->store($request, $id);
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $model = Users::find($id);
    //     if ($model){
    //         $model->delete();
    //         Session::flash('success', ___('Item removed'));
    //     } else
    //         Session::flash('error', ___('Invalid Request'));
    //     return redirect()->route('user.index');
    // }


}

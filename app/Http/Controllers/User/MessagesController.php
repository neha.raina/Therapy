<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Nahid\Talk\Facades\Talk;

use App\Models\User;

class MessagesController extends Controller
{

    protected $authUser; 
    
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * User Messages
     */
    public function showListingMessages ()
    {
        
         // Talk::setAuthUserId(Auth::user()->pkUserID);
          $users = User::all();
         /* echo '<pre>';
          print_r($users[0]->pkUserID);
          die('sanjay');*/
          return view('front.user.message.messages', compact('users'));

    }
    
    
    public function chatHistory($id)
    {  
        $conversations = Talk::getMessagesByUserId($id);
        echo '<pre>';
        var_dump($conversations);
        die('+++++');
        $user = '';
        $conversations=false;
        $messages = [];
        if(!$conversations) { 
            $user = User::find($id);
        } else {
            $user = $conversations->withUser;
            $messages = $conversations->messages;
        }
        return view('front.user.message.conversations', compact('messages', 'user'));
    }
    public function ajaxSendMessage(Request $request)
    {
        if ($request->ajax()) {
            $rules = [
                'message-data'=>'required',
                '_id'=>'required'
            ];
            $this->validate($request, $rules);
            $body = $request->input('message-data');
            $userId = $request->input('_id');
            if ($message = Talk::sendMessageByUserId($userId, $body)) {
                $html = view('ajax.newMessageHtml', compact('message'))->render();
                return response()->json(['status'=>'success', 'html'=>$html], 200);
            }
        }
    }
    public function ajaxDeleteMessage(Request $request, $id)
    {
        if ($request->ajax()) {
            if(Talk::deleteMessage($id)) {
                return response()->json(['status'=>'success'], 200);
            }
            return response()->json(['status'=>'errors', 'msg'=>'something went wrong'], 401);
        }
    }
    public function tests()
    {
        dd(Talk::channel());
    }

}



    
  

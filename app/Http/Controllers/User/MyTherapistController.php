<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MyTherapistController extends Controller
{
    public function index ()
    {
          return view ('front.user.my-therapist');
    }
}

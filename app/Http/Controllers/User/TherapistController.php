<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TherapistController extends Controller
{
    public function index()
    {
        return view('front.user.therapist');
    }
}

<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserTherapist;

class MyBookingsController extends Controller
{
   
     public function index ()
    {
          $myTherapists=UserTherapist::with('user')->get();
//          ceho '<pre>';
//          print_r($myTherapists);
//          die('sanjay');
            return view ('front.user.my-bookings', compact('myTherapists'));
    }
    
}

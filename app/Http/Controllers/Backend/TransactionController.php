<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BasecrudController;
use Illuminate\Http\Request;

use App\Models\Transactions;
use App\Models\TransactionsSearch;
use Illuminate\Support\Facades\Validator;

use Hash;
use Session;
use Illuminate\Support\Facades\Input;

class TransactionController extends BasecrudController
{

    public $modelName = 'App\Models\Transactions';
    public $baseRouteName = 'transactions';
    public $modelPrimaryKey = 'pkTransactionID';
    public $formKey = 'Transactions';

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {

    //     $usersSearch = new TransactionsSearch();
    //     $transactions = $transactionsSearch->search('App\Models\Transactions');
    //     return view('transaction.index', ['models' => $transactions, 'searchModel' => $transactionsSearch]);

    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
        
    //     $model = new Transactions;
        
    //     // load model fields from last request
    //     if (Input::old('Transactions'))
    //         $model->load(Input::old('Transactions'));
        
    //     return view('transaction.create', ['model' => $model]);
    
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $id = null)
    {
        // echo '<pre>';
        // var_dump($request['Transactions']);
        // die();
        
        if (empty($id)){

            $model = new Transactions;
            $model->load($request['Transactions']);

        } else {
            $model = Transactions::findOrNew($id)->getModel()->setScenario('update')->load($request['Transactions']);        
        }

        if ($model->validate() && $model->save()) {
            return redirect()->route('transaction.show', ['id' => $model->pkTransactionID]);
        } else {
            Session::flash('error', $model->getErrors());
            return redirect()->back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id or primaryKey of the model
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     $transaction = Transactions::where(['pkTransactionID' => $id])->first();
    //     if($transaction)
    //         return view('transaction.show',['transaction' => $transaction]);
    //     else {
    //         Session::flash('error', ___('Invalid Access'));
    //         return redirect()->route('transaction.index');
    //     }
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $model = Transactions::find($id);
        
    //     // load model fields from last request
    //     if (Input::old('Transactions'))
    //         $model->load(Input::old('Transactions'));
        
    //     return view('transaction.update', ['model' => $model]);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, int $id)
    // {
    //     return $this->store($request, $id);
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $model = Transactions::find($id);
    //     if ($model){
    //         $model->delete();
    //         Session::flash('success', ___('Item removed'));
    //     } else
    //         Session::flash('error', ___('Invalid Request'));
    //     return redirect()->route('transaction.index');
    // }


}

<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BasecrudController;
use Illuminate\Http\Request;

use App\Models\Faqs;
use App\Models\FaqsSearch;
use Illuminate\Support\Facades\Validator;

use Hash;
use Session;
use Illuminate\Support\Facades\Input;

class FaqController extends BasecrudController
{

    public $modelName = 'App\Models\Faqs';
    public $baseRouteName = 'faqs';
    public $modelPrimaryKey = 'pkFaqID';
    public $formKey = 'Faqs';

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {

    //     $usersSearch = new FaqsSearch();
    //     $faqs = $faqsSearch->search('App\Models\Faqs');
    //     return view('faq.index', ['models' => $faqs, 'searchModel' => $faqsSearch]);

    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
        
    //     $model = new Faqs;
        
    //     // load model fields from last request
    //     if (Input::old('Faqs'))
    //         $model->load(Input::old('Faqs'));
        
    //     return view('faq.create', ['model' => $model]);
    
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $id = null)
    {
        // echo '<pre>';
        // var_dump($request['Faqs']);
        // die();
        
        if (empty($id)){

            $model = new Faqs;
            $model->load($request['Faqs']);

        } else {
            $model = Faqs::findOrNew($id)->getModel()->setScenario('update')->load($request['Faqs']);        
        }

        if ($model->validate() && $model->save()) {
            return redirect()->route('faq.show', ['id' => $model->pkFaqID]);
        } else {
            Session::flash('error', $model->getErrors());
            return redirect()->back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id or primaryKey of the model
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     $faq = Faqs::where(['pkFaqID' => $id])->first();
    //     if($faq)
    //         return view('faq.show',['faq' => $faq]);
    //     else {
    //         Session::flash('error', ___('Invalid Access'));
    //         return redirect()->route('faq.index');
    //     }
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $model = Faqs::find($id);
        
    //     // load model fields from last request
    //     if (Input::old('Faqs'))
    //         $model->load(Input::old('Faqs'));
        
    //     return view('faq.update', ['model' => $model]);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, int $id)
    // {
    //     return $this->store($request, $id);
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $model = Faqs::find($id);
    //     if ($model){
    //         $model->delete();
    //         Session::flash('success', ___('Item removed'));
    //     } else
    //         Session::flash('error', ___('Invalid Request'));
    //     return redirect()->route('faq.index');
    // }


}

<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BasecrudController;
use Illuminate\Http\Request;

use App\Models\products;
use App\Models\productsSearch;
use Illuminate\Support\Facades\Validator;

use Hash;
use Session;
use Illuminate\Support\Facades\Input;

class productController extends BasecrudController
{

    public $modelName = 'App\Models\products';
    public $baseRouteName = 'products';
    public $modelPrimaryKey = 'pkProductID';
    public $formKey = 'products';

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {

    //     $usersSearch = new productsSearch();
    //     $products = $productsSearch->search('App\Models\products');
    //     return view('product.index', ['models' => $products, 'searchModel' => $productsSearch]);

    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
        
    //     $model = new products;
        
    //     // load model fields from last request
    //     if (Input::old('products'))
    //         $model->load(Input::old('products'));
        
    //     return view('product.create', ['model' => $model]);
    
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $id = null)
    {
        // echo '<pre>';
        // var_dump($request['products']);
        // die();
        
        if (empty($id)){

            $model = new products;
            $model->load($request['products']);

        } else {
            $model = products::findOrNew($id)->getModel()->setScenario('update')->load($request['products']);        
        }

        if ($model->validate() && $model->save()) {
            return redirect()->route('product.show', ['id' => $model->pkProductID]);
        } else {
            Session::flash('error', $model->getErrors());
            return redirect()->back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id or primaryKey of the model
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     $product = products::where(['pkProductID' => $id])->first();
    //     if($product)
    //         return view('product.show',['product' => $product]);
    //     else {
    //         Session::flash('error', ___('Invalid Access'));
    //         return redirect()->route('product.index');
    //     }
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $model = products::find($id);
        
    //     // load model fields from last request
    //     if (Input::old('products'))
    //         $model->load(Input::old('products'));
        
    //     return view('product.update', ['model' => $model]);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, int $id)
    // {
    //     return $this->store($request, $id);
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $model = products::find($id);
    //     if ($model){
    //         $model->delete();
    //         Session::flash('success', ___('Item removed'));
    //     } else
    //         Session::flash('error', ___('Invalid Request'));
    //     return redirect()->route('product.index');
    // }


}

<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BasecrudController;
use Illuminate\Http\Request;

use App\Models\Messages;
use App\Models\MessagesSearch;
use Illuminate\Support\Facades\Validator;

use Hash;
use Session;
use Illuminate\Support\Facades\Input;

class MessageController extends BasecrudController
{

    public $modelName = 'App\Models\Messages';
    public $baseRouteName = 'messages';
    public $modelPrimaryKey = 'pkMessagesID';
    public $formKey = 'Messages';

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {

    //     $usersSearch = new MessagesSearch();
    //     $messages = $messagesSearch->search('App\Models\Messages');
    //     return view('message.index', ['models' => $messages, 'searchModel' => $messagesSearch]);

    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
        
    //     $model = new Messages;
        
    //     // load model fields from last request
    //     if (Input::old('Messages'))
    //         $model->load(Input::old('Messages'));
        
    //     return view('message.create', ['model' => $model]);
    
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $id = null)
    {
        // echo '<pre>';
        // var_dump($request['Messages']);
        // die();
        
        if (empty($id)){

            $model = new Messages;
            $model->load($request['Messages']);

        } else {
            $model = Messages::findOrNew($id)->getModel()->setScenario('update')->load($request['Messages']);        
        }

        if ($model->validate() && $model->save()) {
            return redirect()->route('message.show', ['id' => $model->pkMessagesID]);
        } else {
            Session::flash('error', $model->getErrors());
            return redirect()->back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id or primaryKey of the model
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     $message = Messages::where(['pkMessagesID' => $id])->first();
    //     if($message)
    //         return view('message.show',['message' => $message]);
    //     else {
    //         Session::flash('error', ___('Invalid Access'));
    //         return redirect()->route('message.index');
    //     }
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $model = Messages::find($id);
        
    //     // load model fields from last request
    //     if (Input::old('Messages'))
    //         $model->load(Input::old('Messages'));
        
    //     return view('message.update', ['model' => $model]);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, int $id)
    // {
    //     return $this->store($request, $id);
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $model = Messages::find($id);
    //     if ($model){
    //         $model->delete();
    //         Session::flash('success', ___('Item removed'));
    //     } else
    //         Session::flash('error', ___('Invalid Request'));
    //     return redirect()->route('message.index');
    // }


}

<?php
namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BasecrudController;
use Illuminate\Http\Request;

use App\Models\Usertherapists;
use App\Models\UsertherapistsSearch;
use Illuminate\Support\Facades\Validator;

use Hash;
use Session;
use Illuminate\Support\Facades\Input;

class UsertherapistController extends BasecrudController
{

    public $modelName = 'App\Models\Usertherapists';
    public $baseRouteName = 'usertherapists';
    public $modelPrimaryKey = 'pkTherapistID';
    public $formKey = 'Usertherapists';

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {

    //     $usersSearch = new UsertherapistsSearch();
    //     $usertherapists = $usertherapistsSearch->search('App\Models\Usertherapists');
    //     return view('usertherapist.index', ['models' => $usertherapists, 'searchModel' => $usertherapistsSearch]);

    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
        
    //     $model = new Usertherapists;
        
    //     // load model fields from last request
    //     if (Input::old('Usertherapists'))
    //         $model->load(Input::old('Usertherapists'));
        
    //     return view('usertherapist.create', ['model' => $model]);
    
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $id = null)
    {
        // echo '<pre>';
        // var_dump($request['Usertherapists']);
        // die();
        
        if (empty($id)){

            $model = new Usertherapists;
            $model->load($request['Usertherapists']);

        } else {
            $model = Usertherapists::findOrNew($id)->getModel()->setScenario('update')->load($request['Usertherapists']);        
        }

        if ($model->validate() && $model->save()) {
            return redirect()->route('usertherapist.show', ['id' => $model->pkTherapistID]);
        } else {
            Session::flash('error', $model->getErrors());
            return redirect()->back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id or primaryKey of the model
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     $usertherapist = Usertherapists::where(['pkTherapistID' => $id])->first();
    //     if($usertherapist)
    //         return view('usertherapist.show',['usertherapist' => $usertherapist]);
    //     else {
    //         Session::flash('error', ___('Invalid Access'));
    //         return redirect()->route('usertherapist.index');
    //     }
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $model = Usertherapists::find($id);
        
    //     // load model fields from last request
    //     if (Input::old('Usertherapists'))
    //         $model->load(Input::old('Usertherapists'));
        
    //     return view('usertherapist.update', ['model' => $model]);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, int $id)
    // {
    //     return $this->store($request, $id);
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     $model = Usertherapists::find($id);
    //     if ($model){
    //         $model->delete();
    //         Session::flash('success', ___('Item removed'));
    //     } else
    //         Session::flash('error', ___('Invalid Request'));
    //     return redirect()->route('usertherapist.index');
    // }


}

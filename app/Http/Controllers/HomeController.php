<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(Auth::user()->roleType ==='therapist') { 
                         return redirect('therapist-dashboard');
      }
       elseif(Auth::user()->roleType ==='customer'){  
                               return     redirect('customer-dashboard');//->route();
       }
         elseif (Auth::user()->roleType ==='Admin') { 
                                return   redirect('admin/admin-dashboard');
       }
    }
}

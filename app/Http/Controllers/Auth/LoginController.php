<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
   // protected $redirectTo = '/dashboard';
    
     protected function redirectTo ()
     {
         
      if(Auth::user()->roleType ==='therapist') { 
                                     return 'therapist-dashboard';
      }
       elseif(Auth::user()->roleType ==='customer'){ 
                                        return 'customer-dashboard';
       }
         elseif (Auth::user()->roleType ==='Admin') {
                                    return 'admin/admin-dashboard';
       }
                                  
     }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    
    public function showLoginForm()
    { 
		return view('front.auth.login');
			
     }
}



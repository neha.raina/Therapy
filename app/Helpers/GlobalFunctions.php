<?php
/**
 * This file contains global functions
 *
 */

/**
 * Returns translated text 
 *
 * @param string $text text to be translated 
 * @param string $textDomain is domain to which the $text will be translated
 * @param string $scenario some text might have different transalation depending upon different scenario
 */
function ___($text, $textDomain = null, $scenario = null){
	return ''. $text;
}

/**
 * Makes call to ___() function and echos the result
 **/
function __e($text, $textDomain = null, $scenario = null){
	echo ___($text, $textDomain, $scenario);
}

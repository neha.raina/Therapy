<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        foreach (glob(app_path().'/Helpers/*.php') as $filename){
            require_once($filename.'');
        }

        \Form::component('textInput', 'components.form.custom', ['model' => null, 'name', 'attributes' => [], 'type' => 'text']);
        \Form::component('areaInput', 'components.form.custom', ['model' => null, 'name', 'attributes' => [], 'type' => 'area']);
        \Form::component('hiddenInput', 'components.form.custom', ['model' => null, 'name', 'attributes' => [], 'type' => 'hidden']);
        \Form::component('passwordInput', 'components.form.custom', ['model' => null, 'name', 'attributes' => [], 'type' => 'password']);
        \Form::component('dropDown', 'components.form.custom', ['model' => null, 'name', 'options' => [], 'attributes' => [], 'type' => 'select']);
        \Form::component('fileInput', 'components.form.custom', ['model' => null, 'name', 'options' => [], 'attributes' => [], 'type' => 'file']);
        

        // Using Closure based composers...
        // View::composer('dashboard', function ($view) {
        //     //
        // });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // View::composer(
        //     'abc.DataGrid', 'App\Http\ViewComposers\DataGrid'
        // );
        // View::composer('tom', function ($view) {
        //     return 'hello from view component';
        // });
        //
    }
}
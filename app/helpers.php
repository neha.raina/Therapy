<?php

/**
 * Global helpers file with misc functions
 */
if (!function_exists('app_name')) {

    /**
     * Helper to grab the application name
     *
     * @return mixed
     */
    function app_name() {
        return config('app.name');
    }

}

if (!function_exists('access')) {

    /**
     * Access (lol) the Access:: facade as a simple function
     */
    function access() {
        return app('access');
    }

}

if (!function_exists('setActive')) {

    function setActive($path) {
        return Request::is($path . '*') ? 'class=active' : '';
    }

}

if (!function_exists('profilePic')) {

    function profilePic(?string $image):string {
        
           if (isset($image)) {
               return asset('images/'.$image);
           }
               return asset('images/noprofilepic.jpg');
    }

}

if (!function_exists('image')) {

    function image(?string $image):string {
        
           if (isset($image)) {
               return asset('images/'.$image);
           }
               return asset('images/noprofilepic.jpg');
    }

}
    
    
   


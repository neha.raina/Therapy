<?php 
namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Faq extends BaseModel
{
	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	// protected $table = 'faqs';
	public $primaryKey = 'pkFaqID';
	// protected $modelSearchName = 'FaqSearch';

	public function __construct(){
		parent::__construct($this);
	}

	public static function getLabel(){
 		return [
 			'pkFaqID' => ___('Faq ID'),
			'faqQuestion' => ___('Faq Question'),
			'faqAnswer' => ___('Faq Answer'),
			'faqStatus' => ___('Faq Status'),
			'created_at' => ___('Created'),
			'updated_at' => ___('Updated'),
 		];
	}

 	/**
 	 * Contains list of all scenarios
 	 */
 	public function getScenarios(){
 		return [
 			
 			'default' => [
 				'rules' => [
					'faqQuestion' => 'required|max:512',
					'faqAnswer' => 'required',
					'faqStatus' => 'required',
					'created_at' => 'required',
					'updated_at' => 'required',
 				],
 				'fillable' => ['faqQuestion', 'faqAnswer', 'faqStatus', 'created_at', 'updated_at']
 			],

 			'update' => [
 				'inherit' => 'default',
 				//'rules' => [
		 		//	'column1' => 'required|email',
 				//],
 				//'fillable' => ['column1', 'column2']
 			]
 		];
 	}

}

<?php 
namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Usertherapist extends BaseModel
{
	const CREATED_AT = '';
	const UPDATED_AT = '';

	protected $table = 'user_therapists';
	public $primaryKey = 'pkTherapistID';
	// protected $modelSearchName = 'UsertherapistSearch';

	public function __construct(){
		parent::__construct($this);
	}
        
         public function user () 
        {
               return $this->belongsTo('App\Models\User','fkUserID','pkUserID');
        }

	public static function getLabel(){
 		return [
 			'pkTherapistID' => ___('Therapist ID'),
			'fkUserID' => ___('User ID'),
			'therapistProfilePic' => ___('Therapist Profile Pic'),
			'therapistAddress' => ___('Therapist Address'),
			'therapistQualification' => ___('Therapist Qualification'),
			'therapistExperience' => ___('Therapist Experience'),
			'therapistDescription' => ___('Therapist Description'),
 		];
	}

 	/**
 	 * Contains list of all scenarios
 	 */
 	public function getScenarios(){
 		return [
 			
 			'default' => [
 				'rules' => [
					'fkUserID' => 'required',
					'therapistProfilePic' => 'max:500',
					'therapistAddress' => 'max:1000',
					'therapistQualification' => 'max:50',
					'therapistExperience' => '',
					'therapistDescription' => '',
 				],
 				'fillable' => ['fkUserID', 'therapistProfilePic', 'therapistAddress', 'therapistQualification', 'therapistExperience', 'therapistDescription']
 			],

 			'update' => [
 				'inherit' => 'default',
 				//'rules' => [
		 		//	'column1' => 'required|email',
 				//],
 				//'fillable' => ['column1', 'column2']
 			]
 		];
 	}

}

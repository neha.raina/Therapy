<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTherapist extends Model
{
    /*
     *  Table Name
     */
    protected $table='user_therapists';
    /*
     * Primary Key
     */
    protected $primaryKey='pkTherapistID';
    /*
     *  Relation with User His Primary Data (common data)
     */
    public function user() {
        
       return  $this->belongsTo('App\Models\User','fkUserID');
    }
}

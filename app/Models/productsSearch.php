<?php
namespace App\Models;

class productsSearch extends product{
	public static $isSearchModel = true;
	public $searchItemPerPage = 3;
	public $paginationKey = 'page';
	public $searchAttributesUsingLike = ['productName', 'productDescription', 'productImage'];
}

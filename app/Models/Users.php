<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Users extends BaseModel
{
	const CREATED_AT = 'userCreatedAt';
	const UPDATED_AT = 'userUpdatedAt';

	// protected $table = 'users';
	public $primaryKey = 'pkUserID';
	// protected $modelSearchName = 'UsersSearch';

	public function __construct(){
		parent::__construct($this);
	}

	public static function getLabel(){
 		return [
 			'pkUserID' => ___('ID'),
 			'userFirstName' => ___('First Name'),
 			'userLastName' => ___('Last Name'),
 			// 'userGender' => ___('Gender'),
 			'userEmail' => ___('Email'),
 			'userPassword' => ___('Password'),
 			'userRole' => ___('Role'),
 			'userImage' => ___('Image'),
 			'userStatus' => ___('Status')
 		];
	}

 	/**
 	 * Contains list of all scenarios
 	 */
 	public function getScenarios(){
 		return [
 			
 			'default' => [
 				'rules' => [
		 			'userFirstName' => 'required|min:5',
		 			'userEmail' => 'required|email|unique:users,userEmail,'. $this->{$this->primaryKey}. ',pkUserID',
		 			'userStatus' => 'required'			
 				],
 				'fillable' => ['userFirstName', 'userLastName', 'userGender', 'userEmail', 'userStatus']
 			],

 			'update' => [
 				'inherit' => 'default',
 				'rules' => [
		 			'userEmail' => 'required|email',
 				],
 				'fillable' => ['userFirstName', 'userLastName', 'userEmail', 'userStatus', 'userGender']
 			]
 		];
 	}

	/**
	 * Returns Full name of the user
	 * @return string name of the user
	 */ 
 	public function getName(){
 		return trim($this->userFirstName. ' '. $this->userLastName);
 	}

}

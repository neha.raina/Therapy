<?php 
namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class product extends BaseModel
{
	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	// protected $table = 'products';
	public $primaryKey = 'pkProductID';
	// protected $modelSearchName = 'productSearch';

	public function __construct(){
		parent::__construct($this);
	}

	public static function getLabel(){
 		return [
 			'pkProductID' => ___('Product ID'),
			'productName' => ___('Product Name'),
			'productDescription' => ___('Product Description'),
			'productPrice' => ___('Product Price'),
			'productImage' => ___('Product Image'),
			'productStatus' => ___('Product Status'),
			'created_at' => ___('Created'),
			'updated_at' => ___('Updated'),
 		];
	}

 	/**
 	 * Contains list of all scenarios
 	 */
 	public function getScenarios(){
 		return [
 			
 			'default' => [
 				'rules' => [
					'productName' => 'required|max:128',
					'productDescription' => 'required',
					'productPrice' => 'required',
					'productImage' => 'required|max:500',
					'productStatus' => 'required',
					'created_at' => 'required',
					'updated_at' => 'required',
 				],
 				'fillable' => ['productName', 'productDescription', 'productPrice', 'productImage', 'productStatus', 'created_at', 'updated_at']
 			],

 			'update' => [
 				'inherit' => 'default',
 				//'rules' => [
		 		//	'column1' => 'required|email',
 				//],
 				//'fillable' => ['column1', 'column2']
 			]
 		];
 	}

}

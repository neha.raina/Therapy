<?php
namespace App\Models;

class UsertherapistsSearch extends Usertherapists{
	public static $isSearchModel = true;
	public $searchItemPerPage = 3;
	public $paginationKey = 'page';
	public $searchAttributesUsingLike = ['therapistProfilePic', 'therapistAddress', 'therapistQualification', 'therapistDescription'];
}

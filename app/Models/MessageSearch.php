<?php
namespace App\Models;

class MessagesSearch extends Messages{
	public static $isSearchModel = true;
	public $searchItemPerPage = 3;
	public $paginationKey = 'page';
	public $searchAttributesUsingLike = ['message'];
}

<?php

namespace App\Models\History\Traits\Relationship;

use App\Models\User;
use App\Models\History\HistoryType;

/**
 * Class HistoryRelationship
 * @package App\Models\History\Traits\Relationship
 */
trait UserRelationship
{

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function therapistOrCustomer() {
		return $this->hasOne(User::class, 'pkUserID', 'fkUserID');
	}

}
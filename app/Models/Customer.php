<?php 
namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class Customer extends BaseModel
{
	const CREATED_AT = '';
	const UPDATED_AT = '';

	 protected $table = 'user_customers';
	public $primaryKey = 'pkCustomerID';
	// protected $modelSearchName = 'CustomerSearch';

	public function __construct(){
		parent::__construct($this);
	}
        
        public function user () 
        {
               return $this->belongsTo('App\Models\User','fkUserID','pkUserID');
        }

	public static function getLabel(){
 		return [
 			'pkCustomerID' => ___('Customer ID'),
			'fkUserID' => ___('User ID'),
			'customerMobile' => ___('Customer Mobile'),
			'customerProfilePic' => ___('Customer Profile Pic'),
			'customerAddress' => ___('Customer Address'),
 		];
	}

 	/**
 	 * Contains list of all scenarios
 	 */
 	public function getScenarios(){
 		return [
 			
 			'default' => [
 				'rules' => [
					'fkUserID' => 'required',
					'customerMobile' => 'unique:user_customers,customerMobile,'. $this->{$this->primaryKey}. ',pkCustomerID',
					'customerProfilePic' => 'max:255',
					'customerAddress' => 'max:255',
 				],
 				'fillable' => ['fkUserID', 'customerMobile', 'customerProfilePic', 'customerAddress']
 			],

 			'update' => [
 				'inherit' => 'default',
 				//'rules' => [
		 		//	'column1' => 'required|email',
 				//],
 				//'fillable' => ['column1', 'column2']
 			]
 		];
 	}

}

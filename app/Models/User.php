<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\UserTherapist;

class User extends Authenticatable
{
    use Notifiable;
   
     protected $primaryKey='pkUserID';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    /*
     * User Therapist
     */
    public function therapist () 
    {
        
        return $this->hasOne('App\Models\UserTherapist','fkUserID','pkUserID');
    }
    /*
     *  User Customer
     */
    public function customer () 
    {
        
        return $this->hasOne('App\Models\Customer','fkUserID','pkUserID');
    }
}

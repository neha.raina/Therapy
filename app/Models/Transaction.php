<?php 
namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;
use App\Models\UserCustomer;

class Transaction extends BaseModel
{
	const CREATED_AT = 'created_at';
	const UPDATED_AT = '';

	protected $table = 'transactions';
	public $primaryKey = 'pkTransactionID';
	// protected $modelSearchName = 'TransactionSearch';
       
        public function customer()
        { //die('sanjay');

            return $this->belongsTo('App\Models\UserCustomer','fkCustomerID','pkCustomerID');
        }
    
	public function __construct(){
		parent::__construct($this);
	}

	public static function getLabel(){
 		return [
 			'pkTransactionID' => ___('Transaction ID'),
			'fkCustomerID' => ___('Customer ID'),
			'fkTherapistID' => ___('Therapist ID'),
			'transactionAmount' => ___('Transaction Amount'),
			'transactionCurrency' => ___('Transaction Currency'),
			'transactionMethod' => ___('Transaction Method'),
			'transactionDetail' => ___('Transaction Detail'),
			'transactionPaymentID' => ___('Transaction Payment ID'),
			'transactionStatus' => ___('Transaction Status'),
			'created_at' => ___('Created'),
 		];
	}

 	/**
 	 * Contains list of all scenarios
 	 */
 	public function getScenarios(){
 		return [
 			
 			'default' => [
 				'rules' => [
					'fkCustomerID' => 'required',
					'fkTherapistID' => 'required',
					'transactionAmount' => 'required',
					'transactionCurrency' => 'required|max:5',
					'transactionMethod' => 'required',
					'transactionDetail' => '',
					'transactionPaymentID' => 'max:128',
					'transactionStatus' => 'required',
					'created_at' => 'required',
 				],
 				'fillable' => ['fkCustomerID', 'fkTherapistID', 'transactionAmount', 'transactionCurrency', 'transactionMethod', 'transactionDetail', 'transactionPaymentID', 'transactionStatus', 'created_at']
 			],

 			'update' => [
 				'inherit' => 'default',
 				//'rules' => [
		 		//	'column1' => 'required|email',
 				//],
 				//'fillable' => ['column1', 'column2']
 			]
 		];
 	}

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;
use App\Models\UserCustomer;
class Transaction extends Model
{
    /*
     *  for search 
     */
    public static $modelSearchName='SearchTransaction';
    /*
     *  table Name
     */
//    protected $table='transactions';
    /*
     *  primary key
     */
//    protected $primaryKey='pkTransactionID';
    /*
     *  belongto user
     */
    public function customer()
    { //die('sanjay');
        
        return $this->belongsTo('App\Models\UserCustomer','fkCustomerID','pkCustomerID');
    }
}

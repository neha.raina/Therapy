<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;
use App\Models\UserCustomer;
use App\Models\Transaction;
use Illuminate\Support\Facades\Request;

class SearchTransaction extends Transaction
{
    
  public function search(){
       
		$searchParameters = array_filter(Request::input('SearchTansaction',[]));
               
 		if (!empty($searchParameters)) {
 			
 			$result = null;

 			foreach ($searchParameters as $key => $value) {
				if (!$result)
					$result = self::where($key, $value);
				else
					$result = $result->where($key, $value);
 			}

 			return $result->get();

 		} else
 			return self::all();

 	}  
        
}

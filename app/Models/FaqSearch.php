<?php
namespace App\Models;

class FaqsSearch extends Faqs{
	public static $isSearchModel = true;
	public $searchItemPerPage = 3;
	public $paginationKey = 'page';
	public $searchAttributesUsingLike = ['faqQuestion','faqAnswer'];
}

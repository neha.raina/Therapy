<?php 
namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Messages extends BaseModel
{
	const CREATED_AT = 'created_at';
	const UPDATED_AT = '
<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: Notice</p>
<p>Message:  Undefined index: updateTimeField</p>
<p>Filename: Models/{{model-plural-name}}.php</p>
<p>Line Number: 14</p>


	<p>Backtrace:</p>
	
		
	
		
	
		
			<p style="margin-left:10px">
			File: /opt/lampp/htdocs/trash/crud_generator/crud/templates/laravel/app/Models/{{model-plural-name}}.php<br />
			Line: 14<br />
			Function: _error_handler			</p>

		
	
		
	
		
	
		
			<p style="margin-left:10px">
			File: /opt/lampp/htdocs/trash/crud_generator/application/controllers/Laravel.php<br />
			Line: 88<br />
			Function: view			</p>

		
	
		
	
		
			<p style="margin-left:10px">
			File: /opt/lampp/htdocs/trash/crud_generator/index.php<br />
			Line: 315<br />
			Function: require_once			</p>

		
	

</div>';

	// protected $table = 'messages';
	public $primaryKey = 'pkMessagesID';
	// protected $modelSearchName = 'MessageSearch';

	public function __construct(){
		parent::__construct($this);
	}

	public static function getLabel(){
 		return [
 			'pkMessagesID' => ___('Messages ID'),
			'message' => ___('Message'),
			'is_seen' => ___('Is'),
			'deleted_from_sender' => ___('Deleted'),
			'deleted_from_receiver' => ___('Deleted'),
			'user_id' => ___('User'),
			'conversation_id' => ___('Conversation'),
			'created_at' => ___('Created'),
 		];
	}

 	/**
 	 * Contains list of all scenarios
 	 */
 	public function getScenarios(){
 		return [
 			
 			'default' => [
 				'rules' => [
					'message' => 'required',
					'is_seen' => 'required',
					'deleted_from_sender' => 'required',
					'deleted_from_receiver' => 'required',
					'user_id' => 'required',
					'conversation_id' => 'required',
					'created_at' => 'required',
 				],
 				'fillable' => ['message', 'is_seen', 'deleted_from_sender', 'deleted_from_receiver', 'user_id', 'conversation_id', 'created_at']
 			],

 			'update' => [
 				'inherit' => 'default',
 				//'rules' => [
		 		//	'column1' => 'required|email',
 				//],
 				//'fillable' => ['column1', 'column2']
 			]
 		];
 	}

}

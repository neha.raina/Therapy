<?php 
namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Transactions extends BaseModel
{
	const CREATED_AT = 'created_at';
	const UPDATED_AT = '
<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">

<h4>A PHP Error was encountered</h4>

<p>Severity: Notice</p>
<p>Message:  Undefined index: updateTimeField</p>
<p>Filename: Models/{{model-plural-name}}.php</p>
<p>Line Number: 14</p>


	<p>Backtrace:</p>
	
		
	
		
	
		
			<p style="margin-left:10px">
			File: /opt/lampp/htdocs/trash/crud_generator/crud/templates/laravel/app/Models/{{model-plural-name}}.php<br />
			Line: 14<br />
			Function: _error_handler			</p>

		
	
		
	
		
	
		
			<p style="margin-left:10px">
			File: /opt/lampp/htdocs/trash/crud_generator/application/controllers/Laravel.php<br />
			Line: 88<br />
			Function: view			</p>

		
	
		
	
		
			<p style="margin-left:10px">
			File: /opt/lampp/htdocs/trash/crud_generator/index.php<br />
			Line: 315<br />
			Function: require_once			</p>

		
	

</div>';

	// protected $table = 'transactions';
	public $primaryKey = 'pkTransactionID';
	// protected $modelSearchName = 'TransactionSearch';

	public function __construct(){
		parent::__construct($this);
	}

	public static function getLabel(){
 		return [
 			'pkTransactionID' => ___('Transaction ID'),
			'fkCustomerID' => ___('Customer ID'),
			'fkTherapistID' => ___('Therapist ID'),
			'transactionAmount' => ___('Transaction Amount'),
			'transactionCurrency' => ___('Transaction Currency'),
			'transactionMethod' => ___('Transaction Method'),
			'transactionDetail' => ___('Transaction Detail'),
			'transactionPaymentID' => ___('Transaction Payment ID'),
			'transactionStatus' => ___('Transaction Status'),
			'created_at' => ___('Created'),
 		];
	}

 	/**
 	 * Contains list of all scenarios
 	 */
 	public function getScenarios(){
 		return [
 			
 			'default' => [
 				'rules' => [
					'fkCustomerID' => 'required',
					'fkTherapistID' => 'required',
					'transactionAmount' => 'required',
					'transactionCurrency' => 'required|max:5',
					'transactionMethod' => 'required',
					'transactionDetail' => '',
					'transactionPaymentID' => 'max:128',
					'transactionStatus' => 'required',
					'created_at' => 'required',
 				],
 				'fillable' => ['fkCustomerID', 'fkTherapistID', 'transactionAmount', 'transactionCurrency', 'transactionMethod', 'transactionDetail', 'transactionPaymentID', 'transactionStatus', 'created_at']
 			],

 			'update' => [
 				'inherit' => 'default',
 				//'rules' => [
		 		//	'column1' => 'required|email',
 				//],
 				//'fillable' => ['column1', 'column2']
 			]
 		];
 	}

}

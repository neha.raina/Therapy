<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;
use App\Models\Transaction;
class UserCustomer extends Model
{
    /*
     *  table Name
     */
    protected $table='user_customers';
    /*
     *  primary key
     */
    protected $primaryKey='pkCustomerID';
    /*
     *  User Name
     */
    public function User () 
    {
        
       return $this->hasOne('App\Models\User','pkUserID');    
    }
    /*
     *  has many transaction
     */
    public function transactions ()
    {
        
        return $this->HasMany('App\Models\Transaction','fkCustomerID');
    }
}

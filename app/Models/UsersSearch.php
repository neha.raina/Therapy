<?php

namespace App\Models;

class UsersSearch extends Users
{
	public static $isSearchModel = true;
	public $searchItemPerPage = 3;
	public $paginationKey = 'page';

	public $searchAttributesUsingLike = ['userFirstName', 'userLastName'];
}

<?php
namespace App\Models;

class CustomersSearch extends Customer{
	public static $isSearchModel = true;
	public $searchItemPerPage = 3;
	public $paginationKey = 'page';
	public $searchAttributesUsingLike = ['customerProfilePic', 'customerAddress'];
}

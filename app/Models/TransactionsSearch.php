<?php
namespace App\Models;

class TransactionsSearch extends Transaction{
	public static $isSearchModel = true;
	public $searchItemPerPage = 3;
	public $paginationKey = 'page';
	public $searchAttributesUsingLike = ['transactionCurrency', 'transactionDetail', 'transactionPaymentID'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use App\Models\User;

/**
 *
 * 
 * Assumptions 
 * 	table name is = strtolower(modelClassName)
 * 	modelSearchClass name is = modelClassName + 'Search'
 * 	primaryKey is = 'pk' + singular(modelClassName) + 'ID'
 * 	
 **/
class BaseModel extends Model
{
	// const CREATED_AT = 'userCreatedAt';
	// const UPDATED_AT = 'userUpdatedAt';

	protected $table;
	public $primaryKey;
	public $modelSearchName;

	protected $fillable = [];	// current fillables
	protected $rules = [];		// current rules

	protected $errors = [];
	protected $customValidationMessage = [];

	public $scenario = 'default';

	public $searchItemPerPage = 10;
	public $paginationKey = 'page';
	public $searchAttributesUsingLike = [];

	public function __construct($childObject = null) 
	{

		parent::__construct();

		if($childObject) {

			$reflection = new \ReflectionClass($childObject);

			if (isset($childObject::$isSearchModel) && $childObject::$isSearchModel === true) {

				// its a search model
				if (empty($childObject->table))
					$childObject->table = str_replace('search', '', strtolower($reflection->getShortName()));
				
				if (empty($childObject->modelSearchName))
					$childObject->modelSearchName = $reflection->getShortName();

			} else {
				// it a table model
				if (empty($childObject->table))
					$childObject->table = strtolower($reflection->getShortName());
				
				if (empty($childObject->modelSearchName))
					$childObject->modelSearchName = $reflection->getShortName(). 'Search';
				
				if (empty($childObject->primaryKey))
					$childObject->primaryKey = sprintf('pk%sID', Str::singular($reflection->getShortName()));
			}
		
			$this->setScenario('default');	
		}

	}

 	/**
 	 * Contains list of all scenarios
 	 * This fucntion must be overwritten in child classes
 	 */
 	public function getScenarios()
 	{
 		return [
 			'default' => [
 				'rules' => [
 					// 'columnName' => 'rule1|rule2'
 				],
 				'fillable' => []	// columns for mass assign
 			]
 		];
 	}

 	/**
 	 * Returns rules from current scenario
 	 **/ 
 	public function getRules()
 	{

 		$scenarios = $this->getScenarios();
 		$currentScenarioName = $this->scenario;
		return $this->getScenarioRule($currentScenarioName);

 	}

 	/**
 	 * Get scenario rules
 	 * @param $scenarioName is valid scenario name and no further check is done for it
 	 * @return array of rule
 	 **/
 	public function getScenarioRule($scenarioName) 
 	{

 		$rules = [];
 		$scenarios = $this->getScenarios();

 		if (isset($scenarios[$scenarioName])) {

 			$rules = $scenarios[$scenarioName]['rules'];

 			// check if scenario is inherited
 			if (isset($scenarios[$scenarioName]['inherit'])) {
 				$parentRules = $this->getScenarioRule($scenarios[$scenarioName]['inherit']);
 				$rules = array_merge($parentRules, $rules);
 			}

	 		return $rules;
 		
 		} else
 			return [];
 	}

 	/**
	 * Sets current scenario
	 * Also sets current rules and fillable
	 * @param string $scenarioName
 	 **/
 	public function setScenario($scenarioName)
 	{
 		
 		$this->scenario = $scenarioName;
 		// $this->rules = $this->getRules();
 		$this->fillable = $this->getFillableColumns($scenarioName);
 		return $this;

 	}

 	/**
 	 * Returns fillable for current scenario
 	 * @param string $currentScenarioName
 	 **/
 	public function getFillableColumns($currentScenarioName) 
 	{
 		$scenarios = $this->getScenarios();
 		if (isset($scenarios[$currentScenarioName]) && isset($scenarios[$currentScenarioName]['fillable']))
 			return $scenarios[$currentScenarioName]['fillable'];
 		else
 			return [];
 	}

 	public static function attributeLabel($column) 
 	{
 		$labels = static::getLabel();

 		if (isset($labels[$column])) 
 			return $labels[$column];
 		elseif ($column == '*')
 			return $labels;
 		else
 			return $column;
 	}

 	public function getAttributes()
 	{
 		return $this->attributes;
 	}

 	public function validate() 
 	{
 		$validation = Validator::make($this->getAttributes(), $this->getRules(), $this->customValidationMessage);
 		$this->errors = $validation->messages();
 		return $validation->passes();
 	}

 	public function getErrors()
 	{
 		return $this->errors;
 	}

 	/**
 	 * Returns all error for an attruibute
 	 **/
 	public function getError($attributeName)
 	{

 	}

 	public function search1($modelName)
 	{ 

		$searchParameters = array_filter(Request::input($this->modelSearchName, array()));

		$paginatedResult = null;
		$this->forceFill($searchParameters);

 		if (!empty($searchParameters)) {
 			
 			$result = null;

 			foreach ($searchParameters as $key => $value) {
				if (!$result){
					if (in_array($key, $this->searchAttributesUsingLike))
						$result = $modelName::where($key, 'like', '%'. $value. '%');
					else
						$result = $modelName::where($key, $value);
					
				}
				else {
					if (in_array($key, $this->searchAttributesUsingLike))
						$result = $result->where($key, 'like', '%'. $value. '%');
					else
						$result = $result->where($key, $value);
					
				}
 			}

			$paginatedResult = $result->paginate($this->searchItemPerPage, ['*'], $this->paginationKey);

 		} else 
 			$paginatedResult = $modelName::paginate($this->searchItemPerPage, ['*'], $this->paginationKey);

		return $paginatedResult;

 	}

 	public function search($modelName)
 	{

		$searchParameters = array_filter(Request::input($this->modelSearchName, array()));

		$paginatedResult = null;
		$this->forceFill($searchParameters);

 		if (!empty($searchParameters)) {
 			
 			$result = null;

 			foreach ($searchParameters as $key => $value) {
				if (!$result){
					if (in_array($key, $this->searchAttributesUsingLike))
						$result = $modelName::where($key, 'like', '%'. $value. '%');
					else
						$result = $modelName::where($key, $value);
					
				}
				else {
					if (in_array($key, $this->searchAttributesUsingLike))
						$result = $result->where($key, 'like', '%'. $value. '%');
					else
						$result = $result->where($key, $value);
					
				}
 			}

			$paginatedResult = $result->paginate($this->searchItemPerPage, ['*'], $this->paginationKey);

 		} else {
 			if (is_string($modelName)) {
 				# code...
 				$paginatedResult = $modelName::paginate($this->searchItemPerPage, ['*'], $this->paginationKey);
 			} else
 				$paginatedResult = $modelName->paginate($this->searchItemPerPage, ['*'], $this->paginationKey);
 		}

		return $paginatedResult;

 	}
 	/**
 	 * Loads current model with fillable from current scenario
 	 * @param array $params contains key value pair of data to be filled into current model
 	 **/
 	public function load($params)
 	{
 		if (is_array($params))
	 		foreach ($this->fillable as $key) {
	 			if (array_key_exists($key, $params))
	 				$this->setAttribute( $key, $params[$key] );
	 		}
	 	return $this;
 	}

 	public function findOne()
 	{
 		
 	}

}

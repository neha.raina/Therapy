<?php

return [
    'user' => [
        'model' => 'App\Models\User',
    ],
    'broadcast' => [
        'enable' => false,
        'app_name' => 'your-app-name',
        'pusher' => [
            'app_id' => '123456',
            'app_key' => 'yadav',
            'app_secret' => 'yadav',
        ],
    ],
];

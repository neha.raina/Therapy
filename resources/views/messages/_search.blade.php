@extends('common-crud._search')

@section('form')

	{{ Form::model($searchModel, ['route' => ['message.index', ''], 'class' => '', 'role' => 'form', 'method' => 'get']) }}

		<div class="row">

			<div class="col-md-3">
				{{Form::textInput($searchModel, 'pkMessagesID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'message')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'is_seen')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'deleted_from_sender')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'deleted_from_receiver')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'user_id')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'conversation_id')}}
			</div>
					
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12">
				{{ Form::submit(___('SEARCH'), ['class' => 'btn btn-success']) }}
				&nbsp; 
				<a href="{{URL::route($baseRouteName. '.index')}}" class="btn btn-default">{{ ___('RESET') }}</a>
			</div>
		</div>

    {{ Form::close() }}
	
@endsection
<?php $route = 'message.';
$idValue = null;
$method = null;
$isUpdate = false;
if (!$model->exists) {
	$route .= 'store';
	$method = 'POST';
} else {
	$isUpdate = true;
	$route .= 'update';
	$method = 'PATCH';
	$idValue = $model->{$model->primaryKey};
}
?>

{{ Form::model($model, ['route' => [$route, $idValue], 'method' => $method])}}
	{{ csrf_field() }}
	<div class="panel panel-default panel-table">
		<div class="panel-heading">
			<h4>{{($isUpdate)?___('Update'):___('New')}} Message</h4>
		</div>
		<div class="panel-body">
			<div class="row">

			<div class="col-md-3">
				{{Form::areaInput($model, 'message', ['rows' => 5])}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'is_seen')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'deleted_from_sender')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'deleted_from_receiver')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'user_id')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'conversation_id')}}
			</div>


			</div>

			<div class="row">
				<div class="col-md-12">
					<br>
					<button type="submit" class="btn btn-primary" >{{___('SAVE')}}</button>
					&nbsp; 
					<a href="{{URL::route($baseRouteName. '.index')}}" class="btn btn-default">{{___('BACK')}}</a>
				</div>
			</div>
		</div>
	</div>

{{ Form::close() }}

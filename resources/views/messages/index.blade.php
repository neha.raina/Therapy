@include('common-crud.index', [
	'pageTitle' => ___('Message List'),
	'pageHeading' => ___('Message List'),
	'listWidgetHeading' => ___('Message List'),
	
	'baseRouteName' => 'message',
	'searchRoute' => 'messages._search',

	'modelName' => '\App\Models\Messages',
	'models' => $models,
	'searchModel' => $searchModel,

	'columns' => [
		[
			'type' => 'index',
			'label' => '#',
		],
		'pkMessagesID',
		'message',
		'is_seen',
		'deleted_from_sender',
		'deleted_from_receiver',
		'user_id',
		'conversation_id',
		'created_at',
		[
			'type' => 'action',
		]
	]

])
@include('common-crud.show', 
	[
		'wigetTitle' => 'User Details',
		'model' => $messages,
		'columns' => [
			'pkUserID',
			'userEmail',
			'userFirstName',
			'userLastName',
			'userGender'
		]
	])

@include('common-crud.index', [
	'pageTitle' => ___('product List'),
	'pageHeading' => ___('product List'),
	'listWidgetHeading' => ___('product List'),
	
	'baseRouteName' => 'product',
	'searchRoute' => 'products._search',

	'modelName' => '\App\Models\products',
	'models' => $models,
	'searchModel' => $searchModel,

	'columns' => [
		[
			'type' => 'index',
			'label' => '#',
		],
		'pkProductID',
		'productName',
		'productDescription',
		'productPrice',
		'productImage',
		'productStatus',
		'created_at',
		'updated_at',
		[
			'type' => 'action',
		]
	]

])
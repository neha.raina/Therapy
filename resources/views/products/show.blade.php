@include('common-crud.show', 
	[
		'wigetTitle' => 'User Details',
		'model' => $products,
		'columns' => [
			'pkUserID',
			'userEmail',
			'userFirstName',
			'userLastName',
			'userGender'
		]
	])

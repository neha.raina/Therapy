@extends('common-crud._search')

@section('form')

	{{ Form::model($searchModel, ['route' => ['product.index', ''], 'class' => '', 'role' => 'form', 'method' => 'get']) }}

		<div class="row">

			<div class="col-md-3">
				{{Form::textInput($searchModel, 'pkProductID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'productName')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'productDescription')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'productPrice')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'productImage')}}
			</div>
			<div class="col-md-3">
				{{Form::dropDown($searchModel, 'productStatus', [
					'Active' => ___('Active'),
					'Inactive' => ___('Inactive'),
				])}}
			</div>
					
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12">
				{{ Form::submit(___('SEARCH'), ['class' => 'btn btn-success']) }}
				&nbsp; 
				<a href="{{URL::route($baseRouteName. '.index')}}" class="btn btn-default">{{ ___('RESET') }}</a>
			</div>
		</div>

    {{ Form::close() }}
	
@endsection
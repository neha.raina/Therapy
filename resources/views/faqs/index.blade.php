@include('common-crud.index', [
	'pageTitle' => ___('Faq List'),
	'pageHeading' => ___('Faq List'),
	'listWidgetHeading' => ___('Faq List'),
	
	'baseRouteName' => 'faq',
	'searchRoute' => 'faqs._search',

	'modelName' => '\App\Models\Faqs',
	'models' => $models,
	'searchModel' => $searchModel,

	'columns' => [
		[
			'type' => 'index',
			'label' => '#',
		],
		'pkFaqID',
		'faqQuestion',
		'faqAnswer',
		'faqStatus',
		'created_at',
		'updated_at',
		[
			'type' => 'action',
		]
	]

])
@include('common-crud.show', 
	[
		'wigetTitle' => 'User Details',
		'model' => $faqs,
		'columns' => [
			'pkUserID',
			'userEmail',
			'userFirstName',
			'userLastName',
			'userGender'
		]
	])

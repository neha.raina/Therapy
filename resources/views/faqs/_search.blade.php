@extends('common-crud._search')

@section('form')

	{{ Form::model($searchModel, ['route' => ['faq.index', ''], 'class' => '', 'role' => 'form', 'method' => 'get']) }}

		<div class="row">

			<div class="col-md-3">
				{{Form::textInput($searchModel, 'pkFaqID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'faqQuestion')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'faqAnswer')}}
			</div>
			<div class="col-md-3">
				{{Form::dropDown($searchModel, 'faqStatus', [
					'Active' => ___('Active'),
					'Inactive' => ___('Inactive'),
				])}}
			</div>
					
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12">
				{{ Form::submit(___('SEARCH'), ['class' => 'btn btn-success']) }}
				&nbsp; 
				<a href="{{URL::route($baseRouteName. '.index')}}" class="btn btn-default">{{ ___('RESET') }}</a>
			</div>
		</div>

    {{ Form::close() }}
	
@endsection
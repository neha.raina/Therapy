	<?php

		// update name of all form elements
		$modelClassName = substr( get_class($model), strrpos(get_class($model), '\\') + 1);
		$formControlName = $modelClassName. '['. $name. ']';
		$attributes['name'] = $formControlName;
		$attributes['id'] = $modelClassName. '_'. $name;

		$attributeError = null;
		// if (Session::has('error'))
		// 	$attributeError = Session::get('error')->first($name);

?>
<div class="form-group {{((!empty($attributeError))?'has-error':'')}}">
	@if ($type != 'hidden')

    	{{ Form::label($name, ((empty($model))?null:$model->attributeLabel($name)), ['class' => 'control-label']) }}
	
	@endif


	@if ($type == 'text')
	
	    {{ Form::text($name, (empty($attributes['value']))?null:$attributes['value'], array_merge(['class' => 'form-control'], $attributes)) }}

	@elseif ($type == 'area')
	
	    {{ Form::textarea($name, (empty($attributes['value']))?null:$attributes['value'], array_merge(['class' => 'form-control'], $attributes)) }}

	@elseif ($type == 'hidden')
	
	    {{ Form::hidden($name, (empty($attributes['value']))?null:$attributes['value'], array_merge(['class' => 'form-control'], $attributes)) }}

	@elseif ($type == 'password')
	
	    {{ Form::password($name, array_merge(['class' => 'form-control'], $attributes)) }}
	
	@elseif ($type == 'file')
	
	    {{ Form::file($name, array_merge(['class' => 'form-control'], $attributes)) }}
	
	@elseif ($type == 'select')

	    {{ Form::select($name, $options, null, array_merge(['class' => 'form-control'], $attributes)) }}

	@endif
	 
	@if (!empty($attributeError))
		<div class="error">{{$attributeError}}</div>
	@endif
</div>
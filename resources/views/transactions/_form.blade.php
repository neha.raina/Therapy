<?php $route = 'transaction.';
$idValue = null;
$method = null;
$isUpdate = false;
if (!$model->exists) {
	$route .= 'store';
	$method = 'POST';
} else {
	$isUpdate = true;
	$route .= 'update';
	$method = 'PATCH';
	$idValue = $model->{$model->primaryKey};
}
?>

{{ Form::model($model, ['route' => [$route, $idValue], 'method' => $method])}}
	{{ csrf_field() }}
	<div class="panel panel-default panel-table">
		<div class="panel-heading">
			<h4>{{($isUpdate)?___('Update'):___('New')}} Transaction</h4>
		</div>
		<div class="panel-body">
			<div class="row">

			<div class="col-md-3">
				{{Form::textInput($model, 'fkCustomerID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'fkTherapistID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'transactionAmount')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'transactionCurrency')}}
			</div>
			<div class="col-md-3">
				{{Form::dropDown($model, 'transactionMethod', [
					'PayPal' => ___('PayPal'),
					'COD' => ___('COD'),
					'Stripe' => ___('Stripe'),
				])}}
			</div>
			<div class="col-md-3">
				{{Form::areaInput($model, 'transactionDetail', ['rows' => 5])}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'transactionPaymentID')}}
			</div>
			<div class="col-md-3">
				{{Form::dropDown($model, 'transactionStatus', [
					'completed' => ___('completed'),
					'pending' => ___('pending'),
				])}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'created_at')}}
			</div>


			</div>

			<div class="row">
				<div class="col-md-12">
					<br>
					<button type="submit" class="btn btn-primary" >{{___('SAVE')}}</button>
					&nbsp; 
					<a href="{{URL::route($baseRouteName. '.index')}}" class="btn btn-default">{{___('BACK')}}</a>
				</div>
			</div>
		</div>
	</div>

{{ Form::close() }}

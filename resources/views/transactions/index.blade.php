@include('common-crud.index', [
	'pageTitle' => ___('Transaction List'),
	'pageHeading' => ___('Transaction List'),
	'listWidgetHeading' => ___('Transaction List'),
	
	'baseRouteName' => 'transaction',
	'searchRoute' => 'transactions._search',

	'modelName' => '\App\Models\Transactions',
	'models' => $models,
	'searchModel' => $searchModel,

	'columns' => [
		[
			'type' => 'index',
			'label' => '#',
		],
		'pkTransactionID',
		'fkCustomerID',
		'fkTherapistID',
		'transactionAmount',
		'transactionCurrency',
		'transactionMethod',
		'transactionDetail',
		'transactionPaymentID',
		'transactionStatus',
		'created_at',
		[
			'type' => 'action',
		]
	]

])
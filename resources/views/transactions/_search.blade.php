@extends('common-crud._search')

@section('form')

	{{ Form::model($searchModel, ['route' => ['transaction.index', ''], 'class' => '', 'role' => 'form', 'method' => 'get']) }}

		<div class="row">

			<div class="col-md-3">
				{{Form::textInput($searchModel, 'pkTransactionID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'fkCustomerID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'fkTherapistID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'transactionAmount')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'transactionCurrency')}}
			</div>
			<div class="col-md-3">
				{{Form::dropDown($searchModel, 'transactionMethod', [
					'PayPal' => ___('PayPal'),
					'COD' => ___('COD'),
					'Stripe' => ___('Stripe'),
				])}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'transactionDetail')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'transactionPaymentID')}}
			</div>
			<div class="col-md-3">
				{{Form::dropDown($searchModel, 'transactionStatus', [
					'completed' => ___('completed'),
					'pending' => ___('pending'),
				])}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'created_at')}}
			</div>
					
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12">
				{{ Form::submit(___('SEARCH'), ['class' => 'btn btn-success']) }}
				&nbsp; 
				<a href="{{URL::route($baseRouteName. '.index')}}" class="btn btn-default">{{ ___('RESET') }}</a>
			</div>
		</div>

    {{ Form::close() }}
	
@endsection
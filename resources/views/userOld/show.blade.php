@include('common-crud.show', 
	[
		'wigetTitle' => 'User Details',
		'model' => $user,
		'columns' => [
			'pkUserID',
			'userEmail',
			'userFirstName',
			'userLastName',
			'userGender'
		]
	])

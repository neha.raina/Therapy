<?php
$route = 'user.';
$idValue = null;
$method = null;
$isUpdate = false;
if (!$model->exists) {
	$route .= 'store';
	$method = 'POST';
} else {
	$isUpdate = true;
	$route .= 'update';
	$method = 'PATCH';
	$idValue = $model->{$model->primaryKey};
}
?>

{{ Form::model($model, ['route' => [$route, $idValue], 'method' => $method])}}
	{{ csrf_field() }}
	<div class="panel panel-default panel-table">
		<div class="panel-heading">
			<h4>{{($isUpdate)?___('Update'):___('New')}} User</h4>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					{{Form::textInput($model, 'userEmail')}}
				</div>

				<div class="col-md-4">
					{{Form::passwordInput($model, 'userPassword')}}
				</div>

				<div class="col-md-4">
					{{Form::textInput($model, 'userFirstName')}}
				</div>

				<div class="col-md-4">
					{{Form::textInput($model, 'userLastName')}}
				</div>

				<div class="col-md-4">
					{{Form::dropDown($model, 'userGender', [
						'Male' => ___('Male'),
						'Female' => ___('Female'),
						'Trans' => ___('Trans')
					])}}
				</div>

				<div class="col-md-4">
					{{Form::fileInput($model, 'userImage')}}
				</div>

				<div class="col-md-4">
					{{Form::dropDown($model, 'userRole', [
						'Admin' => ___('Admin'),
						'Sub-Admin' => ___('Sub-Admin'),
						'Company Admin' => ___('Company Admin'),
						'Company Sub-Admin' => ___('Company Sub-Admin'),
						'Employee' => ___('Employee')
					])}}
				</div>

				<div class="col-md-4">
					{{Form::dropDown($model, 'userStatus', [
						'Active' => ___('Active'),
						'Under Verification' => ___('Under Verification'),
						'Suspended' => ___('Suspended'),
						'Deleted' => ___('Deleted'),
					])}}
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<br>
					<button type="submit" class="btn btn-primary" >{{___('SAVE')}}</button>
					&nbsp; 
					<a href="{{URL::route($baseRouteName. '.index')}}" class="btn btn-default">{{___('BACK')}}</a>
				</div>
			</div>
		</div>
	</div>

{{ Form::close() }}

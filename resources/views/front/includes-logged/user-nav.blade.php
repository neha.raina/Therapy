<section class="sidebar-container">
    <div class="container-fluid dashboardSetting">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-4 pd0 dynamicHeight">
                <div class="top-leftbar">
                    <figure class="text-center">
                        <a href="{{ route('customer-dashboard') }}">
                            <img src="{{ asset('images/logo.png') }}" alt="logo">
                        </a>
                    </figure>
                </div><!-- .top-leftbar -->
                <div class="sidebar">
                    <ul>
                        <li {{ setActive('customer-dashboard') }} >

                            <a href="{{ route('customer-dashboard')}}"><figure><img src="images/dashboard.png" alt="dashboard"></figure> Dashboard</a>
                        </li>
                        <li {{ setActive('messages') }} >

                            <a href="{{ route('messages')}} "><figure><img src="images/messages.png" alt="messages"></figure> Messages <span class="badge">20</span></a>
                        </li>
                        <li {{ setActive('my-bookings') }} >

                            <a href="{{ route('my-bookings')}}"><figure><img src="images/bookings.png" alt="bookings"></figure> My Bookings <span class="badge">5</span></a>
                        </li>
                        <li {{ setActive('my-therapist') }} >

                            <a href="{{ route('my-therapist')}}"><figure><img src="images/therapists.png" alt="therapists"></figure> My Therapists <span class="badge">2</span></a>
                        </li>
                        <li {{ setActive('transaction') }} >

                            <a href="{{ route('transaction')}}"><figure><img src="images/transaction.png" alt="transaction"></figure> Transaction History</a>
                        </li>
<!--                        <li><a href="{{ route('logout')}}"><figure><img src="images/logout.png" alt="logout"></figure> Log Out</a></li>-->
                        <li>

                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form><figure><img src="images/logout.png" alt="dashboard"></figure> Log Out</a>

                        </li>
                    </ul>
                </div><!-- .sidebar -->
            </div><!-- .col-md-3 -->

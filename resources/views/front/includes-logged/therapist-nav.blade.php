<section class="sidebar-container">
    <div class="container-fluid dashboardSetting">
        <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-4 pd0 dynamicHeight">
                <div class="top-leftbar">
                    <figure class="text-center">
                        <a href="{{  route('therapist-dashboard') }}">
                            <img src="{{ asset ('images/logo.png') }}" alt="Logo">
                        </a>
                    </figure>
                </div><!-- .top-leftbar -->
                <div class="sidebar">
                    <ul>
                        <li {{ setActive('therapist-dashboard') }} >

                            <a href="{{ route('therapist-dashboard')}}"><figure><img src="images/dashboard_grey.png" alt="dashboard"></figure> Dashboard</a>
                        </li>
                        <li {{ setActive('therapist-messages') }} >

                            <a href="{{ route('therapist-messages')}}"><figure><img src="images/messages.png" alt="messages"></figure> Messages <span class="badge">20</span></a>
                        </li>
                        <li {{ setActive('therapist-bookings') }} >

                            <a href="{{ route('therapist-bookings')}}"><figure><img src="images/bookings.png" alt="bookings"></figure> My Bookings <span class="badge">5</span></a>
                        </li>
                        <li {{ setActive('therapist-schedules') }} >

                            <a href="{{ route('therapist-schedules')}}"><figure><img src="images/schedules.png" alt="schedules"></figure> My Schedules <span class="badge">5</span></a>
                        </li>
                        <li {{ setActive('store') }} >

                            <a href="{{ route('store')}}"><figure><img src="images/store.png" alt="store"></figure> Store <span class="badge">2</span></a>
                        </li>
                        <li {{ setActive('therapist-insurance') }} >

                            <a href="{{ route('therapist-insurance')}}"><figure><img src="images/insurance.png" alt="insurance"></figure> Insurance <span class="badge">2</span></a>
                        </li>
                        <li {{ setActive('therapist-transaction') }} >

                            <a href="{{ route('therapist-transaction')}}"><figure><img src="images/transaction.png" alt="transaction"></figure> Transaction History</a>
                        </li>
                        <li>

                            <a href="{{ route('logout')}}"><figure><img src="images/logout.png" alt="logout"></figure> Log Out</a>
                        </li>
                    </ul>
                </div><!-- .sidebar -->
            </div><!-- .col-md-3 -->
<section class="newsletter">
        <div class="container">
          <div class="row">
              <div class="newsletter-container">
                <div class="col-md-4">
                  <div class="newsletter-box">
                    <h3>Stay in Touch</h3>
                    <p>Subscribe to our Newsletter</p>
                  </div>
                </div><!-- .col-md-4 -->
                <div class="col-md-8">
                  <form class="form-horizontal">
                    <div class="form-group">
                      <input type="email" class="form-control" id="email" placeholder="Your Email">
                      <button type="submit" class="btn form-button">Subscribe</button>
                    </div>
                  </form>
                </div><!-- .col-md-8 -->
              </div><!-- .newsletter-container -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .newsletter -->
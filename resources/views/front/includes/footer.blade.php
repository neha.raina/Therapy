<footer>
        <div class="container">
          <div class="row">
              <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="index.html">
                  <figure>
                    <img src="{{url('images/dark-logo.jpg')}}" alt="Logo">
                  </figure>
                </a>
                <ul class="social-buttons">
                  <li><a href=""><img src="{{url('images/facebook.png')}}" alt="Facebook"></a></li>
                  <li><a href=""><img src="{{url('images/twitter.png')}}" alt="Twitter"></a></li>
                  <li><a href=""><img src="{{url('images/linkedin.png')}}" alt="Linkedin"></a></li>
                  <li><a href=""><img src="{{url('images/google_plus.png')}}" alt="Google Plus"></a></li>
                </ul>
                <ul class="contact-buttons">
                  <li><a href=""><figure><img src="{{url('images/phone.jpg')}}" alt="Phone"></figure></span>888-972-6211</a></li>
                  <li><a href=""><figure><img src="{{url('images/mail.jpg')}}" alt="Mail"></figure>info@dummytext.com</a></li>
                </ul>
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="ul-container first-ul">
                  <h4>Conditions</h4>
                  <ul>
                    <li><a href="">Cognitive Behaviour</a></li>
                    <li><a href="">Depression Help</a></li>
                    <li><a href="">Anxiety Help</a></li>
                    <li><a href="">Panic Attacks</a></li>
                    <li><a href="">Stres Relief</a></li>
                    <li><a href="">Eating Disorders</a></li>
                    <li><a href="">Grief Support</a></li>
                  </ul>
                </div><!-- .ul-container -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="ul-container second-ul">
                  <h4 class="less-margin">Useful Links</h4>
                  <ul>
                    <li><a href="">Experts</a></li>
                    <li><a href="">Discussions</a></li>
                    <li><a href="">Most asked questions</a></li>
                    <li><a href="">Motivational Quotes</a></li>
                    <li><a href="">Articles</a></li>
                    <li><a href="">De-stressing techniques</a></li>
                    <li><a href="">Press</a></li>
                  </ul>
                </div><!-- .ul-container -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="ul-container third-ul">
                  <h4>About Us</h4>
                  <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="">Blog</a></li>
                    <li><a href="">News</a></li>
                    <li><a href="">Our Therapist</a></li>
                  </ul>
                </div><!-- .ul-container -->
              </div><!-- .col-md-3 -->
          </div><!-- .row -->
        </div><!-- .container -->
        <div class="bottom-footer">
          <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                  <p><i class="fa fa-copyright" aria-hidden="true"></i> All Copyright | reserved to Text Therapy</p>
                </div><!-- .col-md-6 -->
                <div class="col-md-6 col-sm-6 text-right">
                  <ul>
                    <li><a href="index.html">Terms of Service</a></li>
                    <li><a href="">Privacy Policy</a></li>
                    <li><a href="">Faq's</a></li>
                  </ul>
                </div><!-- .col-md-6 -->
            </div><!-- .row -->
          </div><!-- .container -->
        </div><!-- .bottom-footer -->
      </footer>
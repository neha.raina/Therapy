<header class="header">
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <i class="fa fa-bars"></i>
              </button>
              <a class="navbar-brand" href="{{url('/')}}">
                <figure>
                    <img src="{{url('images/logo.png')}}" alt="Logo">
                </figure>
              </a>
            </div> <!--.navbar-header-->
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav text-uppercase pull-right">
                <li class="active"><a href="">About Us</a></li>
                <li><a href="">My Therapist</a></li>
                <li><a href="">Services</a></li>
                <li><a href="">Blog</a></li>
                <li><a href="">Faq's</a></li>
                <li><a href="{{ url('/login') }}"><figure><img src="{{url('images/login.png')}}"></figure>Login</a></li>
                <li><a href="{{ url('/register') }}"><figure><img src="{{url('images/signup.png')}}"></figure>Signup</a></li>
              </ul>
            </div><!-- .nav-collapse -->
          </div><!-- .container-fluid -->
        </nav>
      </header>
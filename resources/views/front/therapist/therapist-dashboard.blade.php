@extends('front.layouts.app')

@section('content') 



    <div class="rightbar">
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"><a href="#">Account Settings</a></li>
        </ul>
        <div class="container-fluid therapistContainer">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="rightbar-box">
                        <figure>
                            <img src="images/therapist-messages.jpg" alt="Message">
                        </figure>
                        <h3 class="text-center">
                            <span class="span-messages">9</span>
                            Total Messages
                        </h3>
                    </div><!-- .rightbar-box -->
                </div><!-- .col-md-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="rightbar-box">
                        <figure>
                            <img src="images/therapist-bookings.jpg" alt="bookings">
                        </figure>
                        <h3 class="text-center">
                            <span class="span-bookings">7</span>
                            Total Bookings
                        </h3>
                    </div><!-- .rightbar-box -->
                </div><!-- .col-md-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="rightbar-box">
                        <figure>
                            <img src="images/therapist-schedules.jpg" alt="schedules">
                        </figure>
                        <h3 class="text-center">
                            <span class="span-schedules">5</span>
                            Total Schedules
                        </h3>
                    </div><!-- .rightbar-box -->
                </div><!-- .col-md-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="rightbar-box">
                        <figure>
                            <img src="images/therapist-cart.jpg" alt="cart">
                        </figure>
                        <h3 class="text-center">
                            <span class="span-cart">3</span>
                            Cart Items
                        </h3>
                    </div><!-- .rightbar-box -->
                </div><!-- .col-md-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="rightbar-box">
                        <figure>
                            <img src="images/therapist-results.jpg" alt="results">
                        </figure>
                        <h3 class="text-center">
                            <span class="span-results">2</span>
                            Total Stores
                        </h3>
                    </div><!-- .rightbar-box -->
                </div><!-- .col-md-4 -->
            </div><!-- .row -->
        </div><!-- .container-fluid -->
    </div><!-- .rightbar -->
</div><!-- .col-md-9 -->
</div><!-- .row -->
</div><!-- .container-fluid -->
</section><!-- .sidebar-container -->

@endsection
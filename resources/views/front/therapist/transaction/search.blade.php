<div class="col-md-12">
    <div class="searchContainer">
            {!! Form::open(['method' => 'get','route' => 'search', 'class'=>'form-inline']) !!}
            
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 col-sm-6 pd0">
                        <div class="form-group">
                            <label for="name">Transaction Amount</label><br>
<!--                            <input type="text" class="form-control" id="name" placeholder="">-->
                             {!! Form::text('transactionAmount', null, ['placeholder' => '','class' => 'form-control', 'name'=>'SearchTansaction[transactionAmount]']) !!}
                        </div><!-- .form-group -->
                    </div><!-- .col-md-3 -->
                    <div class="col-md-3 col-sm-6 pd0">
                        <div class="form-group">
                            <label for="transaction">Transaction ID</label><br>
                            <!--<input type="text" class="form-control" id="transaction" placeholder="">-->
                            {!! Form::text('transactionPaymentID', null, ['placeholder' => '','class' => 'form-control', 'name'=>'SearchTansaction[transactionPaymentID]']) !!}
                        </div><!-- .form-group -->
                    </div><!-- .col-md-3 -->
                    <div class="col-md-3 col-sm-6 pd0">
                        <div class="form-group">
                            <label for="booking">Booking Date</label><br>
                            <!--<input type="text" class="form-control" id="booking" placeholder="">-->
                             {!! Form::text('created_at', null, ['placeholder' => '','class' => 'form-control','name'=>'SearchTansaction[created_at]']) !!}
                        </div><!-- .form-group -->
                    </div><!-- .col-md-3 -->
                    <div class="col-md-3 col-sm-6 pd0">
                        <div class="form-group">
<!--                            <button type="button" class="btn btn-first">Search</button>-->
                            {!! Form::submit('Search', ['class' => 'btn btn-first']) !!}
                        </div><!-- .form-group -->
                    </div><!-- .col-md-3 -->
                </div><!-- .row -->
            </div><!-- .container-fluid -->
         {!! Form::close() !!}<!-- .form-inline -->
    </div><!-- .searchContainer -->
</div><!-- .col-md-12 -->
@extends('front.layouts.app')

@section('content')  
       
            
                <div class="rightbar">
                  <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Stores</li>
                  </ul>
                  <div class="sidebarRight">
                      <div class="storePage">
                          <div class="searchSection">
                              <form>
                                  <div class="row">
                                  <div class="col-md-3 col-sm-6">
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Product Name</label>
                                        <input id="exampleInputEmail1" type="text" class="form-control" placeholder="Product Name">
                                      </div>
                                  </div>
                                  <div class="col-md-3 col-sm-6">
                                      <div class="form-group">
                                        <label for="exampleSelect1">Price</label>
                                        <select id="exampleSelect1" class="form-control">
                                          <option>$200</option>
                                          <option>$300</option>
                                          <option>$350</option>
                                        </select>
                                      </div>
                                  </div>
                                  <div class="col-md-3 col-sm-6">
                                    <div class="form-group">
                                      <button type="submit" class="btn btn-search">Search</button>
                                    </div>
                                  </div>
                                  <div class="col-md-3 col-sm-6">
                                      <div class="form-group">
                                        <label id="exampleSelect2">Sort By</label>
                                        <select name="exampleSelect2" class="form-control">
                                          <option>New &amp; Popular</option>
                                          <option>New &amp; Popular2</option>
                                          <option>New &amp; Popular3</option>
                                        </select>
                                      </div>
                                  </div>
                              </div>
                              </form>
                          </div>
                          <div class="productList">
                              <div class="row">
                                  <div class="col-lg-4 col-md-6 col-sm-12">
                                      <div class="item">
                                        <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="images/prd1.jpg" alt="...">
                                        </div>
                                        <div class="media-body">
                                          <h4 class="media-heading">Product Name</h4>
                                          <h6>Price:<span>$39</span></h6>
                                          <p>ipsum dolor sit amconsectetur adipiscing elit. Aenean </p>
                                          <a href="" class="btn btn-buyProduct">Buy Product</a>
                                        </div>
                                      </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-4 col-md-6 col-sm-12">
                                      <div class="item">
                                        <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="images/prd2.jpg" alt="...">
                                        </div>
                                        <div class="media-body">
                                          <h4 class="media-heading">Product Name</h4>
                                          <h6>Price:<span>$39</span></h6>
                                          <p>ipsum dolor sit amconsectetur adipiscing elit. Aenean </p>
                                          <a href="" class="btn btn-buyProduct">Buy Product</a>
                                        </div>
                                      </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-4 col-md-6 col-sm-12">
                                      <div class="item">
                                        <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="images/prd3.jpg" alt="...">
                                        </div>
                                        <div class="media-body">
                                          <h4 class="media-heading">Product Name</h4>
                                          <h6>Price:<span>$39</span></h6>
                                          <p>ipsum dolor sit amconsectetur adipiscing elit. Aenean </p>
                                          <a href="" class="btn btn-buyProduct">Buy Product</a>
                                        </div>
                                      </div>
                                      </div>
                                  </div>
                                   <div class="col-lg-4 col-md-6 col-sm-12">
                                      <div class="item">
                                        <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="images/prd3.jpg" alt="...">
                                        </div>
                                        <div class="media-body">
                                          <h4 class="media-heading">Product Name</h4>
                                          <h6>Price:<span>$39</span></h6>
                                          <p>ipsum dolor sit amconsectetur adipiscing elit. Aenean </p>
                                          <a href="" class="btn btn-buyProduct">Buy Product</a>
                                        </div>
                                      </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-4 col-md-6 col-sm-12">
                                      <div class="item">
                                        <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="images/prd1.jpg" alt="...">
                                        </div>
                                        <div class="media-body">
                                          <h4 class="media-heading">Product Name</h4>
                                          <h6>Price:<span>$39</span></h6>
                                          <p>ipsum dolor sit amconsectetur adipiscing elit. Aenean </p>
                                          <a href="" class="btn btn-buyProduct">Buy Product</a>
                                        </div>
                                      </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-4 col-md-6 col-sm-12">
                                      <div class="item">
                                        <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="images/prd2.jpg" alt="...">
                                        </div>
                                        <div class="media-body">
                                          <h4 class="media-heading">Product Name</h4>
                                          <h6>Price:<span>$39</span></h6>
                                          <p>ipsum dolor sit amconsectetur adipiscing elit. Aenean </p>
                                          <a href="" class="btn btn-buyProduct">Buy Product</a>
                                        </div>
                                      </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-4 col-md-6 col-sm-12">
                                      <div class="item">
                                        <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="images/prd2.jpg" alt="...">
                                        </div>
                                        <div class="media-body">
                                          <h4 class="media-heading">Product Name</h4>
                                          <h6>Price:<span>$39</span></h6>
                                          <p>ipsum dolor sit amconsectetur adipiscing elit. Aenean </p>
                                          <a href="" class="btn btn-buyProduct">Buy Product</a>
                                        </div>
                                      </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-4 col-md-6 col-sm-12">
                                      <div class="item">
                                        <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="images/prd3.jpg" alt="...">
                                        </div>
                                        <div class="media-body">
                                          <h4 class="media-heading">Product Name</h4>
                                          <h6>Price:<span>$39</span></h6>
                                          <p>ipsum dolor sit amconsectetur adipiscing elit. Aenean </p>
                                          <a href="" class="btn btn-buyProduct">Buy Product</a>
                                        </div>
                                      </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-4 col-md-6 col-sm-12">
                                      <div class="item">
                                        <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="images/prd1.jpg" alt="...">
                                        </div>
                                        <div class="media-body">
                                          <h4 class="media-heading">Product Name</h4>
                                          <h6>Price:<span>$39</span></h6>
                                          <p>ipsum dolor sit amconsectetur adipiscing elit. Aenean </p>
                                          <a href="" class="btn btn-buyProduct">Buy Product</a>
                                        </div>
                                      </div>
                                      </div>
                                  </div>


                              </div>
                          </div>
                      </div>
                  </div>

                  </div><!-- .rightbar-content -->
                </div><!-- .rightbar -->
           </div><!-- .row -->
         </div><!-- .container-fluid -->
       </section><!-- .sidebar-container -->

      @endsection

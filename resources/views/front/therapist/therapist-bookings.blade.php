@extends('front.layouts.app')

@section('content') 


         
              <div class="rightbar mybookingsRightbar">
                <ul class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li class="active"><a href="#">My Bookings</a></li>
                </ul>
                <div class="container-fluid therapistContainer mybookingsContainer">
                  <div class="row">
                    <div class="col-lg-3 col-md-6">
              				<div class="result-container">
              					<figure>
              						<img class="profile-img" src="images/dashboard-1.jpg" alt="profile image">
              					</figure>
              					<div class="profile">
                          <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>
                              Rosan Martin<span class="pull-right text-booked">Booked</span></h3>
              						<p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="cal-box">
                            <figure><img src="images/calendar.jpg" alt="calendar"></figure>
                            <p>9 Mar 2016, Thursday</p>
                            <figure class="time"><img src="images/time.jpg" alt="time"></figure>
                            <p>09:30 AM</p>
                          </div>
              						<div class="booking">
              							<a href="" class="btn btn-third">Cancel Booking</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="camera"><a href=""></a></li>
                              </ul>
                            </div>
              						</div>
              					</div><!-- .profile -->
              				</div><!-- .result-container -->
              			</div> <!-- .col-lg-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-2.jpg" alt="profile image">
                        </figure>
                        <div class="profile">
                          <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>
                              Rosan Martin<span class="pull-right text-booked">Booked</span></h3>
              						<p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="cal-box">
                            <figure><img src="images/calendar.jpg" alt="calendar"></figure>
                            <p>9 Mar 2016, Thursday</p>
                            <figure class="time"><img src="images/time.jpg" alt="time"></figure>
                            <p>09:30 AM</p>
                          </div>
              						<div class="booking">
              							<a href="" class="btn btn-third">Cancel Booking</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="camera"><a href=""></a></li>
                              </ul>
                            </div>
              						</div>
              					</div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-lg-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-3.jpg" alt="profile image">
                        </figure>
                        <div class="profile">
                          <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>
                              Rosan Martin<span class="pull-right text-profile text-cancelled">Cancelled</span></h3>
              						<p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="cal-box">
                            <figure><img src="images/calendar.jpg" alt="calendar"></figure>
                            <p>9 Mar 2016, Thursday</p>
                            <figure class="time"><img src="images/time.jpg" alt="time"></figure>
                            <p>09:30 AM</p>
                          </div>
              						<div class="booking">
              							<a href="" class="btn btn-third">Book Again</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="messages"><a href=""></a></li>
                              </ul>
                            </div>
              						</div>
              					</div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-lg-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-5.jpg" alt="profile image">
                        </figure>
                        <div class="profile">
                          <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>
                              Rosan Martin<span class="pull-right text-profile text-booked">Booked</span></h3>
              						<p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="cal-box">
                            <figure><img src="images/calendar.jpg" alt="calendar"></figure>
                            <p>9 Mar 2016, Thursday</p>
                            <figure class="time"><img src="images/time.jpg" alt="time"></figure>
                            <p>09:30 AM</p>
                          </div>
              						<div class="booking">
              							<a href="" class="btn btn-third">Cancel Booking</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="camera"><a href=""></a></li>
                              </ul>
                            </div>
              						</div>
              					</div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-lg-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-4.jpg" alt="profile image">
                        </figure>
                        <div class="profile">
                          <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>
                              Rosan Martin<span class="pull-right text-profile text-completed">Completed</span></h3>
              						<p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="cal-box">
                            <figure><img src="images/calendar.jpg" alt="calendar"></figure>
                            <p>9 Mar 2016, Thursday</p>
                            <figure class="time"><img src="images/time.jpg" alt="timee"></figure>
                            <p>09:30 AM</p>
                          </div>
              						<div class="booking">
              							<a href="" class="btn btn-third">Rate Therapist</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="camera"><a href=""></a></li>
                              </ul>
                            </div>
              						</div>
              					</div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-lg-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-6.jpg" alt="profile image">
                        </figure>
                        <div class="profile">
                          <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>
                              Rosan Martin<span class="pull-right text-profile text-completed">Completed</span></h3>
              						<p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="cal-box">
                            <figure><img src="images/calendar.jpg" alt="calendar"></figure>
                            <p>9 Mar 2016, Thursday</p>
                            <figure class="time"><img src="images/time.jpg" alt="time"></figure>
                            <p>09:30 AM</p>
                          </div>
              						<div class="booking">
              							<a href="" class="btn btn-third">Rated</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="messages"><a href=""></a></li>
                              </ul>
                            </div>
              						</div>
              					</div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-lg-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-1.jpg" alt="profile image">
                        </figure>
                        <div class="profile">
                          <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>
                              Rosan Martin<span class="pull-right text-profile text-cancelled">Cancelled</span></h3>
              						<p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="cal-box">
                            <figure><img src="images/calendar.jpg" alt="calendar"></figure>
                            <p>9 Mar 2016, Thursday</p>
                            <figure class="time"><img src="images/time.jpg" alt="time"></figure>
                            <p>09:30 AM</p>
                          </div>
              						<div class="booking">
              							<a href="" class="btn btn-third">Book Again</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="messages"><a href=""></a></li>
                              </ul>
                            </div>
              						</div>
              					</div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-lg-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-2.jpg" alt="profile image">
                        </figure>
                        <div class="profile">
                          <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>
                              Rosan Martin<span class="pull-right text-profile text-booked">Booked</span></h3>
              						<p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="cal-box">
                            <figure><img src="images/calendar.jpg" alt="calendar"></figure>
                            <p>9 Mar 2016, Thursday</p>
                            <figure class="time"><img src="images/time.jpg" alt="time"></figure>
                            <p>09:30 AM</p>
                          </div>
              						<div class="booking">
              							<a href="" class="btn btn-third">Cancel Booking</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="camera"><a href=""></a></li>
                              </ul>
                            </div>
              						</div>
              					</div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-lg-3 -->
                  </div><!-- .row -->
                </div><!-- .container-fluid -->
              </div><!-- .rightbar -->
           </div><!-- .col-md-9 -->
         </div><!-- .row -->
       </div><!-- .container-fluid -->
     </section><!-- .sidebar-container -->

    @endsection
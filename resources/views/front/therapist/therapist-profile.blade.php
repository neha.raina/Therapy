<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="format-decetion" content="telephone=no" />
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="images/logo.png">
      <title>Text Therapy | Therapist Profile</title>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/fonts.css" rel="stylesheet">
      <link href="css/custom.css" rel="stylesheet">
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>

     <header>
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <i class="fa fa-bars"></i>
              </button>
              <a class="navbar-brand" href="index.html">
                <figure>
                    <img src="images/logo.png" alt="Logo">
                </figure>
              </a>
            </div> <!--.navbar-header-->
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav text-uppercase pull-right">
                <li class="active"><a href="">About Us</a></li>
                <li><a href="">My Therapist</a></li>
                <li><a href="">Services</a></li>
                <li><a href="">Blog</a></li>
                <li><a href="">FAQ's</a></li>
                <li><a href=""><figure><img src="images/login.png" alt="login"></figure>Login</a></li>
                <li><a href=""><figure><img src="images/signup.png" alt="signup"></figure>Signup</a></li>
              </ul>
            </div><!-- .nav-collapse -->
          </div><!-- .container-fluid -->
        </nav>
      </header>

  	  <section class="banner profile-banner">
    		<div class="container-fluid">
          <div class="row">
      			<div class="banner-container container">
      				<h1>Therapist Profile</h1>
      				<p>Rasum ut perspiciatis undem </p>
      			</div><!-- .banner-container -->
    		  </div><!-- .row -->
    		</div><!-- .container-fluid -->
  	  </section><!-- .banner -->

      <section class="therapist-profile">
        <div class="container">
          <div class="row">
            <div class="col-md-5 col-sm-5">
              <figure class="profile-main">
                <img src="images/profile-main.jpg" alt="profile">
              </figure>
            </div><!-- .col-md-5 -->
            <div class="col-md-7 col-sm-7">
              <div class=" profile main-profile">
                <h3>Rosan Martin
                  <span class="pull-right">
                    <i class="fa fa-circle" aria-hidden="true"></i>Online
                  </span>
                </h3>
                <p class="descp">Clinical Social Worker
                  <span class="pull-right">
                    <i class="fa fa-star selected" aria-hidden="true"></i>
                    <i class="fa fa-star selected" aria-hidden="true"></i>
                    <i class="fa fa-star selected" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                  </span>
                </p>
                <div class="booking">
                  <a href="" class="btn btn-second">Book Now</a>
                  <div class="therapist-buttons pull-right">
                    <ul>
                      <li class="heart"><a href=""></a></li>
                      <li class="camera"><a href=""></a></li>
                      <li class="messages"><a href=""></a></li>
                    </ul>
                  </div>
                </div><!-- .booking -->
                <div class="main-descp main-border">
                  <div class="row">
                    <div class="col-md-2">
                        <h4>Qualification</h4>
                    </div><!-- .col-md-2 -->
                    <div class="col-md-10">
                      <p>MBA</p>
                    </div><!-- .col-md-10 -->
                  </div><!-- .row -->
                </div><!-- .main-descp -->
                <div class="main-descp main-border">
                  <div class="row">
                    <div class="col-md-2">
                        <h4>Experience</h4>
                    </div><!-- .col-md-2 -->
                    <div class="col-md-10">
                      <p>2-3 Years</p>
                    </div><!-- .col-md-10 -->
                  </div><!-- .row -->
                </div><!-- .main-descp -->
                <div class="main-descp">
                  <div class="row">
                    <div class="col-md-2">
                      <h4>Description</h4>
                    </div><!-- .col-md-2 -->
                    <div class="col-md-10">
                      <p>consectetur adipiscing elit. Aenean euismod bibendum laoreet. sodales Cumsm
                      Proin gravida dolor sit amet lacus accumsan et viverra justo comm pulvinaemp
                      odo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus penasnm
                      et magnis dis parturient montes, nascetur ridiculus mus. Nam parturient monte
                      fermentum, nulla luctus pharetra vulputate, </p>
                    </div><!-- .col-md-10 -->
                  </div><!-- .row -->
                </div><!-- .main-descp -->
              </div><!-- .main-profile -->
            </div><!-- .col-md-7 -->
          </div><!-- .row -->
        </div><!-- .container-->
      </section><!-- .therapist-profile -->

      <section class="review">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h4 class="review-heading">Review</h4>
            </div><!-- .col-md-12 -->
          </div><!-- .row -->
          <div class="row review-bottom">
            <div class="col-md-1">
              <figure class="review-img">
                <img src="images/profile-1-circle.png" alt="review image">
              </figure>
            </div><!-- .col-md-1 -->
            <div class="col-md-11">
              <div class="review-box">
                <h4>John Smith
                  <span>
                    <i class="fa fa-star selected" aria-hidden="true"></i>
                    <i class="fa fa-star selected" aria-hidden="true"></i>
                    <i class="fa fa-star selected" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                  </span>
                </h4>
                <div class="calendar-box">
                  <figure class="pull-left">
                    <img src="images/calendar.jpg" alt="calendar">
                  </figure>
                  <p>9 Mar 2016</p>
                </div>
                <p>consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus
                mollis orci, sed rhoncus sapien nunc eget odio</p>
              </div><!-- .review-box -->
            </div><!-- .col-md-1 -->
          </div><!-- .row -->
          <div class="row review-bottom">
            <div class="col-md-1">
              <figure class="review-img">
                <img src="images/profile-2-circle.png" alt="profile">
              </figure>
            </div><!-- .col-md-1 -->
            <div class="col-md-11">
              <div class="review-box">
                <h4>John Smith
                  <span>
                    <i class="fa fa-star selected" aria-hidden="true"></i>
                    <i class="fa fa-star selected" aria-hidden="true"></i>
                    <i class="fa fa-star selected" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                  </span>
                </h4>
                <div class="calendar-box">
                  <figure class="pull-left">
                    <img src="images/calendar.jpg" alt="calendar">
                  </figure>
                  <p>9 Mar 2016</p>
                </div>
                <p>consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus
                mollis orci, sed rhoncus sapien nunc eget odio</p>
              </div><!-- .review-box -->
            </div><!-- .col-md-1 -->
          </div><!-- .row -->
          <div class="row review-bottom nb">
            <div class="col-md-1">
              <figure class="review-img">
                <img src="images/profile-1-circle.png" alt="review image">
              </figure>
            </div><!-- .col-md-1 -->
            <div class="col-md-11">
              <div class="review-box">
                <h4>John Smith
                  <span>
                    <i class="fa fa-star selected" aria-hidden="true"></i>
                    <i class="fa fa-star selected" aria-hidden="true"></i>
                    <i class="fa fa-star selected" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                  </span>
                </h4>
                <div class="calendar-box">
                  <figure class="pull-left">
                    <img src="images/calendar.jpg" alt="calendar">
                  </figure>
                  <p>9 Mar 2016</p>
                </div>
                <p>consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar
                tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus
                mollis orci, sed rhoncus sapien nunc eget odio</p>
              </div><!-- .review-box -->
            </div><!-- .col-md-1 -->
          </div><!-- .row -->
          <div class="row">
            <div class="col-md-12">
              <h4 class="review-heading write-heading">Write a Review</h4>
            </div><!-- .col-md-12 -->
          </div><!-- .row -->
          <div class="row">
            <div class="col-md-12">
              <div class="rate">
                <h4>Rate</h4>
                <span class="pull-left">
                  <i class="fa fa-star selected" aria-hidden="true"></i>
                  <i class="fa fa-star selected" aria-hidden="true"></i>
                  <i class="fa fa-star selected" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i>
                  <i class="fa fa-star" aria-hidden="true"></i>
                </span>
              </div><!-- .rate -->
            </div><!-- .col-md-12 -->
          </div><!-- .row -->
          <div class="row">
            <div class="col-md-12">
              <form class="form-horizontal form-review">
                <h4>Write Review</h4>
                <div class="form-group">
                  <textarea class="form-control" placeholder="Write"></textarea>
                  <button type="submit" class="btn btn-second">Submit</button>
                </div>
              </form>
            </div><!-- .col-md-12 -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .review -->

      <section class="newsletter">
        <div class="container">
          <div class="row">
              <div class="newsletter-container">
                <div class="col-md-4">
                  <div class="newsletter-box">
                    <h3>Stay in Touch</h3>
                    <p>Subscribe to our Newsletter</p>
                  </div>
                </div><!-- .col-md-4 -->
                <div class="col-md-8">
                  <form class="form-horizontal">
                    <div class="form-group">
                      <input type="email" class="form-control" id="email" placeholder="Your Email">
                      <button type="submit" class="btn form-button">Subscribe</button>
                    </div>
                  </form>
                </div><!-- .col-md-8 -->
              </div><!-- .newsletter-container -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .newsletter -->

      <footer>
        <div class="container">
          <div class="row">
              <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="index.html">
                  <figure>
                    <img src="images/dark-logo.jpg" alt="Logo">
                  </figure>
                </a>
                <ul class="social-buttons">
                  <li><a href=""><img src="images/facebook.png" alt="Facebook"></a></li>
                  <li><a href=""><img src="images/twitter.png" alt="Twitter"></a></li>
                  <li><a href=""><img src="images/linkedin.png" alt="Linkedin"></a></li>
                  <li><a href=""><img src="images/google_plus.png" alt="Google Plus"></a></li>
                </ul>
                <ul class="contact-buttons">
                  <li><a href=""><figure><img src="images/phone.jpg" alt="Phone"></figure>888-972-6211</a></li>
                  <li><a href=""><figure><img src="images/mail.jpg" alt="Mail"></figure>info@dummytext.com</a></li>
                </ul>
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="ul-container first-ul">
                  <h4>Conditions</h4>
                  <ul>
                    <li><a href="">Cognitive Behaviour</a></li>
                    <li><a href="">Depression Help</a></li>
                    <li><a href="">Anxiety Help</a></li>
                    <li><a href="">Panic Attacks</a></li>
                    <li><a href="">Stres Relief</a></li>
                    <li><a href="">Eating Disorders</a></li>
                    <li><a href="">Grief Support</a></li>
                  </ul>
                </div><!-- .ul-container -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="ul-container second-ul">
                  <h4 class="less-margin">Useful Links</h4>
                  <ul>
                    <li><a href="">Experts</a></li>
                    <li><a href="">Discussions</a></li>
                    <li><a href="">Most asked questions</a></li>
                    <li><a href="">Motivational Quotes</a></li>
                    <li><a href="">Articles</a></li>
                    <li><a href="">De-stressing techniques</a></li>
                    <li><a href="">Press</a></li>
                  </ul>
                </div><!-- .ul-container -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="ul-container third-ul">
                  <h4>About Us</h4>
                  <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="">Blog</a></li>
                    <li><a href="">News</a></li>
                    <li><a href="">Our Therapist</a></li>
                  </ul>
                </div><!-- .ul-container -->
              </div><!-- .col-md-3 -->
          </div><!-- .row -->
        </div><!-- .container -->
        <div class="bottom-footer">
          <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                  <p><i class="fa fa-copyright" aria-hidden="true"></i> All Copyright | reserved to Text Therapy</p>
                </div><!-- .col-md-6 -->
                <div class="col-md-6 col-sm-6 text-right">
                  <ul>
                    <li><a href="index.html">Terms of Service</a></li>
                    <li><a href="">Privacy Policy</a></li>
                    <li><a href="">FAQ's</a></li>
                  </ul>
                </div><!-- .col-md-6 -->
            </div><!-- .row -->
          </div><!-- .container -->
        </div><!-- .bottom-footer -->
      </footer>

      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
      <script src="js/custom.js"></script>
      <script src="js/placeholders.min.js"></script>
   </body>
</html>

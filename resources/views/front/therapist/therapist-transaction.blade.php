@extends('front.layouts.app')

@section('content') 


     
           
              <div class="rightbar">
                <ul class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li class="active"><a href="#">My Schedules</a></li>
                </ul>
                <div class="container-fluid therapistContainer transactionContainer">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="searchContainer">
                        <form class="form-inline">
                          <div class="container-fluid">
                            <div class="row">
                              <div class="col-md-3 col-sm-6 pd0">
                                <div class="form-group">
                                  <label for="name">Therapist Name</label><br>
                                  <input type="text" class="form-control" id="name" placeholder="">
                                </div><!-- .form-group -->
                              </div><!-- .col-md-3 -->
                              <div class="col-md-3 col-sm-6 pd0">
                                <div class="form-group">
                                  <label for="transaction">Transaction ID</label><br>
                                  <input type="text" class="form-control" id="transaction" placeholder="">
                                </div><!-- .form-group -->
                              </div><!-- .col-md-3 -->
                              <div class="col-md-3 col-sm-6 pd0">
                                <div class="form-group">
                                  <label for="booking">Booking Date</label><br>
                                  <input type="text" class="form-control" id="booking" placeholder="">
                                </div><!-- .form-group -->
                              </div><!-- .col-md-3 -->
                              <div class="col-md-3 col-sm-6 pd0">
                                <div class="form-group">
                                  <button type="button" class="btn btn-first">Search</button>
                                </div><!-- .form-group -->
                              </div><!-- .col-md-3 -->
                            </div><!-- .row -->
                          </div><!-- .container-fluid -->
                        </form><!-- .form-inline -->
                      </div><!-- .searchContainer -->
                    </div><!-- .col-md-12 -->
                  </div><!-- .row -->
                  <div class="row">
                    <div class="col-md-12">
                      <div class="table-responsive tableContainer">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>#</th>
                              <th>S. NO</th>
                              <th>Booking Date / Time</th>
                              <th>Booking Mode</th>
                              <th>Client  Name</th>
                              <th>Amount</th>
                              <th>Transaction ID</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><input id="checkbox1" type="checkbox"><label for="checkbox1"></label></td>
                              <td>1</td>
                              <td>09-Aug-2017 / 09:30 AM</td>
                              <td>Video Call</td>
                              <td>John Cena</td>
                              <td>$19</td>
                              <td>982972892</td>
                              <td><a href="">Delete</a></td>
                            </tr>
                            <tr>
                              <td><input id="checkbox2" type="checkbox"><label for="checkbox2"></label></td>
                              <td>1</td>
                              <td>09-Aug-2017 / 09:30 AM</td>
                              <td>Video Call</td>
                              <td>John Cena</td>
                              <td>$19</td>
                              <td>982972892</td>
                              <td><a href="">Delete</a></td>
                            </tr>
                            <tr>
                              <td><input id="checkbox3" type="checkbox"><label for="checkbox3"></label></td>
                              <td>1</td>
                              <td>09-Aug-2017 / 09:30 AM</td>
                              <td>Video Call</td>
                              <td>John Cena</td>
                              <td>$19</td>
                              <td>982972892</td>
                              <td><a href="">Delete</a></td>
                            </tr>
                            <tr>
                              <td><input id="checkbox4" type="checkbox"><label for="checkbox4"></label></td>
                              <td>1</td>
                              <td>09-Aug-2017 / 09:30 AM</td>
                              <td>Video Call</td>
                              <td>John Cena</td>
                              <td>$19</td>
                              <td>982972892</td>
                              <td><a href="">Delete</a></td>
                            </tr>
                            <tr>
                              <td><input id="checkbox5" type="checkbox"><label for="checkbox5"></label></td>
                              <td>1</td>
                              <td>09-Aug-2017 / 09:30 AM</td>
                              <td>Video Call</td>
                              <td>John Cena</td>
                              <td>$19</td>
                              <td>982972892</td>
                              <td><a href="">Delete</a></td>
                            </tr>
                            <tr>
                              <td><input id="checkbox6" type="checkbox"><label for="checkbox6"></label></td>
                              <td>1</td>
                              <td>09-Aug-2017 / 09:30 AM</td>
                              <td>Video Call</td>
                              <td>John Cena</td>
                              <td>$19</td>
                              <td>982972892</td>
                              <td><a href="">Delete</a></td>
                            </tr>
                            <tr>
                              <td><input id="checkbox7" type="checkbox"><label for="checkbox7"></label></td>
                              <td>1</td>
                              <td>09-Aug-2017 / 09:30 AM</td>
                              <td>Video Call</td>
                              <td>John Cena</td>
                              <td>$19</td>
                              <td>982972892</td>
                              <td><a href="">Delete</a></td>
                            </tr>
                          </tbody>
                        </table>
                      </div><!-- .searchContainer -->
                    </div><!-- .col-md-12 -->
                  </div><!-- .row -->
                </div><!-- .container-fluid -->
              </div><!-- .rightbar -->
           </div><!-- .col-md-9 -->
         </div><!-- .row -->
       </div><!-- .container-fluid -->
     </section><!-- .sidebar-container -->

     @endsection
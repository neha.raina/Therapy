@extends('front.layouts.app')

@section('title', 'Forget Password')

@section('content')



</section>
<section class="createAccount">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="createAccountForm">
                        <div class="formContainer">
                            <div class="crateformInner signInF">
                                <div class="formTop">
                                    <figure>
                                        <img src="{{url('images/keyicon.png ')}}" alt="">
                                    </figure>
                                    <p> Forgot your password?</p>
                                    <p class="description">Enter your email address below and we’ll send you reset instructions</p>
                                    @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                    @endif
                                </div>

                                <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                                    {{ csrf_field() }}

                                    <div class="form">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                    
                                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required  placeholder="Email">

                                                    @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btnSubmit">Submit</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </form>  
                            </div>
                            <div class="crateformBottom text-center">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <p>Return to<a href="{{ route('login') }}">Login </a>Page</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



@endsection
@extends('front.layouts.app')

@section('title', 'Sign In')

@section('content')  


  
</section>
      <section class="createAccount">
          <div class="container-fluid">
              <div class="container">
                  <div class="row">
                    <div class="col-sm-12">
                        <div class="createAccountForm">
                          <div class="formContainer">
                            <div class="crateformInner signInF">  
                              <div class="formTop">
                                    <figure>
                                        <img src="images/userIcon.png" alt="">
                                    </figure>
                                    <p>Sign In</p>
                              </div>
                              <div class="form">
                                  <form method="POST" action="{{ route('login') }}">
                                        {{ csrf_field() }}
                                 <div class="row">
                                     
                                      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <div class="col-sm-12">
                                      <div class="form-group">
                                         
                                           <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="email">
                                           
                                    </div>
                                    </div>
                                      </div>
                                     
                                     
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                             <div class="col-sm-12">
                                      <d    iv class="form-group">
                                          
                                           <input id="password" type="password" class="form-control" name="password" required placeholder="Password">
                                           
                                    </div>
                                 @if ($errors->has('email'))
                                        <span class="help-block" >
                                        <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                         @endif
                                   </div>
                                 </div>      
                                   
                         
                              
                                    <div class="col-sm-12 subscribe">
                                      <div class="row">
                                          <div class="col-sm-4 col-xs-6 text-left">
                                               
                                               <input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} > 
                                                <label for="remember">Remember Me</label>
                                          </div>
                                          
                                          
                                          
                                          
                                          
                                          <div class="col-sm-8 col-xs-6 text-right"><a href="{{ route('password.request') }}">Forgot password?</a></div>
                                      </div>
                                   </div>
                                   <div class="col-sm-12">
                                      <div class="form-group">
                                          <button type="submit" class="btn btnSubmit">Sign In</button> 
                                      </div>
                                   </div>
                                  
                                </div>
                                  </form>
                              </div>
                            </div>  
                            <div class="crateformBottom text-center">
                                  <div class="row">
                                        <div class="col-sm-12">
                                          <p>Not a member yet?<a href="{{ route('register') }}">Create Account  </a></p>
                                        </div>
                                  </div>
                            </div>
                          </div>  
                        </div>
                    </div>
                  </div>
              </div>
          </div>
      </section>

      
     

      
@endsection
@extends('front.layouts.app')

@section('title', 'Sign Up')

@section('content')

  	

</section>
      <section class="createAccount">
          <div class="container-fluid">
              <div class="container">
                  <div class="row">
                    <div class="col-sm-12">
                        <div class="createAccountForm">
                            <div class="formContainer">
                                <div class="crateformInner">
                            <div class="formTop">
                                  <figure>
                                      <img src="images/pencil.png" alt="">
                                  </figure>
                                  <p>Create Account</p>
                            </div>
                            <div class="form">
                               <div class="row">
                            <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                                  {{ csrf_field() }}
                                  <input type="hidden" name="roleType" value="customer"> 
                                  
                                  <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}" >
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="First Name">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                                  </div>
                                  </div>
                                  
                                  <div class="col-sm-6">
                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}" >
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Last Name">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                                  </div>
                                  </div>
                                  
                                 <div class="col-sm-12">
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}" >
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                 </div>
                                 <div class="col-sm-12">
                                   <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                       <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                 </div>
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                                    </div>
                                 </div>
                                 <div class="col-sm-12">

                                        <button type="submit" class="btn btnSubmit">Create Account</button>

                                 </div>
                               </form>
                                 <div class="col-sm-12 subscribe">
                                    <div class="row">
                                        <div class="col-sm-4 text-left">
                                             <input id="box1" type="checkbox" />
                                              <label for="box1">Subscribe Newsletter</label>
                                        </div>
                                        <div class="col-sm-8"><p>By signing up, you agree to our <a href="#">T&amp;C</a> and <a href="#">Privacy Policy</a></p></div>
                                    </div>
                                 </div>
                              </div>
                            </div>
                          </div>
                          <div class="crateformBottom text-center">
                                <div class="row">
                                      <div class="col-sm-12">
                                        <p>Already a member? <a href="{{ url('/login') }}">Login Here</a></p>
                                      </div>
                                </div>
                          </div>
                            </div>
                        </div>
                    </div>
                  </div>
              </div>
          </div>
      </section>



@endsection
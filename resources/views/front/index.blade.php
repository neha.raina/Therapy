@extends ('front.layouts.app')
@section ('content')

      <section class="home-banner">
        <div class="container-fluid">
          <div class="row">
              <div class="owl-carousel owl-theme home-carousel">
                  <div class="item">
                    <figure>
                      <img class="img-responsive" src="images/banner.jpg" alt="Banner Image">
                    </figure>
                    <div class="home-caption">
                      <div class="vertical-middle">
                        <h1>Want Someone<br><span>Helpful to talk to you?</span></h1>
                        <p>Chat now with your own kind peer counselor. Breakthrough,<br> see hope, join the tens of thousands helped just like you.</p>
                        <a class="btn btn-second" href="">Get Started</a>
                      </div> <!--.vertical-middle-->
                    </div><!-- .home-caption -->
                  </div><!-- .item -->
                  <div class="item">
                    <figure>
                      <img class="img-responsive" src="images/banner-2.jpg" alt="Banner Image">
                    </figure>
                     <div class="home-caption">
                      <div class="vertical-middle">
                        <h1>Want Someone<br><span>Helpful to talk to you?</span></h1>
                        <p>Chat now with your own kind peer counselor. Breakthrough,<br> see hope, join the tens of thousands helped just like you.</p>
                        <a class="btn btn-second" href="">Get Started</a>
                      </div> <!--.vertical-middle-->
                    </div><!-- .home-caption -->
                  </div><!-- .item -->
                  <div class="item">
                    <figure>
                      <img class="img-responsive" src="images/banner-3.jpg" alt="Banner Image">
                    </figure>
                     <div class="home-caption">
                      <div class="vertical-middle">
                        <h1>Want Someone<br><span>Helpful to talk to you?</span></h1>
                        <p>Chat now with your own kind peer counselor. Breakthrough,<br> see hope, join the tens of thousands helped just like you.</p>
                        <a class="btn btn-second" href="">Get Started</a>
                      </div> <!--.vertical-middle-->
                    </div><!-- .home-caption -->
                  </div><!-- .item -->
                  <!-- <div class="item">
                    <figure>
                      <img class="img-responsive" src="images/banner.jpg" alt="Banner Image">
                    </figure>
                  </div> --><!-- .item -->
              </div><!-- .home-carousel -->

          </div><!-- .row -->
        </div><!-- .container-fluid -->
      </section><!-- .home-banner -->

      <section class="why-therapy">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="heading-box text-center">
                <h2>Why Choose Text Therapy?</h2>
                <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators.</p>
              </div><!-- .why-box -->
            </div><!-- .col-md-12 -->
          </div><!-- .row -->
          <div class="row text-center">
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="box">
                    <figure>
                      <img src="images/available.png" alt="Available">
                    </figure>
                    <h3>24x7 Available</h3>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac</p>
                </div><!-- .box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="box">
                    <figure>
                      <img src="images/affordable.png" alt="Affordable">
                    </figure>
                    <h3>It's Affordable</h3>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac</p>
                </div><!-- .box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="box">
                    <figure>
                      <img src="images/confidential.png" alt="Confidential">
                    </figure>
                    <h3>Confidential</h3>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac</p>
                </div><!-- .box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="box">
                    <figure>
                      <img src="images/experts.png" alt="Experts">
                    </figure>
                    <h3>450+ Experts</h3>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac</p>
                </div><!-- .box -->
              </div><!-- .col-md-3 -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .why-therapy -->

      <section class="professional">
        <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <div class="heading-box text-center">
                    <h2>Our Professional Therapist</h2>
                    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators.</p>
                  </div> <!-- .heading-box -->
              </div><!-- .col-md-12 -->
          </div><!-- .row -->
          <div class="row">
            <div class="col-md-12">
              <div class="owl-carousel owl-theme professional-carousel">
                  <div class="item">
                    <div class="professionalInner">
                      <figure>
                      <img src="images/therapist-1.jpg" alt="Therapist Image">
                      </figure>
                      <div class="member-box">
                          <h4>Puma Benz</h4>
                          <p class="member-para">Set ammet dummy text</p>
                          <p class="read-more">If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined.Sed porta interdum mollis. Nam magna quam.</p>
                          <a href="">Read More</a>
                      </div><!-- .member-box -->
                    </div>
                  </div><!-- .item -->
                  <div class="item">
                    <div class="professionalInner">
                        <figure>
                      <img src="images/therapist-2.jpg" alt="Therapist Image">
                      </figure>
                      <div class="member-box">
                          <h4>Joshlyn Anderson</h4>
                          <p class="member-para">Set ammet dummy text</p>
                          <p class="read-more">If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined.Sed porta interdum mollis. Nam magna quam.</p>
                          <a href="">Read More</a>
                      </div><!-- .member-box -->
                    </div>
                  </div><!-- .item -->
                  <div class="item">
                    <div class="professionalInner">
                      <figure>
                      <img src="images/therapist-3.jpg" alt="Therapist Image">
                      </figure>
                      <div class="member-box">
                          <h4>Tiffany Alex</h4>
                          <p class="member-para">Set ammet dummy text</p>
                          <p class="read-more">If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined.Sed porta interdum mollis. Nam magna quam.</p>
                          <a href="">Read More</a>
                      </div><!-- .member-box -->
                    </div>
                  </div><!-- .item -->
                  <div class="item">
                    <div class="professionalInner">
                      <figure>
                      <img src="images/therapist-4.jpg" alt="Therapist Image">
                      </figure>
                      <div class="member-box">
                          <h4>Shriya Saran</h4>
                          <p class="member-para">Set ammet dummy text</p>
                          <p class="read-more">If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined.Sed porta interdum mollis. Nam magna quam.</p>
                          <a href="">Read More</a>
                      </div><!-- .member-box -->
                    </div>
                  </div><!-- .item -->
                  <div class="item">
                    <div class="professionalInner">
                      <figure>
                      <img src="images/therapist-1.jpg" alt="Therapist Image">
                      </figure>
                      <div class="member-box">
                          <h4>Puma Benz</h4>
                          <p class="member-para">Set ammet dummy text</p>
                          <p class="read-more">If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined.Sed porta interdum mollis. Nam magna quam.</p>
                          <a href="">Read More</a>
                      </div><!-- .member-box -->
                    </div>
                  </div><!-- .item -->
              </div><!-- .professional-carousel -->
            </div><!-- .col-md-12 -->
          </div><!-- .row -->
          <div class="row">
            <div class="col-md-12 text-center">
              <a href="" class="btn btn-first">See all therapist</a>
            </div><!-- .col-md-12 -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .professional -->

      <section class="how-it-works">
        <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <div class="heading-box text-center">
                    <h2>How Text Therapy Works</h2>
                    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators.</p>
                  </div> <!-- .heading-box -->
              </div><!-- .col-md-12 -->
          </div><!-- .row -->
          <div class="row text-center">
              <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="work-box work-assesment">
                    <figure>
                      <img class="img-responsive" src="images/assesment.png" alt="Therapist Image">
                    </figure>
                    <h4>Get a Assesment</h4>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac, eleifend tristique est.</p>
                  </div><!-- .work-box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="work-box work-match">
                    <figure>
                      <img class="img-responsive" src="images/matched.png" alt="Therapist Image">
                    </figure>
                    <h4>Matched Therapist</h4>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac, eleifend tristique est.</p>
                  </div><!-- .work-box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="work-box work-plan">
                    <figure>
                      <img class="img-responsive" src="images/plan.png" alt="Therapist Image">
                    </figure>
                    <h4>Select a plan</h4>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac, eleifend tristique est.</p>
                  </div><!-- .work-box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="work-box work-chat">
                    <figure>
                      <img class="img-responsive" src="images/chat.png" alt="Therapist Image">
                    </figure>
                    <h4>Start Chat</h4>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac, eleifend tristique est.</p>
                  </div><!-- .work-box -->
              </div><!-- .col-md-3 -->
          </div><!-- .row -->
          <div class="row">
              <div class="col-md-12 text-center">
                  <a href="" class="btn btn-second">Find Therapist Now!</a>
              </div><!-- .col-md-12 -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .how-it-works -->

      <section class="testimonials">
        <div class="container">
          <div class="row">
              <div class="col-md-12 text-center">
                <div class="heading-box">
                  <h2>What Happy People Are Saying?</h2>
                </div><!-- .heading-box -->
                <div class="owl-carousel owl-theme testimonial-carousel">
                    <div class="item">
                      <p><i>Nunc tincidunt porta diam, ut molestie eros molestie non. Nullam egestas vehicula urna nec condimentum. Aenean tincidunt, diam eget feugiat congue, mi nisl scelerisque libero, et blandit magna nisl et neque. Donec et porttitor elit. Suspendisse quis pharetra tortor. </i></p>
                      <figure>
                        <img src="images/testimonial.png" alt="Banner Image">
                      </figure>
                    </div><!-- .item -->
                    <div class="item">
                      <p><i>Nunc tincidunt porta diam, ut molestie eros molestie non. Nullam egestas vehicula urna nec condimentum. Aenean tincidunt, diam eget feugiat congue, mi nisl scelerisque libero, et blandit magna nisl et neque. Donec et porttitor elit. Suspendisse quis pharetra tortor. </i></p>
                      <figure>
                        <img src="images/testimonial.png" alt="Banner Image">
                      </figure>
                    </div><!-- .item -->
                    <div class="item">
                      <p><i>Nunc tincidunt porta diam, ut molestie eros molestie non. Nullam egestas vehicula urna nec condimentum. Aenean tincidunt, diam eget feugiat congue, mi nisl scelerisque libero, et blandit magna nisl et neque. Donec et porttitor elit. Suspendisse quis pharetra tortor. </i></p>
                      <figure>
                        <img src="images/testimonial.png" alt="Banner Image">
                      </figure>
                    </div><!-- .item -->
                    <div class="item">
                      <p><i>Nunc tincidunt porta diam, ut molestie eros molestie non. Nullam egestas vehicula urna nec condimentum. Aenean tincidunt, diam eget feugiat congue, mi nisl scelerisque libero, et blandit magna nisl et neque. Donec et porttitor elit. Suspendisse quis pharetra tortor. </i></p>
                      <figure>
                        <img src="images/testimonial.png" alt="Banner Image">
                      </figure>
                    </div><!-- .item -->
                </div> <!-- testimonial-carousel -->
              </div><!-- .col-md-12 -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .testimonials -->

   @endsection

     
 
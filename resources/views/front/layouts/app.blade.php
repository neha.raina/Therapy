<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="{{ asset('images/logo.png') }}">
        <title>@yield('title', app_name())</title>

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', 'text therapy')">
        <meta name="author" content="@yield('meta_author', 'role')">
        @yield('meta')
 
        <!-- Styles -->
        @yield('before-styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}"/> 
        <link rel="stylesheet" type="text/css" href="{{ asset('css/fonts.css') }}"/> 
        <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}"/> 
        <link rel="stylesheet" type="text/css" href="{{ asset('css/ion.rangeSlider.css') }}"/> 
        <link rel="stylesheet" type="text/css" href="{{ asset('css/ion.rangeSlider.skinFlat.css') }}"/> 
        <link rel="stylesheet" type="text/css" href="{{ asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css') }}"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css') }}"/>
          <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}"/> 
            <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}"/> 
        
        
         @yield('after-styles')

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body>
        <div id="app">
           
            @if (Auth::check())
                 @if (Auth::user()->roleType ==='customer')
                      @include('front.includes-logged.user-nav')
                      @include('front.includes-logged.user-header')
                 @endif
                 
                 @if (Auth::user()->roleType ==='therapist')
                      @include('front.includes-logged.therapist-nav')
                      @include('front.includes-logged.therapist-header')
                 @endif
            @else
                @include('front.includes.nav')
                @include('front.includes.banner')
            @endif
            
            @yield('content')
            
            @if (!Auth::check())
            @include('front.includes.subscribe')
            @include('front.includes.footer')
            @endif
            
        </div><!--#app-->

        <!-- Scripts -->
        @yield('before-scripts')
        
        <script src="{{ asset('js/jquery.js') }}" ></script>
        <script src="{{ asset('js/bootstrap.js') }}" ></script>
        <script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js') }}" ></script>
        <script src="{{ asset('js/placeholders.min.js') }}" ></script>
        <script src="{{ asset('js/custom.js') }}" ></script>
       
        
       @yield('after-scripts')

          
    </body>
</html>
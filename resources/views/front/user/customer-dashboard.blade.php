@extends('front.layouts.app')

@section ('content')


              <div class="rightbar dashboardRightbar">
                <ul class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li class="active"><a href="#">Dashboard</a></li>
                </ul>
                <div class="rightbar-content text-center">
                  <h2>Welcome</h2>
                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, </p>
                  <a href="{{ route('find-therapist') }}" class="btn btn-second">Find Therapist</a>
                  <a href="{{ route('therapist') }}" class="btn btn-third">Browse All</a>
                </div><!-- .rightbar-content -->
              </div><!-- .rightbar -->
           </div><!-- .col-md-9 -->
         </div><!-- .row -->
       </div><!-- .container-fluid -->
     </section><!-- .sidebar-container -->

   @endsection
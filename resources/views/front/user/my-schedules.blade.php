@extends('front.layouts.app')

@section ('content')


    <div class="rightbar mySchedule">
        <div class="myScheduleInner">
            <ul class="breadcrumb pull-left">
                <li><a href="#">Home</a></li>
                <li class="active"><a href="#">My Schedules </a></li>
            </ul>
            <ul class="date pull-right">
                <li><span class="today">Today</span></li>
                <li class="yearScroll">
                    <a href="javscript:void(0);"><i class="fa fa-angle-left" aria-hidden="true"></i>
                    </a><span class="month">Jan</span> <span class="year">2017</span><a href="javscript:void(0);"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </li> <li> <a href="javscript:void(0)" class="edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    </a></li>
            </ul>
        </div>
        <div class="sidebarRight">
            <div class="calender">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Monday</th>
                                <th>Tuesday</th>
                                <th>Wednesday</th>
                                <th>Thursday</th>
                                <th>Friday</th>
                                <th>Saturday</th>
                                <th>Sunday</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="inactive">26</td>
                                <td class="inactive">27</td>
                                <td class="inactive">28</td>
                                <td class="inactive">29</td>
                                <td class="inactive">30</td>
                                <td class="inactive">31</td>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>3</td>
                                <td>4</td>
                                <td class="lightRed" data-toggle="popover" title="Video Call Scheduled" data-content=" 09:30 -10:00 AM">5</td>
                                <td>6</td>
                                <td class="green" data-toggle="popover" title="Video Call Scheduled" data-content=" 09:30 -10:00 AM">7</td>
                                <td>8</td>
                            </tr>
                            <tr>
                                <td class="yellow" data-toggle="popover" title="Video Call Scheduled" data-content=" 09:30 -10:00 AM">9</td>
                                <td>10</td>
                                <td>11</td>
                                <td>12</td>
                                <td>13</td>
                                <td>14</td>
                                <td>15</td>
                            </tr>
                            <tr>
                                <td>16</td>
                                <td>17</td>
                                <td>18</td>
                                <td>19</td>
                                <td>20</td>
                                <td>21</td>
                                <td>22</td>
                            </tr>
                            <tr>
                                <td>23</td>
                                <td>24</td>
                                <td>25</td>
                                <td>26</td>
                                <td class="skyblue" data-toggle="popover" title="Video Call Scheduled" data-content=" 09:30 -10:00 AM">27</td>
                                <td>28</td>
                                <td>29</td>
                            </tr>
                            <tr>
                                <td>30</td>
                                <td class="lightBlue" data-toggle="popover" title="Video Call Scheduled" data-content=" 09:30 -10:00 AM">31</td>
                                <td class="inactive">1</td>
                                <td class="inactive">2</td>
                                <td class="inactive">3</td>
                                <td class="inactive">4</td>
                                <td class="inactive">5</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div><!-- .rightbar-content -->
    </div><!-- .rightbar -->
</div><!-- .col-md-9 -->
</div><!-- .row -->
</div><!-- .container-fluid -->
</section><!-- .sidebar-container -->

@endsection

@section ('after-scripts')
<script type="text/javascript">
    var image = '<img src="https://developer.chrome.com/extensions/examples/api/idle/idle_simple/sample-128.png">';
    $('[data-toggle="popover"]').popover({
        placement: 'bottom',
        trigger: 'hover',
        container: 'body',
        content: image,
        html: true
    });
</script>

@endsection
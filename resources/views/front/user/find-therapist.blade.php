<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="format-decetion" content="telephone=no" />
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="images/logo.png">
      <title>Text Therapy | Find Therapist</title>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/fonts.css" rel="stylesheet">
      <link href="css/custom.css" rel="stylesheet">
      <link href="css/ion.rangeSlider.css" rel="stylesheet">
      <link rel="stylesheet" href="css/ion.rangeSlider.skinFlat.css" />
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>

     <header>
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <i class="fa fa-bars"></i>
              </button>
              <a class="navbar-brand" href="index.html">
                <figure>
                    <img src="images/logo.png" alt="Logo">
                </figure>
              </a>
            </div> <!--.navbar-header-->
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav text-uppercase pull-right">
                <li class="active"><a href="">About Us</a></li>
                <li><a href="">My Therapist</a></li>
                <li><a href="">Services</a></li>
                <li><a href="">Blog</a></li>
                <li><a href="">FAQ's</a></li>
                <li><a href=""><figure><img src="images/login.png" alt="login"></figure>Login</a></li>
                <li><a href=""><figure><img src="images/signup.png" alt="signup"></figure>Signup</a></li>
              </ul>
            </div><!-- .nav-collapse -->
          </div><!-- .container-fluid -->
        </nav>
      </header>

  	  <section class="banner find-therapist">
    		<div class="container-fluid">
          <div class="row">
      			<div class=" banner-container container">
      				<h1>Find Therapist</h1>
      				<p>Sed ut perspiciatis undem</p>
      			</div><!-- .banner-container -->
    		  </div><!-- .row -->
    		</div><!-- .container-fluid -->
  	  </section><!-- .banner -->

      <section class="better-therapist">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="heading-box text-center">
                <h2>We will help you to choose the better Therapist</h2>
                <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators.</p>
              </div><!-- .heading-box -->
            </div><!-- .col-md-12 -->
          </div><!-- .row -->
          <div class="row">
            <div class="col-md-12">
              <form class="form-inline">
                <div class="row">
                <div class="form-group col-md-3 col-sm-6">
                  <label for="city">Your city and state, or postal code</label><br>
                  <input type="text" class="form-control" id="city" placeholder="Your City">
                </div><!-- .form-group -->
                <div class="form-group col-md-3 col-sm-6">
                  <label for="types">Therapist types</label><br>
                  <select id="types" class="form-control">
                    <option value="select">Select Types</option>
                    <option value="select">Select Types</option>
                    <option value="select">Select Types</option>
                    <option value="select">Select Types</option>
                  </select>
                </div><!-- .form-group -->
                <div class="form-group col-md-3 col-sm-6">
                  <label for="specialties">Specialties</label><br>
                  <select id="specialties" class="form-control">
                    <option value="speciality">Select Specialties</option>
                    <option value="speciality">Select Specialties</option>
                    <option value="speciality">Select Specialties</option>
                    <option value="speciality">Select Specialties</option>
                  </select>
                </div><!-- .form-group -->
                <div class="form-group col-md-3 col-sm-6">
                  <label for="payment">Payment type</label><br>
                  <select id="payment" class="form-control">
                    <option value="pay">Select Types</option>
                    <option value="pay">Select Types</option>
                    <option value="pay">Select Types</option>
                    <option value="pay">Select Types</option>
                  </select>
                </div><!-- .form-group -->
                <div class="form-group col-md-3 col-sm-6">
                  <label for="language">Language</label><br>
                  <input type="text" class="form-control" id="language" placeholder="English">
                </div><!-- .form-group -->
                <div class="form-group col-md-3 col-sm-6">
                  <label for="gender">Gender</label><br>
                  <select id="gender" class="form-control">
                    <option value="Either">Either</option>
                    <option value="either">Either</option>
                    <option value="either">Either</option>
                    <option value="either">Either</option>
                  </select>
                </div><!-- .form-group -->
                <div class="form-group col-md-3 col-sm-6">
                  <label for="name">Find by name</label><br>
                  <select id="name" class="form-control">
                    <option value="your_name">Your therapist's name</option>
                    <option value="your_name">Your therapist's name</option>
                    <option value="your_name">Your therapist's name</option>
                    <option value="your_name">Your therapist's name</option>
                  </select>
                </div><!-- .form-group -->
                <div class="form-group col-md-3 col-sm-6">
                  <label for="range">Adjust price range (self-pay rate)</label><br>
                  <input type="text" id="range" name="range" value="" />
                </div><!-- .form-group -->
              </div><!-- .row -->
            </form><!-- .form-inline -->
            </div><!-- .col-md-12 -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .better-therapist -->

      <section class="therapist-results">
        <div class="container">
          <div class="row">
            <div class="resultboxContainer">
              <div class="col-md-3 pd0">
                  <div class="result-box">
                    <figure>
                      <img src="images/therapist-1-small.jpg" alt="theraist">
                    </figure>
                  </div><!-- .result-box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-9 pd0">
                  <div class="result-box profile">
                    <h3>Rossy Martin</h3>
                    <p class="descp">Clinical Social Worker</p>
                    <p class="exp">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperigeaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsaluptatem qvoluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sesciunt. Nequequisqua est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</p>
                    <div class="row">
                      <div class="col-md-2 pd0">
                        <div class="profile-bottom first-profile">
                          <h4>Payment type</h4>
                          <p>Insurance</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-2 -->
                      <div class="col-md-2 pd0">
                        <div class="profile-bottom second-profile">
                          <h4>Self-pay rate</h4>
                          <p>$100/hr</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-3 -->
                      <div class="col-md-8 pd0">
                        <div class="profile-bottom third-profile">
                          <h4>Specialities</h4>
                          <p>Depression, Domestic Violence, Grief/Loss, Maternal Health/Postpartum,Parenting, Relationships, Stress</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-6 -->
                    </div><!-- .row -->
                  </div><!-- .profile -->
              </div><!-- .col-md-9 -->
            </div><!-- .resultboxContainer -->
          </div><!-- .row -->
          <div class="row">
            <div class="resultboxContainer">
              <div class="col-md-3 pd0">
                  <div class="result-box">
                    <figure>
                      <img src="images/therapist-2-small.jpg" alt="theraist">
                    </figure>
                  </div><!-- .result-box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-9 pd0">
                  <div class="result-box profile">
                    <h3>Rossy Martin</h3>
                    <p class="descp">Clinical Social Worker</p>
                    <p class="exp">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperigeaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsaluptatem qvoluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sesciunt. Nequequisqua est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</p>
                    <div class="row">
                      <div class="col-md-2 pd0">
                        <div class="profile-bottom first-profile">
                          <h4>Payment type</h4>
                          <p>Insurance</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-2 -->
                      <div class="col-md-2 pd0">
                        <div class="profile-bottom second-profile">
                          <h4>Self-pay rate</h4>
                          <p>$100/hr</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-3 -->
                      <div class="col-md-8 pd0">
                        <div class="profile-bottom third-profile">
                          <h4>Specialities</h4>
                          <p>Depression, Domestic Violence, Grief/Loss, Maternal Health/Postpartum,Parenting, Relationships, Stress</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-6 -->
                    </div><!-- .row -->
                  </div><!-- .profile -->
              </div><!-- .col-md-9 -->
            </div><!-- .resultboxContainer -->
          </div><!-- .row -->
          <div class="row">
            <div class="resultboxContainer">
              <div class="col-md-3 pd0">
                  <div class="result-box">
                    <figure>
                      <img src="images/therapist-4-small.jpg" alt="theraist">
                    </figure>
                  </div><!-- .result-box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-9 pd0">
                  <div class="result-box profile">
                    <h3>Rossy Martin</h3>
                    <p class="descp">Clinical Social Worker</p>
                    <p class="exp">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperigeaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsaluptatem qvoluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sesciunt. Nequequisqua est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</p>
                    <div class="row">
                      <div class="col-md-2 pd0">
                        <div class="profile-bottom first-profile">
                          <h4>Payment type</h4>
                          <p>Insurance</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-2 -->
                      <div class="col-md-2 pd0">
                        <div class="profile-bottom second-profile">
                          <h4>Self-pay rate</h4>
                          <p>$100/hr</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-3 -->
                      <div class="col-md-8 pd0">
                        <div class="profile-bottom third-profile">
                          <h4>Specialities</h4>
                          <p>Depression, Domestic Violence, Grief/Loss, Maternal Health/Postpartum,Parenting, Relationships, Stress</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-6 -->
                    </div><!-- .row -->
                  </div><!-- .profile -->
              </div><!-- .col-md-9 -->
            </div><!-- .resultboxContainer -->
          </div><!-- .row -->
          <div class="row">
            <div class="resultboxContainer">
              <div class="col-md-3 pd0">
                  <div class="result-box">
                    <figure>
                      <img src="images/therapist-1-small.jpg" alt="theraist">
                    </figure>
                  </div><!-- .result-box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-9 pd0">
                  <div class="result-box profile">
                    <h3>Rossy Martin</h3>
                    <p class="descp">Clinical Social Worker</p>
                    <p class="exp">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperigeaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsaluptatem qvoluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sesciunt. Nequequisqua est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</p>
                    <div class="row">
                      <div class="col-md-2 pd0">
                        <div class="profile-bottom first-profile">
                          <h4>Payment type</h4>
                          <p>Insurance</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-2 -->
                      <div class="col-md-2 pd0">
                        <div class="profile-bottom second-profile">
                          <h4>Self-pay rate</h4>
                          <p>$100/hr</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-3 -->
                      <div class="col-md-8 pd0">
                        <div class="profile-bottom third-profile">
                          <h4>Specialities</h4>
                          <p>Depression, Domestic Violence, Grief/Loss, Maternal Health/Postpartum,Parenting, Relationships, Stress</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-6 -->
                    </div><!-- .row -->
                  </div><!-- .profile -->
              </div><!-- .col-md-9 -->
            </div><!-- .resultboxContainer -->
          </div><!-- .row -->
          <div class="row">
            <div class="resultboxContainer">
              <div class="col-md-3 pd0">
                  <div class="result-box">
                    <figure>
                      <img src="images/therapist-3-small.jpg" alt="theraist">
                    </figure>
                  </div><!-- .result-box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-9 pd0">
                  <div class="result-box profile">
                    <h3>Rossy Martin</h3>
                    <p class="descp">Clinical Social Worker</p>
                    <p class="exp">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperigeaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsaluptatem qvoluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sesciunt. Nequequisqua est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</p>
                    <div class="row">
                      <div class="col-md-2 pd0">
                        <div class="profile-bottom first-profile">
                          <h4>Payment type</h4>
                          <p>Insurance</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-2 -->
                      <div class="col-md-2 pd0">
                        <div class="profile-bottom second-profile">
                          <h4>Self-pay rate</h4>
                          <p>$100/hr</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-3 -->
                      <div class="col-md-8 pd0">
                        <div class="profile-bottom third-profile">
                          <h4>Specialities</h4>
                          <p>Depression, Domestic Violence, Grief/Loss, Maternal Health/Postpartum,Parenting, Relationships, Stress</p>
                        </div><!-- .profile-bottom -->
                      </div><!-- .col-md-6 -->
                    </div><!-- .row -->
                  </div><!-- .profile -->
              </div><!-- .col-md-9 -->
            </div><!-- .resultboxContainer -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .therapist-results -->

      <section class="newsletter">
        <div class="container">
          <div class="row">
              <div class="newsletter-container">
                <div class="col-md-4">
                  <div class="newsletter-box">
                    <h3>Stay in Touch</h3>
                    <p>Subscribe to our Newsletter</p>
                  </div>
                </div><!-- .col-md-4 -->
                <div class="col-md-8">
                  <form class="form-horizontal">
                    <div class="form-group">
                      <input type="email" class="form-control" id="email" placeholder="Your Email">
                      <button type="submit" class="btn form-button">Subscribe</button>
                    </div>
                  </form>
                </div><!-- .col-md-8 -->
              </div><!-- .newsletter-container -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .newsletter -->

      <footer>
        <div class="container">
          <div class="row">
              <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="index.html">
                  <figure>
                    <img src="images/dark-logo.jpg" alt="Logo">
                  </figure>
                </a>
                <ul class="social-buttons">
                  <li><a href=""><img src="images/facebook.png" alt="Facebook"></a></li>
                  <li><a href=""><img src="images/twitter.png" alt="Twitter"></a></li>
                  <li><a href=""><img src="images/linkedin.png" alt="Linkedin"></a></li>
                  <li><a href=""><img src="images/google_plus.png" alt="Google Plus"></a></li>
                </ul>
                <ul class="contact-buttons">
                  <li><a href=""><figure><img src="images/phone.jpg" alt="Phone"></figure>888-972-6211</a></li>
                  <li><a href=""><figure><img src="images/mail.jpg" alt="Mail"></figure>info@dummytext.com</a></li>
                </ul>
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="ul-container first-ul">
                  <h4>Conditions</h4>
                  <ul>
                    <li><a href="">Cognitive Behaviour</a></li>
                    <li><a href="">Depression Help</a></li>
                    <li><a href="">Anxiety Help</a></li>
                    <li><a href="">Panic Attacks</a></li>
                    <li><a href="">Stres Relief</a></li>
                    <li><a href="">Eating Disorders</a></li>
                    <li><a href="">Grief Support</a></li>
                  </ul>
                </div><!-- .ul-container -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="ul-container second-ul">
                  <h4 class="less-margin">Useful Links</h4>
                  <ul>
                    <li><a href="">Experts</a></li>
                    <li><a href="">Discussions</a></li>
                    <li><a href="">Most asked questions</a></li>
                    <li><a href="">Motivational Quotes</a></li>
                    <li><a href="">Articles</a></li>
                    <li><a href="">De-stressing techniques</a></li>
                    <li><a href="">Press</a></li>
                  </ul>
                </div><!-- .ul-container -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="ul-container third-ul">
                  <h4>About Us</h4>
                  <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="">Blog</a></li>
                    <li><a href="">News</a></li>
                    <li><a href="">Our Therapist</a></li>
                  </ul>
                </div><!-- .ul-container -->
              </div><!-- .col-md-3 -->
          </div><!-- .row -->
        </div><!-- .container -->
        <div class="bottom-footer">
          <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                  <p><i class="fa fa-copyright" aria-hidden="true"></i> All Copyright | reserved to Text Therapy</p>
                </div><!-- .col-md-6 -->
                <div class="col-md-6 col-sm-6 text-right">
                  <ul>
                    <li><a href="index.html">Terms of Service</a></li>
                    <li><a href="">Privacy Policy</a></li>
                    <li><a href="">FAQ's</a></li>
                  </ul>
                </div><!-- .col-md-6 -->
            </div><!-- .row -->
          </div><!-- .container -->
        </div><!-- .bottom-footer -->
      </footer>

      <script src="js/jquery.js"></script>
      <script src="js/ion.rangeSlider.js"></script>
      <script src="js/bootstrap.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
      <script src="js/custom.js"></script>
      <script src="js/placeholders.min.js"></script>
      <script>
        /* TRIGERRING rangeSlider */
        $(function () {
            $("#range").ionRangeSlider({
                hide_min_max: true,
                keyboard: true,
                min: 90,
                max: 399,
                from: 100,
                to: 399,
                type: 'double',
                step: 1,
                prefix: "$",
                grid: true
            });
        });
      </script>
   </body>
</html>

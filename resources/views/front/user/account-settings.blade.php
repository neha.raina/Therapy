@extends('front.layouts.app')

@section ('content')

         
              <div class="rightbar">
                <ul class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li class="active"><a href="#">Account Settings</a></li>
                </ul>
                <div class="sidebarRight">
                  <div class="accountSettingForm">
                      <div class="createAccountForm">
                          <div class="crateformInner">

                            <div class="form">
                                <div class="formTop">
                                  <figure>
                                      <img src="images/userAccount.png" alt="">
                                  </figure>
                                  <div class="browseFile">
                                      <div class="browseLeft">
                                          <label class="" for="my-file-selector">
                                        <input id="my-file-selector" type="file" style="display:none;" onchange='$("#upload-file-info").html($(this).val());'>
                                        Upload picture
                                    </label>
                                    <span class='label label-info' id="upload-file-info"></span>
                                      </div>
                                      <div class="delete"><a href="javascript:void(0);">Delete</a></div>



                            </div>
                               <div class="row">
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control"   placeholder="First Name">
                                  </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control"   placeholder="Last Name">
                                  </div>
                                 </div>
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                          <input type="email" class="form-control"   placeholder="Email">
                                    </div>
                                 </div>
                                 <div class="col-sm-12">
                                   <div class="form-group">
                                          <input type="text" class="form-control"   placeholder="Password">
                                    </div>
                                 </div>

                                 <div class="col-sm-12 subscribe text-left">
                                    <input id="box1" type="checkbox" />
                                    <label for="box1">Subscribe Newsletter</label>
                                  </div>
                                 <div class="col-sm-12">
                                    <button type="submit" class="btn btnSubmit">Create Account</button>
                                  </div>

                              </div>
                            </div>
                          </div>

                        </div>
                  </div>

                </div><!-- .rightbar-content -->
              </div><!-- .rightbar -->
           </div><!-- .col-md-9 -->
         </div><!-- .row -->
       </div><!-- .container-fluid -->
       </div>
     </section><!-- .sidebar-container -->

@endsection
<div id="chat_1" class="tab-pane fade active in">
                          <div class="container-fluid">
                            <div class="row">
                              <div class="chatsContainer">
                                <div class="col-lg-3 col-md-4 pd0">
                                  <div class="chats-box left-box">
                                    <figure class="pull-left"><img src="images/profile-2-circle.png" alt="review image"></figure>
                                    <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>Johnny Smith</h3>
                                    <p class="date">9 Mar 2016</p>
                                  </div><!-- .chats-box -->
                                </div><!-- .col-md-4 -->
                                <div class="col-lg-9 col-md-8 pd0">
                                  <div class="messageSection sent">
                                    <p class="message-text">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, There is no one
                                      who loves pain itself, who seeks after it and wants to have it. que porro quisquam est qui dolorem
                                      ipsum quia dolor sit amet, consectetur</p>
                                  </div><!-- messageSection -->
                                </div><!-- .col-md-8 -->
                              </div><!-- .chatsContainer -->
                            </div><!-- .row -->
                            <div class="row">
                              <div class="chatsContainer">
                                <div class="col-lg-9 col-md-8 pd0">
                                  <div class="messageSection received">
                                    <p class="message-text">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, There is no one
                                      who loves pain itself, who seeks after it and wants to have it. que porro quisquam est qui dolorem
                                      ipsum quia dolor sit amet, consectetur</p>
                                  </div><!-- messageSection -->
                                </div><!-- .col-md-8 -->
                                <div class="col-lg-3 col-md-4 pd0">
                                  <div class="chats-box right-box">
                                    <figure class="pull-right"><img src="images/profile-1-circle.png" alt="review image"></figure>
                                    <h3>Richa Chandra</h3>
                                    <p class="date">9 Mar 2016</p>
                                  </div><!-- .chats-box -->
                                </div><!-- .col-md-4 -->
                              </div><!-- .chatsContainer -->
                            </div><!-- .row -->
                            <div class="row">
                              <div class="chatsContainer">
                                <div class="col-lg-3 col-md-4 pd0">
                                  <div class="chats-box left-box">
                                    <figure class="pull-left"><img src="images/profile-2-circle.png" alt="review image"></figure>
                                    <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>Johnny Smith</h3>
                                    <p class="date">9 Mar 2016</p>
                                  </div><!-- .chats-box -->
                                </div><!-- .col-md-4 -->
                                <div class="col-lg-9 col-md-8 pd0">
                                  <div class="messageSection sent">
                                    <figure class="pull-left"><img src="images/uploaded_image.jpg" alt="upload"></figure>
                                    <p class="image-name">image003<span>.jpg</span></p>
                                    <div class="pull-right">
                                      <a href="" data-toggle="tooltip" title="View" data-placement="bottom"><i class="fa fa-search"></i></a>
                                      <a href="" data-toggle="tooltip" title="Download" data-placement="bottom"><figure><img src="images/download.png" alt="Download"></figure></a>
                                    </div>
                                    <p class="image-info">4 days ago . <span>24.22KB</span></p>
                                  </div><!-- messageSection -->
                                </div><!-- .col-md-8 -->
                              </div><!-- .chatsContainer -->
                            </div><!-- .row -->
                          </div><!-- .container-fluid -->
                        </div><!-- #chatOne -->
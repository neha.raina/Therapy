@extends('front.layouts.app')

@section ('content')


         
              <div class="rightbar messagesRightbar">
                <ul class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li class="active"><a href="#">Messages</a></li>
                </ul>
                <div class="container-fluid messagesContainer">
                  <div class="row">
                    <div class="col-lg-3 col-md-4 pd0 leftUsers">
                      <div class="users">
                        <ul class="nav nav-tabs nav-stacked">
                            
                            @foreach ( $users as $user)
                            
                            
                            <li onclick="getId({{ $user->pkUserID }})" ><a data-toggle="tab" href="#chat_{{ $user->pkUserID }}">
                            <div class="row">
                              <div class="col-md-3 col-sm-3 col-xs-3 pd0">
                                <figure class="users-img"><img src="images/profile-1-circle.png" alt="review image"></figure>
                              </div><!-- .col-md-3 -->
                              <div class="col-md-9 col-sm-9 col-xs-9 pd0">
                                <div class="users-descp">
                                  <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>{{ $user->name }}</h3>
                                  <figure class="pull-right"><img src="images/delete_chat.png" alt="Delete Image"></figure>
                                  <p class="status">Consectetur adipiscing elit. Aenean</p>
                                  <p class="date">9 Mar 2016</p>
                                </div><!-- .users-descp -->
                              </div><!-- .col-md-9 -->
                            </div><!-- .row -->
                          </a></li>
                          
                          @endforeach
                          
                          <li class="active"><a data-toggle="tab" href="#chatTwo">
                            <div class="row">
                              <div class="col-md-3 col-sm-3 col-xs-3 pd0">
                                <figure class="users-img"><img src="images/profile-2-circle.png" alt="review image"></figure>
                              </div><!-- .col-md-3 -->
                              <div class="col-md-9 col-sm-9 col-xs-9 pd0">
                                <div class="users-descp">
                                  <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>John Smith</h3>
                                  <figure class="pull-right"><img src="images/delete_chat.png" alt="Delete Image"></figure>
                                  <p class="status">Consectetur adipiscing elit. Aenean</p>
                                  <p class="date">9 Mar 2016</p>
                                </div><!-- .users-descp -->
                              </div><!-- .col-md-9 -->
                            </div><!-- .row -->
                          </a></li>
                          
                        </ul>
                      </div><!-- .users -->
                    </div><!-- .col-md-4 -->
                    <div class="col-lg-9 col-md-8 pd0 rightUsers">
                      <div class="tab-content chats">
                        <div id="chat_1" class="tab-pane fade">
                          <div class="container-fluid">
                            <div class="row">
                              <div class="chatsContainer">
                                <div class="col-lg-3 col-md-4 pd0">
                                  <div class="chats-box left-box">
                                    <figure class="pull-left"><img src="images/profile-2-circle.png" alt="review image"></figure>
                                    <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>Johnny Smith</h3>
                                    <p class="date">9 Mar 2016</p>
                                  </div><!-- .chats-box -->
                                </div><!-- .col-md-4 -->
                                <div class="col-lg-9 col-md-8 pd0">
                                  <div class="messageSection sent">
                                    <p class="message-text">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, There is no one
                                      who loves pain itself, who seeks after it and wants to have it. que porro quisquam est qui dolorem
                                      ipsum quia dolor sit amet, consectetur</p>
                                  </div><!-- messageSection -->
                                </div><!-- .col-md-8 -->
                              </div><!-- .chatsContainer -->
                            </div><!-- .row -->
                            <div class="row">
                              <div class="chatsContainer">
                                <div class="col-lg-9 col-md-8 pd0">
                                  <div class="messageSection received">
                                    <p class="message-text">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, There is no one
                                      who loves pain itself, who seeks after it and wants to have it. que porro quisquam est qui dolorem
                                      ipsum quia dolor sit amet, consectetur</p>
                                  </div><!-- messageSection -->
                                </div><!-- .col-md-8 -->
                                <div class="col-lg-3 col-md-4 pd0">
                                  <div class="chats-box right-box">
                                    <figure class="pull-right"><img src="images/profile-1-circle.png" alt="review image"></figure>
                                    <h3>Richa Chandra</h3>
                                    <p class="date">9 Mar 2016</p>
                                  </div><!-- .chats-box -->
                                </div><!-- .col-md-4 -->
                              </div><!-- .chatsContainer -->
                            </div><!-- .row -->
                            <div class="row">
                              <div class="chatsContainer">
                                <div class="col-lg-3 col-md-4 pd0">
                                  <div class="chats-box left-box">
                                    <figure class="pull-left"><img src="images/profile-2-circle.png" alt="review image"></figure>
                                    <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>Johnny Smith</h3>
                                    <p class="date">9 Mar 2016</p>
                                  </div><!-- .chats-box -->
                                </div><!-- .col-md-4 -->
                                <div class="col-lg-9 col-md-8 pd0">
                                  <div class="messageSection sent">
                                    <figure class="pull-left"><img src="images/uploaded_image.jpg" alt="upload"></figure>
                                    <p class="image-name">image003<span>.jpg</span></p>
                                    <div class="pull-right">
                                      <a href="" data-toggle="tooltip" title="View" data-placement="bottom"><i class="fa fa-search"></i></a>
                                      <a href="" data-toggle="tooltip" title="Download" data-placement="bottom"><figure><img src="images/download.png" alt="Download"></figure></a>
                                    </div>
                                    <p class="image-info">4 days ago . <span>24.22KB</span></p>
                                  </div><!-- messageSection -->
                                </div><!-- .col-md-8 -->
                              </div><!-- .chatsContainer -->
                            </div><!-- .row -->
                          </div><!-- .container-fluid -->
                        </div><!-- #chatOne -->
                        <div id="chat_2" class="tab-pane fade in active">
                          <div class="container-fluid">
                            <div class="row">
                              <div class="chatsContainer">
                                <div class="col-lg-3 col-md-4 pd0">
                                  <div class="chats-box left-box">
                                    <figure class="pull-left"><img src="images/profile-2-circle.png" alt="review image"></figure>
                                    <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>Johnny Smith</h3>
                                    <p class="date">9 Mar 2016</p>
                                  </div><!-- .chats-box -->
                                </div><!-- .col-md-4 -->
                                <div class="col-lg-9 col-md-8 pd0">
                                  <div class="messageSection sent">
                                    <p class="message-text">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, There is no one
                                      who loves pain itself, who seeks after it and wants to have it. que porro quisquam est qui dolorem
                                      ipsum quia dolor sit amet, consectetur</p>
                                  </div><!-- messageSection -->
                                </div><!-- .col-md-8 -->
                              </div><!-- .chatsContainer -->
                            </div><!-- .row -->
                            <div class="row">
                              <div class="chatsContainer">
                                <div class="col-lg-9 col-md-8 pd0">
                                  <div class="messageSection received">
                                    <p class="message-text">Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, There is no one
                                      who loves pain itself, who seeks after it and wants to have it. que porro quisquam est qui dolorem
                                      ipsum quia dolor sit amet, consectetur</p>
                                  </div><!-- messageSection -->
                                </div><!-- .col-md-8 -->
                                <div class="col-lg-3 col-md-4 pd0">
                                  <div class="chats-box right-box">
                                    <figure class="pull-right"><img src="images/profile-1-circle.png" alt="review image"></figure>
                                    <h3>Richa Chandra</h3>
                                    <p class="date">9 Mar 2016</p>
                                  </div><!-- .chats-box -->
                                </div><!-- .col-md-4 -->
                              </div><!-- .chatsContainer -->
                            </div><!-- .row -->
                            <div class="row">
                              <div class="chatsContainer">
                                <div class="col-lg-3 col-md-4 pd0">
                                  <div class="chats-box left-box">
                                    <figure class="pull-left"><img src="images/profile-2-circle.png" alt="review image"></figure>
                                    <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>Johnny Smith</h3>
                                    <p class="date">9 Mar 2016</p>
                                  </div><!-- .chats-box -->
                                </div><!-- .col-md-4 -->
                                <div class="col-lg-9 col-md-8 pd0">
                                  <div class="messageSection sent">
                                    <figure class="pull-left"><img src="images/uploaded_image.jpg" alt="upload"></figure>
                                    <p class="image-name">image003<span>.jpg</span></p>
                                    <div class="pull-right">
                                      <a href="" data-toggle="tooltip" title="View" data-placement="bottom"><i class="fa fa-search"></i></a>
                                      <a href="" data-toggle="tooltip" title="Download" data-placement="bottom"><figure><img src="images/download.png" alt="Download"></figure></a>
                                    </div>
                                    <p class="image-info">4 days ago . <span>24.22KB</span></p>
                                  </div><!-- messageSection -->
                                </div><!-- .col-md-8 -->
                              </div><!-- .chatsContainer -->
                            </div><!-- .row -->
                          </div><!-- .container-fluid -->
                        </div><!-- #chatTwo -->
                       </div><!-- .chats -->
                      <div class="sending-section">
                        <div class="file-section text-center">
                          <p>Drop files here to attach them</p>
                          <p>or</p>
                          <label class="fileContainer">Select Files<input type="file"></label>
                        </div><!-- .sending-section -->
                        <div class="message-section">
                          <textarea class="form-control" placeholder="Write your message here...."></textarea>
                          <div class="checkbox">
                            <input id="box1" type="checkbox" />
                            <label for="box1">Enter Sends a message.</label>
                          </div>
                          <a href="" class="btn btn-first">Send</a>
                        </div><!-- .message-section -->
                      </div><!-- .sending-section -->
                    </div><!-- .col-md-8 -->
                  </div><!-- .row -->
                </div><!-- .container-fluid -->
              </div><!-- .rightbar -->
           </div><!-- .col-md-9 -->
         </div><!-- .row -->
       </div><!-- .container-fluid -->
     </section><!-- .sidebar-container -->

@endsection

@section('after-scripts')
     <script>
         function getId(id) 
         { 
             $.ajax({

                type: "GET",
                url: 'messages/'+id,
                //type:"html",
                success: function (data) {
                    console.log(data);

                   // $("#task" + task_id).remove();
                },
                error: function (data) {
                    console.log('Error:', data);
                }
               });
         }
      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
      });
      </script>
 @endsection

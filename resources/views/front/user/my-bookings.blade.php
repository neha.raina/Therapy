@extends('front.layouts.app')

@section ('content')
<?php
//          echo '<pre>';
//          print_r($myTherapists[0]);
//          die('sanjay');
?>

  
              <div class="rightbar mybookingsRightbar">
                <ul class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li class="active"><a href="#">My Bookings</a></li>
                </ul>
                <div class="container-fluid therapistContainer mybookingsContainer">
                  <div class="row">
                      @foreach ($myTherapists as $myTherapist)
                      
                    <div class="col-lg-3 col-md-6">
              				<div class="result-container">
              					<figure>
              						<img class="profile-img" src="{{ profilePic($myTherapist->therapistProfilePic) }}" alt="profile image">
              					</figure>
              					<div class="profile">
                          <h3><span class="pull-left"><i class="fa fa-circle" aria-hidden="true"></i></span>
                              {!! $myTherapist->user->name !!}<span class="pull-right text-booked">Booked</span></h3>
              						<p class="exp">Experience <span>{!! $myTherapist->therapistExperience !!} Years</span></p>
                          <div class="cal-box">
                            <figure><img src="images/calendar.jpg" alt="calendar"></figure>
                            <p>9 Mar 2016, Thursday</p>
                            <figure class="time"><img src="images/time.jpg" alt="time"></figure>
                            <p>09:30 AM</p>
                          </div>
              						<div class="booking">
              							<a href="" class="btn btn-third">Cancel Booking</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="camera"><a href=""></a></li>
                              </ul>
                            </div>
              						</div>
              					</div><!-- .profile -->
              				</div><!-- .result-container -->
              			</div> 
                   @endforeach
                  </div><!-- .row -->
                </div><!-- .container-fluid -->
              </div><!-- .rightbar -->
           </div><!-- .col-md-9 -->
         </div><!-- .row -->
       </div><!-- .container-fluid -->
     </section><!-- .sidebar-container -->

    @endsection 
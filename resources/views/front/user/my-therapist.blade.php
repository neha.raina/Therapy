@extends('front.layouts.app')

@section ('content')

  <div class="rightbar MytherapistRighbar">
                <ul class="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li class="active"><a href="#">My Therapists</a></li>
                </ul>
                <div class="container-fluid therapistContainer mytherapistContainer">
                  <div class="row">
                    <div class="col-lg-3 col-md-6">
              				<div class="result-container">
              					<figure>
              						<img class="profile-img" src="images/dashboard-1.jpg" alt="Profile Image">
              					</figure>
              					<div class="profile">
              						<h3>Rosan Martin
              							<span class="pull-right">
              								<i class="fa fa-circle" aria-hidden="true"></i>Online
              							</span>
              						</h3>
            							<span class="therapist_descp">
            								<i class="fa fa-star selected" aria-hidden="true"></i>
            								<i class="fa fa-star selected" aria-hidden="true"></i>
            								<i class="fa fa-star selected" aria-hidden="true"></i>
            								<i class="fa fa-star" aria-hidden="true"></i>
            								<i class="fa fa-star" aria-hidden="true"></i>
            							</span>
              						<p class="exp">Experience <span>2-3 Years</span></p>
              						<div class="booking">
              							<a href="" class="btn btn-third">Book Now</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="heart"><a href=""></a></li>
                                <li class="camera"><a href=""></a></li>
                                <li class="messages"><a href=""></a></li>
                              </ul>
                            </div>
              						</div>
              					</div><!-- .profile -->
              				</div><!-- .result-container -->
              			</div> <!-- .col-md-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-2.jpg" alt="Profile Image 2">
                        </figure>
                        <div class="profile">
                          <h3>Rosan Martin
                            <span class="pull-right">
                              <i class="fa fa-circle" aria-hidden="true"></i>Online
                            </span>
                          </h3>
                          <span class="therapist_descp">
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                          </span>
                          <p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="booking">
                            <a href="" class="btn btn-third">Book Now</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="heart"><a href=""></a></li>
                                <li class="camera"><a href=""></a></li>
                                <li class="messages"><a href=""></a></li>
                              </ul>
                            </div>
                          </div>
                        </div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-md-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-3.jpg" alt="Profile Image three">
                        </figure>
                        <div class="profile">
                          <h3>Rosan Martin
                            <span class="pull-right">
                              <i class="fa fa-circle" aria-hidden="true"></i>Online
                            </span>
                          </h3>
                          <span class="therapist_descp">
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                          </span>
                          <p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="booking">
                            <a href="" class="btn btn-third">Book Now</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="heart"><a href=""></a></li>
                                <li class="camera"><a href=""></a></li>
                                <li class="messages"><a href=""></a></li>
                              </ul>
                            </div>
                          </div>
                        </div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-md-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-5.jpg" alt="Profile Image Four">
                        </figure>
                        <div class="profile">
                          <h3>Rosan Martin
                            <span class="pull-right">
                              <i class="fa fa-circle" aria-hidden="true"></i>Online
                            </span>
                          </h3>
                          <span class="therapist_descp">
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                          </span>
                          <p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="booking">
                            <a href="" class="btn btn-third">Book Now</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="heart"><a href=""></a></li>
                                <li class="camera"><a href=""></a></li>
                                <li class="messages"><a href=""></a></li>
                              </ul>
                            </div>
                          </div>
                        </div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-md-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-4.jpg" alt="Image Profile five">
                        </figure>
                        <div class="profile">
                          <h3>Rosan Martin
                            <span class="pull-right">
                              <i class="fa fa-circle" aria-hidden="true"></i>Online
                            </span>
                          </h3>
                          <span class="therapist_descp">
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                          </span>
                          <p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="booking">
                            <a href="" class="btn btn-third">Book Now</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="heart"><a href=""></a></li>
                                <li class="camera"><a href=""></a></li>
                                <li class="messages"><a href=""></a></li>
                              </ul>
                            </div>
                          </div>
                        </div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-md-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-6.jpg" alt="Image Profile six">
                        </figure>
                        <div class="profile">
                          <h3>Rosan Martin
                            <span class="pull-right">
                              <i class="fa fa-circle" aria-hidden="true"></i>Online
                            </span>
                          </h3>
                          <span class="therapist_descp">
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                          </span>
                          <p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="booking">
                            <a href="" class="btn btn-third">Book Now</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="heart"><a href=""></a></li>
                                <li class="camera"><a href=""></a></li>
                                <li class="messages"><a href=""></a></li>
                              </ul>
                            </div>
                          </div>
                        </div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-md-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-1.jpg" alt="Image Profile seven">
                        </figure>
                        <div class="profile">
                          <h3>Rosan Martin
                            <span class="pull-right">
                              <i class="fa fa-circle" aria-hidden="true"></i>Online
                            </span>
                          </h3>
                          <span class="therapist_descp">
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                          </span>
                          <p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="booking">
                            <a href="" class="btn btn-third">Book Now</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="heart"><a href=""></a></li>
                                <li class="camera"><a href=""></a></li>
                                <li class="messages"><a href=""></a></li>
                              </ul>
                            </div>
                          </div>
                        </div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-md-3 -->
                    <div class="col-lg-3 col-md-6">
                      <div class="result-container">
                        <figure>
                          <img class="profile-img" src="images/dashboard-2.jpg" alt="Image Profile Eight">
                        </figure>
                        <div class="profile">
                          <h3>Rosan Martin
                            <span class="pull-right">
                              <i class="fa fa-circle" aria-hidden="true"></i>Online
                            </span>
                          </h3>
                          <span class="therapist_descp">
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star selected" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                          </span>
                          <p class="exp">Experience <span>2-3 Years</span></p>
                          <div class="booking">
                            <a href="" class="btn btn-third">Book Now</a>
                            <div class="therapist-buttons pull-right">
                              <ul>
                                <li class="heart"><a href=""></a></li>
                                <li class="camera"><a href=""></a></li>
                                <li class="messages"><a href=""></a></li>
                              </ul>
                            </div>
                          </div>
                        </div><!-- .profile -->
                      </div><!-- .result-container -->
                    </div> <!-- .col-md-3 -->
                  </div><!-- .row -->
                </div><!-- .container-fluid -->
              </div><!-- .rightbar -->
           </div><!-- .col-md-9 -->
         </div><!-- .row -->
       </div><!-- .container-fluid -->
     </section><!-- .sidebar-container -->

@endsection
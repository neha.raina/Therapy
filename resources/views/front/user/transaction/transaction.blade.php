<?php
//requirement gathering // make class after catergoring and  relation of the classes
//echo '<pre>';
//print_r($transactions);
//die;
?>
@extends('front.layouts.app')

@section ('content')

<div class="rightbar">
    <ul class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li class="active"><a href="#">My Schedules</a></li>
    </ul>
    <div class="container-fluid therapistContainer transactionContainer">
        <div class="row">

            @include('front.user.transaction.search')

        </div><!-- .row -->
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive tableContainer">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>S. NO</th>
                                <th>Booking Date / Time</th>
                                <th>Booking Mode</th>
                                <th>Client  Name</th>
                                <th>Amount</th>
                                <th>Transaction ID</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($transactions as $transaction) 
                            <tr>
                                <td><input name="" id="checkbox_{{ $transaction->pkTransactionID }}" type="checkbox"><label for="checkbox_{{ $transaction->pkTransactionID }}"></label></td>
                                <td>{{ 'serial no' }}</td>
                                <td>{{ $transaction->created_at }}</td>
                                <td>{{ $transaction->customer->user->email    }}</td>
                                <td>{{ $transaction->customer->user->name  }}</td>
                                <td>{{ $transaction->transactionAmount }}</td>
                                <td> {{ $transaction->transactionPaymentID }}</td>
                                <td><!--    <a href="">Delete</a>-->
                                {!! Form::open(['method' => 'post','route' => ['transaction/delete', $transaction->pkTransactionID],'style'=>'display:inline']) !!}
                                {!! Form::submit('Delete') !!}
                                {!! Form::close() !!}
                                </td>
                            </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div><!-- .searchContainer -->
            </div><!-- .col-md-12 -->
        </div><!-- .row -->
    </div><!-- .container-fluid -->
</div><!-- .rightbar -->
</div><!-- .col-md-9 -->
</div><!-- .row -->
</div><!-- .container-fluid -->
</section><!-- .sidebar-container -->

@endsection
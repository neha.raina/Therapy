<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="format-decetion" content="telephone=no" />
      <meta name="description" content="">
      <meta name="author" content="">
      <link rel="icon" href="images/logo.png">
      <title>Text Therapy | Home</title>
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/fonts.css" rel="stylesheet">
      <link href="css/custom.css" rel="stylesheet">
      <link href="css/owl.carousel.min.css" rel="stylesheet">
      <link href="css/owl.theme.default.min.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">
      <link href="css/responsive.css" rel="stylesheet">
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
      <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css" rel="stylesheet"> -->
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>

     <header>
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <i class="fa fa-bars"></i>
              </button>
              <a class="navbar-brand" href="index.html">
                <figure>
                    <img src="images/logo.png" alt="Logo">
                </figure>
              </a>
            </div> <!--.navbar-header-->
            <div id="navbar" class="collapse navbar-collapse">
              <ul class="nav navbar-nav text-uppercase pull-right">
                <li class="active"><a href="">About Us</a></li>
                <li><a href="">My Therapist</a></li>
                <li><a href="">Services</a></li>
                <li><a href="">Blog</a></li>
                <li><a href="">FAQ's</a></li>
                <li><a href=""><figure><img src="images/login.png" alt="login"></figure>Login</a></li>
                <li><a href=""><figure><img src="images/signup.png" alt="signup"></figure>Signup</a></li>
              </ul>
            </div><!-- .nav-collapse -->
          </div> <!-- .container-fluid -->
        </nav>
      </header>

      <section class="home-banner">
        <div class="container-fluid">
          <div class="row">
              <div class="owl-carousel owl-theme home-carousel">
                  <div class="item">
                    <figure>
                      <img class="img-responsive" src="images/banner.jpg" alt="Banner Image">
                    </figure>
                    <div class="home-caption">
                      <div class="vertical-middle">
                        <h1>Want Someone<br><span>Helpful to talk to you?</span></h1>
                        <p>Chat now with your own kind peer counselor. Breakthrough,<br> see hope, join the tens of thousands helped just like you.</p>
                        <a class="btn btn-second" href="">Get Started</a>
                      </div> <!--.vertical-middle-->
                    </div><!-- .home-caption -->
                  </div><!-- .item -->
                  <div class="item">
                    <figure>
                      <img class="img-responsive" src="images/banner-2.jpg" alt="Banner Image">
                    </figure>
                     <div class="home-caption">
                      <div class="vertical-middle">
                        <h1>Want Someone<br><span>Helpful to talk to you?</span></h1>
                        <p>Chat now with your own kind peer counselor. Breakthrough,<br> see hope, join the tens of thousands helped just like you.</p>
                        <a class="btn btn-second" href="">Get Started</a>
                      </div> <!--.vertical-middle-->
                    </div><!-- .home-caption -->
                  </div><!-- .item -->
                  <div class="item">
                    <figure>
                      <img class="img-responsive" src="images/banner-3.jpg" alt="Banner Image">
                    </figure>
                     <div class="home-caption">
                      <div class="vertical-middle">
                        <h1>Want Someone<br><span>Helpful to talk to you?</span></h1>
                        <p>Chat now with your own kind peer counselor. Breakthrough,<br> see hope, join the tens of thousands helped just like you.</p>
                        <a class="btn btn-second" href="">Get Started</a>
                      </div> <!--.vertical-middle-->
                    </div><!-- .home-caption -->
                  </div><!-- .item -->
                  <!-- <div class="item">
                    <figure>
                      <img class="img-responsive" src="images/banner.jpg" alt="Banner Image">
                    </figure>
                  </div> --><!-- .item -->
              </div><!-- .home-carousel -->

          </div><!-- .row -->
        </div><!-- .container-fluid -->
      </section><!-- .home-banner -->

      <section class="why-therapy">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="heading-box text-center">
                <h2>Why Choose Text Therapy?</h2>
                <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators.</p>
              </div><!-- .why-box -->
            </div><!-- .col-md-12 -->
          </div><!-- .row -->
          <div class="row text-center">
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="box">
                    <figure>
                      <img src="images/available.png" alt="Available">
                    </figure>
                    <h3>24x7 Available</h3>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac</p>
                </div><!-- .box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="box">
                    <figure>
                      <img src="images/affordable.png" alt="Affordable">
                    </figure>
                    <h3>It's Affordable</h3>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac</p>
                </div><!-- .box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="box">
                    <figure>
                      <img src="images/confidential.png" alt="Confidential">
                    </figure>
                    <h3>Confidential</h3>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac</p>
                </div><!-- .box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="box">
                    <figure>
                      <img src="images/experts.png" alt="Experts">
                    </figure>
                    <h3>450+ Experts</h3>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac</p>
                </div><!-- .box -->
              </div><!-- .col-md-3 -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .why-therapy -->

      <section class="professional">
        <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <div class="heading-box text-center">
                    <h2>Our Professional Therapist</h2>
                    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators.</p>
                  </div> <!-- .heading-box -->
              </div><!-- .col-md-12 -->
          </div><!-- .row -->
          <div class="row">
            <div class="col-md-12">
              <div class="owl-carousel owl-theme professional-carousel">
                  <div class="item">
                    <div class="professionalInner">
                      <figure>
                      <img src="images/therapist-1.jpg" alt="Therapist Image">
                      </figure>
                      <div class="member-box">
                          <h4>Puma Benz</h4>
                          <p class="member-para">Set ammet dummy text</p>
                          <p class="read-more">If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined.Sed porta interdum mollis. Nam magna quam.</p>
                          <a href="">Read More</a>
                      </div><!-- .member-box -->
                    </div>
                  </div><!-- .item -->
                  <div class="item">
                    <div class="professionalInner">
                        <figure>
                      <img src="images/therapist-2.jpg" alt="Therapist Image">
                      </figure>
                      <div class="member-box">
                          <h4>Joshlyn Anderson</h4>
                          <p class="member-para">Set ammet dummy text</p>
                          <p class="read-more">If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined.Sed porta interdum mollis. Nam magna quam.</p>
                          <a href="">Read More</a>
                      </div><!-- .member-box -->
                    </div>
                  </div><!-- .item -->
                  <div class="item">
                    <div class="professionalInner">
                      <figure>
                      <img src="images/therapist-3.jpg" alt="Therapist Image">
                      </figure>
                      <div class="member-box">
                          <h4>Tiffany Alex</h4>
                          <p class="member-para">Set ammet dummy text</p>
                          <p class="read-more">If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined.Sed porta interdum mollis. Nam magna quam.</p>
                          <a href="">Read More</a>
                      </div><!-- .member-box -->
                    </div>
                  </div><!-- .item -->
                  <div class="item">
                    <div class="professionalInner">
                      <figure>
                      <img src="images/therapist-4.jpg" alt="Therapist Image">
                      </figure>
                      <div class="member-box">
                          <h4>Shriya Saran</h4>
                          <p class="member-para">Set ammet dummy text</p>
                          <p class="read-more">If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined.Sed porta interdum mollis. Nam magna quam.</p>
                          <a href="">Read More</a>
                      </div><!-- .member-box -->
                    </div>
                  </div><!-- .item -->
                  <div class="item">
                    <div class="professionalInner">
                      <figure>
                      <img src="images/therapist-1.jpg" alt="Therapist Image">
                      </figure>
                      <div class="member-box">
                          <h4>Puma Benz</h4>
                          <p class="member-para">Set ammet dummy text</p>
                          <p class="read-more">If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined.Sed porta interdum mollis. Nam magna quam.</p>
                          <a href="">Read More</a>
                      </div><!-- .member-box -->
                    </div>
                  </div><!-- .item -->
              </div><!-- .professional-carousel -->
            </div><!-- .col-md-12 -->
          </div><!-- .row -->
          <div class="row">
            <div class="col-md-12 text-center">
              <a href="" class="btn btn-first">See all therapist</a>
            </div><!-- .col-md-12 -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .professional -->

      <section class="how-it-works">
        <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <div class="heading-box text-center">
                    <h2>How Text Therapy Works</h2>
                    <p>If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators.</p>
                  </div> <!-- .heading-box -->
              </div><!-- .col-md-12 -->
          </div><!-- .row -->
          <div class="row text-center">
              <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="work-box work-assesment">
                    <figure>
                      <img class="img-responsive" src="images/assesment.png" alt="Therapist Image">
                    </figure>
                    <h4>Get a Assesment</h4>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac, eleifend tristique est.</p>
                  </div><!-- .work-box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="work-box work-match">
                    <figure>
                      <img class="img-responsive" src="images/matched.png" alt="Therapist Image">
                    </figure>
                    <h4>Matched Therapist</h4>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac, eleifend tristique est.</p>
                  </div><!-- .work-box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="work-box work-plan">
                    <figure>
                      <img class="img-responsive" src="images/plan.png" alt="Therapist Image">
                    </figure>
                    <h4>Select a plan</h4>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac, eleifend tristique est.</p>
                  </div><!-- .work-box -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="work-box work-chat">
                    <figure>
                      <img class="img-responsive" src="images/chat.png" alt="Therapist Image">
                    </figure>
                    <h4>Start Chat</h4>
                    <p>Sed porta interdum mollis. Nam magna quam, euismod sed nisl ac, eleifend tristique est.</p>
                  </div><!-- .work-box -->
              </div><!-- .col-md-3 -->
          </div><!-- .row -->
          <div class="row">
              <div class="col-md-12 text-center">
                  <a href="" class="btn btn-second">Find Therapist Now!</a>
              </div><!-- .col-md-12 -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .how-it-works -->

      <section class="testimonials">
        <div class="container">
          <div class="row">
              <div class="col-md-12 text-center">
                <div class="heading-box">
                  <h2>What Happy People Are Saying?</h2>
                </div><!-- .heading-box -->
                <div class="owl-carousel owl-theme testimonial-carousel">
                    <div class="item">
                      <p><i>Nunc tincidunt porta diam, ut molestie eros molestie non. Nullam egestas vehicula urna nec condimentum. Aenean tincidunt, diam eget feugiat congue, mi nisl scelerisque libero, et blandit magna nisl et neque. Donec et porttitor elit. Suspendisse quis pharetra tortor. </i></p>
                      <figure>
                        <img src="images/testimonial.png" alt="Banner Image">
                      </figure>
                    </div><!-- .item -->
                    <div class="item">
                      <p><i>Nunc tincidunt porta diam, ut molestie eros molestie non. Nullam egestas vehicula urna nec condimentum. Aenean tincidunt, diam eget feugiat congue, mi nisl scelerisque libero, et blandit magna nisl et neque. Donec et porttitor elit. Suspendisse quis pharetra tortor. </i></p>
                      <figure>
                        <img src="images/testimonial.png" alt="Banner Image">
                      </figure>
                    </div><!-- .item -->
                    <div class="item">
                      <p><i>Nunc tincidunt porta diam, ut molestie eros molestie non. Nullam egestas vehicula urna nec condimentum. Aenean tincidunt, diam eget feugiat congue, mi nisl scelerisque libero, et blandit magna nisl et neque. Donec et porttitor elit. Suspendisse quis pharetra tortor. </i></p>
                      <figure>
                        <img src="images/testimonial.png" alt="Banner Image">
                      </figure>
                    </div><!-- .item -->
                    <div class="item">
                      <p><i>Nunc tincidunt porta diam, ut molestie eros molestie non. Nullam egestas vehicula urna nec condimentum. Aenean tincidunt, diam eget feugiat congue, mi nisl scelerisque libero, et blandit magna nisl et neque. Donec et porttitor elit. Suspendisse quis pharetra tortor. </i></p>
                      <figure>
                        <img src="images/testimonial.png" alt="Banner Image">
                      </figure>
                    </div><!-- .item -->
                </div> <!-- testimonial-carousel -->
              </div><!-- .col-md-12 -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .testimonials -->

      <section class="newsletter">
        <div class="container">
          <div class="row">
              <div class="newsletter-container">
                <div class="col-md-4">
                  <div class="newsletter-box">
                    <h3>Stay in Touch</h3>
                    <p>Subscribe to our Newsletter</p>
                  </div>
                </div><!-- .col-md-4 -->
                <div class="col-md-8">
                  <form class="form-horizontal">
                    <div class="form-group">
                      <input type="email" class="form-control" id="email" placeholder="Your Email">
                      <button type="submit" class="btn form-button">Subscribe</button>
                    </div>
                  </form>
                </div><!-- .col-md-8 -->
              </div><!-- .newsletter-container -->
          </div><!-- .row -->
        </div><!-- .container -->
      </section><!-- .newsletter -->

      <footer>
        <div class="container">
          <div class="row">
              <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="index.html">
                  <figure>
                    <img src="images/dark-logo.jpg" alt="Logo">
                  </figure>
                </a>
                <ul class="social-buttons">
                  <li><a href=""><img src="images/facebook.png" alt="Facebook"></a></li>
                  <li><a href=""><img src="images/twitter.png" alt="Twitter"></a></li>
                  <li><a href=""><img src="images/linkedin.png" alt="Linkedin"></a></li>
                  <li><a href=""><img src="images/google_plus.png" alt="Google Plus"></a></li>
                </ul>
                <ul class="contact-buttons">
                  <li><a href=""><figure><img src="images/phone.jpg" alt="Phone"></figure>888-972-6211</a></li>
                  <li><a href=""><figure><img src="images/mail.jpg" alt="Mail"></figure>info@dummytext.com</a></li>
                </ul>
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="ul-container first-ul">
                  <h4>Conditions</h4>
                  <ul>
                    <li><a href="">Cognitive Behaviour</a></li>
                    <li><a href="">Depression Help</a></li>
                    <li><a href="">Anxiety Help</a></li>
                    <li><a href="">Panic Attacks</a></li>
                    <li><a href="">Stres Relief</a></li>
                    <li><a href="">Eating Disorders</a></li>
                    <li><a href="">Grief Support</a></li>
                  </ul>
                </div><!-- .ul-container -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="ul-container second-ul">
                  <h4 class="less-margin">Useful Links</h4>
                  <ul>
                    <li><a href="">Experts</a></li>
                    <li><a href="">Discussions</a></li>
                    <li><a href="">Most asked questions</a></li>
                    <li><a href="">Motivational Quotes</a></li>
                    <li><a href="">Articles</a></li>
                    <li><a href="">De-stressing techniques</a></li>
                    <li><a href="">Press</a></li>
                  </ul>
                </div><!-- .ul-container -->
              </div><!-- .col-md-3 -->
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="ul-container third-ul">
                  <h4>About Us</h4>
                  <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="">Blog</a></li>
                    <li><a href="">News</a></li>
                    <li><a href="">Our Therapist</a></li>
                  </ul>
                </div><!-- .ul-container -->
              </div><!-- .col-md-3 -->
          </div><!-- .row -->
        </div><!-- .container -->
        <div class="bottom-footer">
          <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                  <p><i class="fa fa-copyright" aria-hidden="true"></i> All Copyright | reserved to Text Therapy</p>
                </div><!-- .col-md-6 -->
                <div class="col-md-6 col-sm-6 text-right">
                  <ul>
                    <li><a href="index.html">Terms of Service</a></li>
                    <li><a href="">Privacy Policy</a></li>
                    <li><a href="">FAQ's</a></li>
                  </ul>
                </div><!-- .col-md-6 -->
            </div><!-- .row -->
          </div><!-- .container -->
        </div><!-- .bottom-footer -->
      </footer>

      <script src="js/jquery.js"></script>
      <script src="js/bootstrap.js"></script>
      <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script> -->
      <script src="js/owl.carousel.min.js"></script>
      <script src="js/custom.js"></script>
      <script src="js/placeholders.min.js"></script>
   </body>
</html>

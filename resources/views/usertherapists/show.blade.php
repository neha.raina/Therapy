@include('common-crud.show', 
	[
		'wigetTitle' => 'User Details',
		'model' => $usertherapists,
		'columns' => [
			'pkUserID',
			'userEmail',
			'userFirstName',
			'userLastName',
			'userGender'
		]
	])

@extends('common-crud._search')

@section('form')

	{{ Form::model($searchModel, ['route' => ['usertherapist.index', ''], 'class' => '', 'role' => 'form', 'method' => 'get']) }}

		<div class="row">

			<div class="col-md-3">
				{{Form::textInput($searchModel, 'pkTherapistID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'fkUserID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'therapistProfilePic')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'therapistAddress')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'therapistQualification')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'therapistExperience')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'therapistDescription')}}
			</div>
					
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12">
				{{ Form::submit(___('SEARCH'), ['class' => 'btn btn-success']) }}
				&nbsp; 
				<a href="{{URL::route($baseRouteName. '.index')}}" class="btn btn-default">{{ ___('RESET') }}</a>
			</div>
		</div>

    {{ Form::close() }}
	
@endsection
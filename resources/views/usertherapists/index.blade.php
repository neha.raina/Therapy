@include('common-crud.index', [
	'pageTitle' => ___('Usertherapist List'),
	'pageHeading' => ___('Usertherapist List'),
	'listWidgetHeading' => ___('Usertherapist List'),
	
	'baseRouteName' => 'usertherapist',
	'searchRoute' => 'usertherapists._search',

	'modelName' => '\App\Models\Usertherapists',
	'models' => $models,
	'searchModel' => $searchModel,

	'columns' => [
		[
			'type' => 'index',
			'label' => '#',
		],
		
                [
			'label' => ___('Therapist Name'),
		        'value' => function($model){
                            return $model->user->name;
			}
		],
		[
			'label' =>___('Therapist Email'),
		        'value' => function($model){
                           return $model->user->email;;
			}
		], 
		'therapistProfilePic',
		'therapistAddress',
		'therapistQualification',
		'therapistExperience',
		'therapistDescription',
		[
			'type' => 'action',
		]
	]

])
@extends('adminlte::page')
@section('title', 'Usertherapist Form')

@section('content_header')
    <h1 class="pull-left">Add Usertherapist</h1>
	<div class="clearfix"></div>
@stop

@section('content')

	@include('common-crud.alert')

	<?php  
		$currentRoute =  Route::currentRouteName();
		$baseRouteName = substr( $currentRoute, 0, strrpos($currentRoute, '.') );
	?>
	
	<div class="row">
		<div class="col-md-12">
			@include('usertherapists._form', ['model' => $model])	
		</div><!-- end of .col-md-6 -->
	</div><!-- end of .row -->
	

@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
    <link rel="stylesheet" href="{{asset('css/backend_custom.css')}}">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

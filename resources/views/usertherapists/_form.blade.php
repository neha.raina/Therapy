<?php $route = 'usertherapist.';
$idValue = null;
$method = null;
$isUpdate = false;
if (!$model->exists) {
	$route .= 'store';
	$method = 'POST';
} else {
	$isUpdate = true;
	$route .= 'update';
	$method = 'PATCH';
	$idValue = $model->{$model->primaryKey};
}
?>

{{ Form::model($model, ['route' => [$route, $idValue], 'method' => $method])}}
	{{ csrf_field() }}
	<div class="panel panel-default panel-table">
		<div class="panel-heading">
			<h4>{{($isUpdate)?___('Update'):___('New')}} Usertherapist</h4>
		</div>
		<div class="panel-body">
			<div class="row">

			<div class="col-md-3">
				{{Form::textInput($model, 'fkUserID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'therapistProfilePic')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'therapistAddress')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'therapistQualification')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'therapistExperience')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($model, 'therapistDescription')}}
			</div>


			</div>

			<div class="row">
				<div class="col-md-12">
					<br>
					<button type="submit" class="btn btn-primary" >{{___('SAVE')}}</button>
					&nbsp; 
					<a href="{{URL::route($baseRouteName. '.index')}}" class="btn btn-default">{{___('BACK')}}</a>
				</div>
			</div>
		</div>
	</div>

{{ Form::close() }}

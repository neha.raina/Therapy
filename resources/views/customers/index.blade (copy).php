@include('common-crud.index', [
	'pageTitle' => ___('Customer List'),
	'pageHeading' => ___('Customer List'),
	'listWidgetHeading' => ___('Customer List'),
	
	'baseRouteName' => 'customer',
	'searchRoute' => 'customers._search',

	'modelName' => '\App\Models\Customers',
	'models' => $models,
	'searchModel' => $searchModel,

	'columns' => [
		[
			'type' => 'index',
			'label' => '#',
		],
		[
			'lab1el' => ___('Name'),
			<!--'attribute' => 'userFirstName',-->
			'value' => function($model){
				return $model->getName();
			}
		],
		'Customer Email ',
		'customerMobile',
		'customerProfilePic',
		'customerAddress',
		[
			'type' => 'action',
		]
	]

])
@include('common-crud.index', [
	'pageTitle' => ___('Customer List'),
	'pageHeading' => ___('Customer List'),
	'listWidgetHeading' => ___('Customer List'),
	
	'baseRouteName' => 'customer',
	'searchRoute' => 'customers._search',

	'modelName' => '\App\Models\Customer',
	'models' => $models,
	'searchModel' => $searchModel,

	'columns' => [
		[
			'type' => 'index',
			'label' => '#',
		],
		[
			'label' => ___('Customer Name'),
		        'value' => function($model){
                            return $model->user->name;
			}
		],
		[
			'label' =>___('Customer Email'),
		        'value' => function($model){
                           return $model->user->email;;
			}
		],
	
		'customerMobile',
		'customerProfilePic',
		'customerAddress',
		[
			'type' => 'action',
		]
	]

])
@include('common-crud.show', 
	[
		'wigetTitle' => 'User Details',
		'model' => $customers,
		'columns' => [
			'pkUserID',
			'userEmail',
			'userFirstName',
			'userLastName',
			'userGender'
		]
	])

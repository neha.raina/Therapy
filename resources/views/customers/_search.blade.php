@extends('common-crud._search')

@section('form')

	{{ Form::model($searchModel, ['route' => ['customer.index', ''], 'class' => '', 'role' => 'form', 'method' => 'get']) }}

		<div class="row">

			<div class="col-md-3">
				{{Form::textInput($searchModel, 'pkCustomerID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'fkUserID')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'customerMobile')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'customerProfilePic')}}
			</div>
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'customerAddress')}}
			</div>
					
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12">
				{{ Form::submit(___('SEARCH'), ['class' => 'btn btn-success']) }}
				&nbsp; 
				<a href="{{URL::route($baseRouteName. '.index')}}" class="btn btn-default">{{ ___('RESET') }}</a>
			</div>
		</div>

    {{ Form::close() }}
	
@endsection
@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
    <h1 class="pull-left">Details</h1>
	<div class="clearfix"></div>
@stop

@section('content')
	
	<div class="row">
		<div class="col-md-6">

			@include('common-crud.alert')
			
			<div class="panel panel-default panel-table">
				<div class="panel-heading">
					<h4>{{$wigetTitle}}</h4>
				</div>
				<div class="panel-body">
					<table class="table table-hover table-striped table-bordered">
						<tbody>
							@foreach ($columns as $column)
								<tr>
									<th>{{$model::attributeLabel($column)}}</th>
									<td>{{$model->$column}}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		
		</div><!-- end of .col-md-6 -->
	</div><!-- end of .row -->
	
	<?php 
		$currentRoute =  Route::currentRouteName();
		$indexRoute = substr( $currentRoute, 0, strrpos($currentRoute, '.') + 1). 'index';
	?>
		<a href="{{URL::route($indexRoute)}}" class="btn btn-default">BACK</a>

@endsection

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

<div class="panel panel-default">
	<div class="panel-heading">
		<h4>{{___('Search')}}</h4>
	</div>
	<div class="panel-body">
    	
    	@yield('form')
	
	</div>
</div>
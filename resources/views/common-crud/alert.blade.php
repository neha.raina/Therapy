<?php
$messageTypes = ['error', 'success', 'warning', 'info'];
$index = 1;
?>
@foreach ($messageTypes as $type)
	@if(Session::has($type))
		<?php 
		$messages = Session::get($type);
		if ($messages instanceof Illuminate\Support\MessageBag)
			$messages = $messages->all();
		if (is_string($messages))
			$messages = [$messages];
		?>
		@foreach ($messages as $message)
		<div class="alert alert-{{$type}} alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			{{$message}}
		</div>
		@endforeach
	@endif
@endforeach

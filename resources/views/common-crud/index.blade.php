@extends('adminlte::page')
@section('title', $pageTitle)

@section('content_header')
    <h1 class="pull-left">
    	@if (isset($pageHeading)) 
    		{{$pageHeading}} 
    	@else
    		{{$pageTitle}}
    	@endif
   	</h1>
	<a href="{{URL::route($baseRouteName. '.create')}}" class="btn btn-primary pull-right">{{___('ADD NEW')}}</a>
	<div class="clearfix"></div>
@stop

@section('content')
	
	@include('common-crud.alert')

	@include($searchRoute, ['baseRouteName' => $baseRouteName, 'searchModel' => $searchModel])

    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">
                	<h4>{{$listWidgetHeading}}</h4>
               	</div>
                <div class="panel-body">

					<table class="dataTable table table-hover table-striped">
						<thead>
							<tr>
							@foreach ($columns as $column)
								<?php $columnLabel = ''; ?>
								@if (is_array($column))
									
									@if (array_key_exists('label', $column))
										<?php $columnLabel = $column['label']; ?>
									@else

										@if (array_key_exists('type', $column)) 

											@if ($column['type'] == 'index')
												<?php $columnLabel = '#'; ?>
											@elseif ($column['type'] == 'action')
												<?php $columnLabel = ___('Action'); ?>
											@endif

										@elseif (isset($column['attribute']))
											<?php $columnLabel = $modelName::attributeLabel($column['attribute']); ?>
										@endif

									@endif

								@else
									<?php $columnLabel = $modelName::attributeLabel($column); ?>
								@endif

								<th>
									{{-- avoid sorting of index, action or complex columns --}}
									@if (is_string($column))
										
									@elseif (is_array($column) && isset($column['attribute']))

									@else

									@endif
								
									{{$columnLabel}}
								
								</th>
							
							@endforeach
							</tr>
						</thead>

						<tbody>
							<?php $index = 1; ?>
							@foreach ($models as $model)
								<tr>
									@foreach ($columns as $column)
										
										@if (is_array($column))

											@if (array_key_exists('type', $column)) 

												@if ($column['type'] == 'index')
													<td>{{$index++}}</td>
												@elseif ($column['type'] == 'action')
													<td class="action-column">
														@if (isset($column['template']))
															
														@else
															<a href="{{URL::route($baseRouteName. '.show', [ 'id' => $model->{$model->primaryKey}] )}}" class="btn btn-warning"><i class="fa fa-fw fa-eye"></i></a>
															<a href="{{URL::route($baseRouteName. '.edit', [ 'id' => $model->{$model->primaryKey}] )}}" class="btn btn-info"><i class="fa fa-fw fa-pencil"></i></a>
															
															{{Form::open(['url' => URL::route($baseRouteName. '.destroy', [ 'id' => $model->{$model->primaryKey}] ), 'method' => 'DELETE', 'onsubmit' => 'return confirmDelete()' ])}}
																<button class="btn btn-danger"><i class="fa fa-fw fa-remove"></i></button>
															{{ Form::close() }}
														@endif
													</td>
												@endif

											@elseif (isset($column['value']) && is_callable($column['value']))
												<td>{{call_user_func_array($column['value'], ['model' => $model])}}</td>
											@endif


										@else
											<td>{{$model->$column}}</td>
										@endif
									@endforeach

								</tr>
							@endforeach
						</tbody>
					</table>
					
			        <div class="col-md-12 text-center">
						{{$models->appends($_GET)->links()}}
			        </div>
                </div>
            </div>
        </div>
        	
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('/css/backend_custom.css')}}">
@stop

@section('js')
    <script>
    function confirmDelete(){
		
		if (confirm("<?= ___('Are you sure to delete this item ?')?>"))
			return true;
		else
			return false;
	}
    </script>
    <script> console.log('Hi!'); </script>
@stop
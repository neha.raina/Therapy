@include('common-crud.index', [
	'pageTitle' => ___('User List'),
	'pageHeading' => ___('User List'),
	'listWidgetHeading' => ___('User List'),
	
	'baseRouteName' => 'user',
	'searchRoute' => 'user._search',

	'modelName' => '\App\Models\Users',
	'models' => $models,
	'searchModel' => $searchModel,

	'columns' => [
		[
			'type' => 'index',
			'label' => '#',
		],
		'name',
		'email',
                'roleType',
		
		[
			'type' => 'action',
		]
	]

])
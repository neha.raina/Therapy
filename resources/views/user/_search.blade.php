@extends('common-crud._search')

@section('form')

	{{ Form::model($searchModel, ['route' => ['user.index', ''], 'class' => '', 'role' => 'form', 'method' => 'get']) }}

		<div class="row">

				
			<div class="col-md-3">
				{{Form::textInput($searchModel, 'userFirstName')}}
			</div>

			<div class="col-md-3">
				{{Form::textInput($searchModel, 'userLastName')}}
			</div>

			<div class="col-md-3">
				{{Form::dropDown($searchModel, 'userGender', [
					'' => ___('Choose ...'),
					'Male' => ___('Male'),
					'Female' => ___('Female'),
					'Trans' => ___('Trans')
				])}}
			</div>

			<div class="col-md-3">
				{{Form::dropDown($searchModel, 'userRole', [
					'' => ___('Choose ...'),
					'Admin' => ___('Admin'),
					'Sub-Admin' => ___('Sub-Admin'),
					'Company Admin' => ___('Company Admin'),
					'Company Sub-Admin' => ___('Company Sub-Admin'),
					'Employee' => ___('Employee')
				])}}
			</div>

			<div class="col-md-3">
				{{Form::dropDown($searchModel, 'userStatus', [
					'' => ___('Choose ...'),
					'Active' => ___('Active'),
					'Under Verification' => ___('Under Verification'),
					'Suspended' => ___('Suspended'),
					'Deleted' => ___('Deleted')
				])}}
			</div>

			
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12">
				{{ Form::submit(___('SEARCH'), ['class' => 'btn btn-success']) }}
				&nbsp; 
				<a href="{{URL::route($baseRouteName. '.index')}}" class="btn btn-default"><?= ___('RESET') ?></a>
			</div>
		</div>

    {{ Form::close() }}
	
@endsection
<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::group(['namespace' => 'Auth'], function () { 

    /*
     * These routes require the user to be logged in
     */
    Route::group(['middleware' => 'auth'], function () { 
        Route::get('logout', 'LoginController@logout')->name('logout');

        //For when admin is logged in as user from backend
        Route::get('logout-as', 'LoginController@logoutAs')->name('logout-as');

        // Change Password Routes
        Route::get('password/change', 'ChangePasswordController@changePassword');//->name('password.change');
    });

    /*
     * These routes require no user to be logged in
     */
    Route::group(['middleware' => 'guest'], function () { 
        // Authentication Routes
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login')->name('login');

        // Socialite Routes
        Route::get('login/{provider}', 'SocialLoginController@login')->name('social.login');

        // Registration Routes
        if (config('access.users.registration')) {
            Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
            Route::post('register', 'RegisterController@register')->name('register');
        }

        // Confirm Account Routes
        Route::get('account/confirm/{token}', 'ConfirmAccountController@confirm')->name('account.confirm');
        Route::get('account/confirm/resend/{user}', 'ConfirmAccountController@sendConfirmationEmail')->name('account.confirm.resend');

        // Password Reset Routes
        Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.email');
        Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');

        Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset.form');
        Route::post('password/reset', 'ResetPasswordController@reset')->name('password.reset');
    });
});



Route::get('/','HomeController@index');

Auth::routes();

    Route::group(['namespace' => 'Admin','prefix' => 'admin'], function(){ 
     
         Route::get('/','AdminController@index');
         Route::get('admin-dashboard', 'DashboardController@index');
         Route::get('profile', 'ProfileController@update');
         Route::get('password/change', 'ChangePasswordController@changePassword');
        
        Route::resource('user','UserController');
        Route::resource('customer','CustomerController');
        Route::resource('usertherapist','UsertherapistController');
        Route::resource('course','CourseController');
        Route::resource('faq','FaqController');
        Route::resource('transaction','TransactionController');
        Route::resource('message','MessageController');
        Route::resource('product','ProductController');

});



Route::group(['namespace' => 'User'], function () {

    Route::group(['middleware' =>['auth','customer'] ], function() { 

            Route::get('/home', 'HomeController@index');

            Route::get('messages', 'MessagesController@showListingMessages')->name('messages');
            
            Route::get('messages/{id}', 'MessagesController@chatHistory');

            Route::get('customer-dashboard', 'DashboardController@index')->name('customer-dashboard');

            Route::get('my-bookings', 'MyBookingsController@index')->name('my-bookings');

            Route::get('my-therapist', 'MyBookingsController@index')->name('my-therapist');

            Route::get('transaction', 'TransactionController@index')->name('transaction');
            
            Route::post('delete/{id}', 'TransactionController@destroy')->name('transaction/delete')->where('id', '[0-9]+');
            
            Route::get('search', 'TransactionController@search')->name('search');

            Route::get('therapist', 'TherapistController@index')->name('therapist');

            Route::get('find-therapist', 'FindTherapistController@index')->name('find-therapist');
        });
 
});

Route::group(['namespace' => 'Therapist'], function () {

    Route::group(['middleware' => ['auth','therapist']], function() {

        Route::get('therapist-dashboard', 'TherapistDashboardController@index')->name('therapist-dashboard');

        Route::get('therapist-messages', 'TherapistMessagesController@index')->name('therapist-messages');

        Route::get('therapist-bookings', 'TherapistBookingsController@index')->name('therapist-bookings');

        Route::get('therapist-schedules', 'TherapistSchedulesController@index')->name('therapist-schedules');

        Route::get('store', 'StoreController@index')->name('store');

        Route::get('therapist-insurance', 'TherapistInsuranceController@index')->name('therapist-insurance');

        Route::get('therapist-transaction', 'TherapistTransactionController@index')->name('therapist-transaction');
        
        Route::post('delete/{id}', 'TherapistTransactionController@destroy')->name('transaction/delete')->where('id', '[0-9]+');
         
        Route::get('search', 'TherapistTransactionController@search')->name('search');
    });
});



$(document).ready(function(){
  /* TRIGGERING CAROUSELS */



$('.home-carousel').owlCarousel({
    items:1,
    loop:true,
    autoplay:true
});
  $('.professional-carousel').owlCarousel({
    items:4,
    loop:true,
    margin:30,
    merge:true,
    autoplay:true,
    dots:false,
    nav:true,
    responsive:{
        320:{
           items:1
        },
        570 :{
           items:2
        },
        991:{
           items:3
        },
        1200  :{
            items:4
        }
    }
});
  $('.testimonial-carousel').owlCarousel({
    items:1,
    loop:true,
    nav:true,
    autoplay:true,
    dots: false

});


  /* ADDING HOVER ANIMATION ON PROFESSSIONAL CAROUSEL ITEMS*/
  $('.professional-carousel .item').hover(function(){
    $(this).find('.member-box').toggleClass('inc-height');
    $(this).find('img').toggleClass('color-img');
  });

  /* CHANGING NAVBAR-TOGGLE ICON*/
  $('.navbar-header button').click(function(){
    if($(this).find('i').hasClass('fa-bars')){
      $(this).find('i').removeClass('fa-bars');
      $(this).find('i').addClass('fa-close');
    } else {
      $(this).find('i').removeClass('fa-close');
      $(this).find('i').addClass('fa-bars');
    }
  });

});


      var sidebarLeft = function(){
        var sidebarHeight = $('.dynamicHeight');
        var winHeight = $(window).height();
        $(sidebarHeight).height(winHeight);

      };

   // Match Height Area
   /*var sidebarLeft2 = function(){
      var rightsidbar = $('.eqh').height();
      $('.dynamicHeight3').height(rightsidbar );
    };*/

    sidebarLeft();
    sidebarLeft2();
  $(window).load(function(){
    sidebarLeft();
    sidebarLeft2();
  });
   $(window).resize(function() {
      sidebarLeft();
      sidebarLeft2();
   });
   
   
   
